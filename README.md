# Ogre
See Pierre Mézières's gitlab for further informations 

*last update : 18/12/2018*

Ogre stands for **O**bject **G**raphic **R**ender **E**ngine.

It's a project of a simple 3D Render Engine.

It's mainly inspired and designed from 2 other projects:

* [Learn Opengl](https://learnopengl.com/): a very good tutorial to learn how to use the OpenGL API.
* [Radium-Engine](https://github.com/STORM-IRIT/Radium-Engine): a 3D Render Engine from the [STORM](https://github.com/STORM-IRIT/Radium-Engine) team from [IRIT](https://www.irit.fr/).

## Supported compiler and platforms

* Only *Linux* for the moment

**Please follow these rules ! (Accident arrive quickly ...)** :skull:

## Build instructions

After cloning the project:

### Getting submodules
# normally, no need to get the submodules because they are already on the master branch

### Configure build

It's recommanded to follow the usual sequence:

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
$ make
```
(yes, twice "make" because the first one won't succeed, this is normal)

### Run the project

You'll find the *executable* in bin folder. :sunglasses:

## Assets

If you want to download some assets, [get this](https://drive.google.com/open?id=1RiFE7bAllgjzrg7_Dw6zRhFpGAosDN24).

For the moment, it's recommanded to download it !

Put the directory *Assets* directly in the directory where you have *src/Shaders/...*.

## Plugin

The concept of plugin has begun to be implemented.

For the moment, plugin have to be with the source (src/Plugin directory).
 
A small tutorial will be added later, when adding plugin will be more advanced.

For the moment, follow the plugin interface (src/Plugin/PluginInterface) and don't to forget to add the plugin in the plugin vector (src/Plugin/LoaderPlugin) if you want it to load !

## Extra

Enjoy ! :smile:

Do not hesitate to report some bugs or any indication you find useful !
