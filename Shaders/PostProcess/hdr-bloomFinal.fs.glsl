#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D scene;
uniform sampler2D bloomBlur;
uniform float exposure;
uniform float gamma;

void main()
{            
    	vec3 hdrColor = texture(scene, texCoord).rgb;      
    	vec3 bloomColor = texture(bloomBlur, texCoord).rgb;
    	hdrColor += bloomColor; // additive blending
    	// tone mapping
    	vec3 result = vec3(1.0) - exp(-hdrColor * exposure);
    	// also gamma correct  
    	result = pow(result, vec3(1.0 / gamma));
    	color = vec4(result, 1.0);

}
