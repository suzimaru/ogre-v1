#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

void main()
{
    color = texture(screenTexture, texCoord);
    float average = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
    color = vec4(vec3(average), 1.0);
}