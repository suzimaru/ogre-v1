#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

void main()
{

    color = vec4(vec3(1.0 - texture(screenTexture, texCoord)), 1.0);
}