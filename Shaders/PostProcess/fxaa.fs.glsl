#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

uniform float thresholdMin;
uniform float thresholdMax;
uniform vec2 texelStep;

const float subpix_trim =2;
const float subpix_trim_scale=0;
const float subpix_cap=3./4.;

const float searchIterations = 10;
//const float search_acceleration = 1;
//const float search_threshold = 1./4.;



float colorToLuma(vec3 rgb){
    return rgb.r * 0.299 + rgb.g * 0.587 + rgb.b * 0.114;
}

void main()
{
    vec3 colorMid = textureOffset(screenTexture, texCoord, ivec2(0,0)).rgb;

    /****************** Detecting where to apply AA ********************/
//  Luma pour le fragment courant
    float lumaMid = colorToLuma(colorMid);

//  Luma pour les 4 voisins du fragment courant
    float lumaB = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(0,-1)).rgb);
    float lumaH = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(0,1)).rgb);
    float lumaG = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(-1,0)).rgb);
    float lumaD = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(1,0)).rgb);

//  trouver les max et min de luma dans le voisinage
    float lumaMin = min(lumaMid, min(min(lumaB, lumaH), min(lumaG, lumaD)));
    float lumaMax = max(lumaMid, max(max(lumaB, lumaH), max(lumaG, lumaD)));
    float lumaRange = lumaMax - lumaMin;

    // si plus petit qu'un certain seuil, pas nécessaire
    if(lumaRange < max(thresholdMin, lumaMax * thresholdMax))
    {
        color = vec4(colorMid, 1.0);
        return;
    }


    /************ Estimating gradient and choosing edge direction *****************/

    // récupérer luma des coins
    float lumaBG = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(-1,-1)).rgb);
    float lumaHD = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(1,1)).rgb);
    float lumaHG = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(-1,1)).rgb);
    float lumaBD = colorToLuma(textureOffset(screenTexture, texCoord, ivec2(1,-1)).rgb);

    // combiner les 4 lumas bords
    float lumaBH = lumaB + lumaH;
    float lumaGD = lumaG + lumaD;

    // combiner pour les coins
    float lumaCG = lumaBG + lumaHG;
    float lumaCB = lumaBG + lumaBD;
    float lumaCD = lumaBD + lumaHD;
    float lumaCH = lumaHD + lumaHG;

    // calculer l'estimation des gradients le long des axes verticaux et horizontaux
    float edgeHorizontal = abs(-2.0 * lumaG + lumaCG) + abs(-2.0 * lumaMid + lumaBH)*2.0 + abs(-2.0 * lumaD + lumaCD);
    float edgeVertical =   abs(-2.0 * lumaH + lumaCH) + abs(-2.0 * lumaMid + lumaGD)*2.0 + abs(-2.0 * lumaB + lumaCB);

    // pour savoir si le bord local est horizontal ou vertical
    bool isHorizontal = (edgeHorizontal >= edgeVertical);


    /* ******************* Choosing edge orientation *********************/
    //selectionner les deux texels lumas voisins dans la direction opposé au bord local
    float luma1 = isHorizontal ? lumaB : lumaG;
    float luma2 = isHorizontal ? lumaH : lumaD;

    //calculer les gradients dans cette direction
    float grad1 = luma1 - lumaMid;
    float grad2 = luma2 - lumaMid;

    // récupérer le steepest
    bool is1Steepest = abs(grad1) >= abs(grad2);

    float gradScaled = 0.25*max(abs(grad1),abs(grad2));

    float stepLength = isHorizontal ? texelStep.y : texelStep.x;

    float lumaLocalAverage = 0.0;
    if(is1Steepest){
	//switch les directions
        stepLength = -stepLength;
        lumaLocalAverage = 0.5*(luma1 +lumaMid);
    }else{
        lumaLocalAverage = 0.5*(luma2 + lumaMid);
    }

    vec2 currentUv = texCoord;
    if(isHorizontal){
        currentUv.y +=stepLength * 0.5;
    }else{
        currentUv.x +=stepLength *0.5;
    }


    /********************* ITERATION EXPLORATION ***********************/
   
    vec2 offset = isHorizontal ? vec2(texelStep.x,0.0) : vec2(0.0,texelStep.y);
    
    vec2 pos1 = currentUv;
    vec2 pos2 = currentUv;

    float lumaEnd1, lumaEnd2;
    bool reach1 = false, reach2 = false;

    for(int i = 0; i < searchIterations; i++){
        if(!reach1) 
    	    lumaEnd1 = colorToLuma(texture(screenTexture, pos1).rgb) - lumaLocalAverage;
        if(!reach2)
	    lumaEnd2 = colorToLuma(texture(screenTexture, pos2).rgb) - lumaLocalAverage;

// If the luma deltas at the current extremities is larger than the local gradient, we have reached the side of the edge.
        reach1 = abs(lumaEnd1) >= gradScaled; 
        reach2 = abs(lumaEnd2) >= gradScaled;
        
        if(!reach1) 
	    pos1 -= offset;
        if(!reach2) 
	    pos2 += offset;
        if(reach1 && reach2){ break;}
    }



    /******************** EStimating offset ************************/

    float distance1 = isHorizontal ? (texCoord.x - pos1.x) : (texCoord.y - pos1.y);
    float distance2 = isHorizontal ? (pos2.x - texCoord.x) : (pos2.y - texCoord.y);

    bool isDirection1 = distance1 < distance2;

    float distanceFinal = min(distance1, distance2);
    float edgeThickness = (distance1 + distance2);
    float pixelOffset = - distanceFinal / edgeThickness + 0.5;

    bool isLumaCenterSmaller = lumaMid < lumaLocalAverage;
    // If the luma at center is smaller than at its neighbour, the delta luma at each end should be positive (same variation).
    // (in the direction of the closer side of the edge.)
    bool correctVariation = ((isDirection1 ? lumaEnd1 : lumaEnd2) < 0.0) != isLumaCenterSmaller;
    
    float finalOffset = correctVariation ? pixelOffset : 0.0;


    /****************** Subpixel antialiasing ************************/

    float lumaAverage = (1.0/12.0) * (2.0 * (lumaBH + lumaGD) + lumaCG + lumaCD);

    float subPixelOffset1 = clamp(abs(lumaAverage - lumaMid)/lumaRange,0.0,1.0);
    float subPixelOffset2 = (-2.0 * subPixelOffset1 + 3.0) * subPixelOffset1 * subPixelOffset1;
    float subPixelOffsetFinal = subPixelOffset2 * subPixelOffset2 * 0.75;

    finalOffset = max(finalOffset,subPixelOffsetFinal);


    /********************** Final read ***************************/

    vec2 finalUv = texCoord;
    if(isHorizontal){
        finalUv.y += finalOffset * stepLength;
    } else {
        finalUv.x += finalOffset * stepLength;
    }
    color = vec4(texture(screenTexture,finalUv).rgb, 1);
}

