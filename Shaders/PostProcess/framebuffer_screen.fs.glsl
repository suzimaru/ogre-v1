#version 410 core
out vec4 color;

in vec2 texCoord;

uniform sampler2D screenTexture;

void main()
{
    vec3 col = texture(screenTexture, texCoord).rgb;
    color = vec4(col, 1.0);
}
