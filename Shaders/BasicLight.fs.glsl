#version 410 core

#include "Data.glsl"

in vec3 normal;
in vec3 fragPos;
in vec2 texCoord;

out vec4 color;

#define MAX_LIGHTS 10

uniform int numberLights;

uniform sampler2D textureSSAO;
uniform bool ssaoActivated;

uniform Light lights[MAX_LIGHTS];
uniform vec3 viewPos;
uniform Material material;

#include "Material.glsl"
#include "Lighting.glsl"

void main()
{
    vec3 result = vec3(0);

    for(int i=0; i<numberLights; i++){
        result += compute_lighting(lights[i]);
    }

    color = vec4(result, 1.0);
}
