struct Attenuation{
    float constant;
    float linear;
    float quadratic;
};

struct DirLight
{
    vec3 direction;
};

struct SpotLight
{
    vec3 position;
    vec3 direction;

    float innerAngle;
    float outerAngle;

    Attenuation attenuation;
};

struct PointLight
{
    vec3 position;

    Attenuation attenuation;
};

struct Light
{
    int type;
    vec3 color;

    DirLight dirLight;
    SpotLight spotLight;
    PointLight pointLight;

};

struct Material
{
    float ambient;

    vec3 kd;
    vec3 ks;
    float ns;

    bool hasKdTexture;
    bool hasKsTexture;
    bool hasNsTexture;

    sampler2D kdTexture;
    sampler2D ksTexture;
    sampler2D nsTexture;
};