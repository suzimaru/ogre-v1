#version 410 core

#include "Data.glsl"
in vec3 normal;

uniform Material material;
in vec2 texCoord;

out vec4 color;


#include "Material.glsl"

void main()
{
    //color = vec4(vec3(clamp(dot(normalize(normal), vec3(0,0,1)), 0, 1)), 1.0);
    color = vec4(getKd(),0.5);
}
