#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 itexCoord;

out vec3 texCoord;

uniform mat4 proj;
uniform mat4 view;

void main()
{
    vec4 pos = proj * view * vec4(position,1.0);
    gl_Position = pos.xyww;
    texCoord = position;
}
