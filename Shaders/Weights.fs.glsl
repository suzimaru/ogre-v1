#version 410 core
out vec4 color;

in vec2 texCoord;

void main()
{
    color = vec4(texCoord.x, texCoord.y, 0.0, 1.0);
}
