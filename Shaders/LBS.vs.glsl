#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 itexCoord;

uniform mat4 model;
uniform mat4 proj;
uniform mat4 view;
uniform mat4 o1s
uniform mat4 os2

out vec3 normal;
out vec2 texCoord;
out vec3 fragPos;

void main()
{
    // il n'y a que 2 os, donc les transformations os1 et os2
    // ne seront affectees que par 2 poids
    // stockes dans les deux 1eres coordonnees de texture
    mat4 blendingMatrix = itexCoord.x * os1 + itexCoord.y * os2;
    vec4 newPos4 = blendingMatrix * vec4(position,1);
    vec3 newPos = vec3(newPos4.x,newPos4.y,newPos4.z);
    normal = mat3(transpose(inverse(model))) * inormal;
    gl_Position = proj * view * model * vec4(newPos, 1.0);
    texCoord = itexCoord;
    fragPos = vec3(model * vec4(newPos, 1.0));
}

