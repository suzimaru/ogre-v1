#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;
layout (location = 2) in vec2 itexCoord;

out vec3 fragPos;
out vec2 texCoord;
out vec3 normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main()
{
    //vec4 viewPos = view * model * vec4(position, 1.0);
    //fragPos = viewPos.xyz; 
    //texCoord = itexCoord;
    
    //mat3 normalMatrix = transpose(inverse(mat3(view * model)));
    //normal = normalMatrix * inormal;
    
    //gl_Position = proj * viewPos;
    
    normal=mat3(transpose(inverse(model))) * inormal;
    gl_Position = proj * view * model * vec4(position, 1.0);
    texCoord = itexCoord;
    fragPos = vec3(view * model * vec4(position, 1.0));

}
