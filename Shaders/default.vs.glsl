#version 410 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 inormal;

uniform mat4 model;
uniform mat4 proj;
uniform mat4 view;

out vec3 normal;
out vec3 fragPos;

void main()
{
    normal=inormal;
    gl_Position = proj * view * model * vec4(position, 1.0);
    fragPos = vec3(model * vec4(position, 1.0));
}
