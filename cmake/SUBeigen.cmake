
# here is defined the way we want to import eigen
ExternalProject_Add(
eigen

# where the source will live
SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/submodule/eigen-git-mirror"

# override default behaviours
UPDATE_COMMAND ""
GIT_SUBMODULES submodule/eigen-git-mirror

# set the installatin to root
# INSTALL_COMMAND cmake -E echo "Skipping install step."
INSTALL_DIR "${SUBMODULE_INSTALL_DIRECTORY}"
CMAKE_GENERATOR ${CMAKE_GENERATOR}
CMAKE_GENERATOR_TOOLSET ${CMAKE_GENERATOR_TOOLSET}
CMAKE_ARGS
-DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
-DINCLUDE_INSTALL_DIR=${SUBMODULE_INSTALL_DIRECTORY}/include
-DCMAKE_BUILD_TYPE=${SUBMODULE_BUILD_TYPE}
-DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
-DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
-DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
-DCMAKE_SHARED_LINKER_FLAGS=${CMAKE_SHARED_LINKER_FLAGS}
-DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}
-DEIGEN_TEST_CXX11=ON
-DBUILD_TESTING=OFF
)

add_custom_target(eigen3
DEPENDS eigen
)
# ----------------------------------------------------------------------------------------------------------------------

set(EIGEN_INCLUDE_DIR ${SUBMODULE_INSTALL_DIRECTORY}/include)
