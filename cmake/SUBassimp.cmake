
#set(ASSIMP_DIR ${CMAKE_SOURCE_DIR}/submodule)
#find_path(ASSIMP_DIR ...)
#if((NOT ASSIMP_DIR) OR (NOT EXISTS ${ASSIMP_DIR}))
#    message(FATAL_ERROR "UNABLE TO FIND ASSIMP : do not forget git submodule init/update")

#else()
#    add_subdirectory(submodule/assimp)
#endif()

set(ASSIMPLIBNAME assimp)

set(ASSIMP_INCLUDE_DIR ${SUBMODULE_INSTALL_DIRECTORY}/include)
set(ASSIMP_LIBRARIES "${SUBMODULE_INSTALL_DIRECTORY}/lib/lib${ASSIMPLIBNAME}.so")


ExternalProject_Add(
        assimp
        # where the source will live
        SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/submodule/assimp"

        # override default behaviours
        UPDATE_COMMAND ""
        GIT_SUBMODULES submodule/assimp

        # set the installatin to installed/assimp
        INSTALL_DIR "${SUBMODULE_INSTALL_DIRECTORY}"
        BUILD_BYPRODUCTS "${ASSIMP_LIBRARIES}"
        CMAKE_GENERATOR ${CMAKE_GENERATOR}
        CMAKE_GENERATOR_TOOLSET ${CMAKE_GENERATOR_TOOLSET}
        CMAKE_ARGS
        -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
        -DASSIMP_BUILD_ASSIMP_TOOLS=False
        -DASSIMP_BUILD_SAMPLES=False
        -DASSIMP_BUILD_TESTS=False
        -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
        -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}
        -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}
        -DCMAKE_SHARED_LINKER_FLAGS=${CMAKE_SHARED_LINKER_FLAGS}
        -DCMAKE_BUILD_TYPE=${SUBMODULE_BUILD_TYPE}
        -DCMAKE_PREFIX_PATH=${CMAKE_PREFIX_PATH}
        -DASSIMP_INSTALL_PDB=False
        -DINSTALL_NAME_DIR=${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
)

add_custom_target(assimp_lib
        DEPENDS assimp
        )
