
include(ExternalProject)

set(SUBMODULE_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/submodule)
set(SUBMODULE_INSTALL_DIRECTORY ${OGRE_BUILD_DIRECTORY}/submodule)
set(SUBMODULE_BUILD_TYPE Release)

include(SUBassimp)
include(SUBeigen)
include(SUBopenmesh)

#stb
file(GLOB_RECURSE stb_headers "${CMAKE_CURRENT_SOURCE_DIR}/submodule/stb/*.h")
add_custom_target(stb_lib
        COMMAND ${CMAKE_COMMAND} -E make_directory ${SUBMODULE_INSTALL_DIRECTORY}/include/stb/
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${stb_headers} ${SUBMODULE_INSTALL_DIRECTORY}/include/stb/)
set(STB_INCLUDE_DIR ${SUBMODULE_INSTALL_DIRECTORY}/include/)