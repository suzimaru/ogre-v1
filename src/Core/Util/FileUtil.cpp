#include "FileUtil.hpp"

#include <cstdlib>
#include <climits>
#include <string.h>
#include <libgen.h>

std::string FileUtil::searchDirectory;

bool FileUtil::IsAbsolutePath(const std::string &filename) {
    return (filename.size() > 0) && filename[0] == '/';
}

std::string FileUtil::AbsolutePath(const std::string &filename) {
    char full[1000];
    if (realpath(filename.c_str(), full))
        return std::string(full);
    else
        return filename;
}

std::string FileUtil::ResolveFilename(const std::string &filename) {
    if (searchDirectory.empty() || filename.empty())
        return filename;
    else if (IsAbsolutePath(filename))
        return filename;
    else if (searchDirectory[searchDirectory.size() - 1] == '/')
        return searchDirectory + filename;
    else
        return searchDirectory + "/" + filename;
}

/*std::string FileUtil::DirectoryContaining(const std::string &filename) {
    char *t = strdup(filename.c_str());
    std::string result = dirname(t);
    free(t);
    return result;
}*/

void FileUtil::SetSearchDirectory(const std::string &dirname) {
    searchDirectory = dirname;
}
