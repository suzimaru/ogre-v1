//
// Created by pierre on 24/07/18.
//

#ifndef OGRE_STRINGUTILS_HPP
#define OGRE_STRINGUTILS_HPP


#include <vector>
#include <string>

class StringUtils {
public:

    // Split the string with character 'c', but if 2 'c' are following, the second will not be removed
    static std::vector<std::string> splitString(std::string &str, char c);
    // Split the string with character 'c', remove all 'c' in the string
    static std::vector<std::string> splitStringForce(std::string &str, char c);
};


#endif //OGRE_STRINGUTILS_HPP
