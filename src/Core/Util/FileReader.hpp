#ifndef FILEREADER_HPP
#define FILEREADER_HPP

#include <string>
#include <vector>

class FileReader
{
public:
    FileReader();

    /* Get the content of the file */
    static const char* getFileWithCharStar(const char* name);
    static std::string getFileWithString(const char* name);
    static const char* getFileWithCharStar(const std::string &name);
    static std::string getFileWithString(const std::string &name);

};

#endif // FILEREADER_HPP
