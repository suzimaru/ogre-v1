//
// Created by pierre on 24/07/18.
//

#include <sstream>
#include "StringUtils.hpp"


std::vector<std::string> StringUtils::splitString(std::string &str, char c){
    std::stringstream ss{str};
    std::string one;
    std::vector<std::string> res;

    while(std::getline(ss,one,c))
        res.push_back(one);

    return res;
}

std::vector<std::string> StringUtils::splitStringForce(std::string &str, char c){
    std::stringstream ss{str};
    std::string one;
    std::vector<std::string> res;

    while(std::getline(ss,one,c)){
        if(one!="") res.push_back(one);
    }

    return res;
}