#ifndef FILEUTIL_HPP
#define FILEUTIL_HPP


#include <string>
#include <cctype>
#include <vector>

/* Basic functions to manage file and directory */

class FileUtil
{
public:

    static bool IsAbsolutePath(const std::string &filename);
    static std::string AbsolutePath(const std::string &filename);
    static std::string ResolveFilename(const std::string &filename);
    //static std::string DirectoryContaining(const std::string &filename);
    static void SetSearchDirectory(const std::string &dirname);

    static std::string searchDirectory;


};

#endif // FILEUTIL_HPP
