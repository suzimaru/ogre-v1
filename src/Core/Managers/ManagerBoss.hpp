//
// Created by pierre on 26/07/18.
//

#ifndef OGRE_MANAGERBOSS_HPP
#define OGRE_MANAGERBOSS_HPP

#include <map>
#include <string>
#include <vector>
#include <src/Core/Log/Log.hpp>

#define IDM this->m_nameM

enum Mode_mb{MODE_NAME, MODE_INDEX};


template<typename T>
class ManagerBoss {
public:


    ManagerBoss(Mode_mb mode, std::string name);

    Mode_mb m_mode;
    std::string m_nameM;
};

template<typename T>
ManagerBoss<T>::ManagerBoss(Mode_mb mode, std::string name): m_mode{mode}, m_nameM{"["+name+"] "}{
}

#endif //OGRE_MANAGERBOSS_HPP
