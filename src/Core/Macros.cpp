#include "Macros.hpp"
#include <Core/Log/Log.hpp>

float degToRad(float t){
    return (Math_Pi * 2) * (t / 360.f);
}

void displayMat4(Matrix4 mat){
    std::stringstream ss;
    ss << "\n   ";
    for(int i=0; i!=4; i++){
        for(int j=0; j!=4; j++){
            ss << mat[i][j] << "  ";
        }
        ss << "\n   ";
    }
    Log(logInfo) << "Matrix :" << ss.str();
}

void displayVec3(Vector3 vec){
    Log(logInfo) << "Vec x:" << vec.x << " y:" << vec.y << " z:" << vec.z;
}

Vector3 getPositionMat4(Matrix4 mat){
    return Vector3{mat[3][0], mat[3][1], mat[3][2]};
}

void setPositionMat4(Matrix4 &mat, Vector3 vec){
    mat[3][0] = vec.x;
    mat[3][1] = vec.y;
    mat[3][2] = vec.z;
}

void GL_CHECK_ERROR(){
    GLenum err;
    Log(logInfo) << "Check Error OpenGL:";
    while((err = glGetError()) != GL_NO_ERROR){
        Log(logError) << "OpenGL Error:" << err;
    }
}

Transform Identity() {
    return Matrix4(1.0, 0.0, 0.0, 0.0,
                   0.0, 1.0, 0.0, 0.0,
                   0.0, 0.0, 1.0, 0.0,
                   0.0, 0.0, 0.0, 1.0);
}