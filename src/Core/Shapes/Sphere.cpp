#include "Sphere.hpp"

#include <Core/Macros.hpp>
#include <Core/Log/Log.hpp>

#include <math.h>
#include <iostream>

Mesh createSphereShape(float radius){

    Mesh m;
    if(radius<0) return m;

    uint slices=20, stacks=20;

    for ( uint u = 0; u <= slices; ++u )
    {
        float phi = 2 * u * M_PI / slices;
        for ( uint v = 0; v <= stacks; ++v )
        {
            // Regular vertices on the sphere.
            float theta = v * M_PI / stacks ;

            m.m_vertices.push_back( Point3(
                                             -radius * std::cos( phi ) * std::sin( theta ),
                                             radius * std::cos( theta ),
                                             radius * std::sin( phi ) * std::sin( theta )
                                             ));


            m.m_texCoord.push_back(Point3((float)u/slices,(float)v/stacks, 0) );
            // Regular triangles
            if ( v < stacks && u<slices)
            {
                const uint nextSlice = ((u + 1) % (slices + 1)) * (stacks + 1);
                const uint baseSlice = u * (stacks + 1);
                m.setTriangle(
                            Triangle( baseSlice + v , baseSlice + v +1, nextSlice + v +1 ));
                m.setTriangle(
                            Triangle( baseSlice + v , nextSlice + v + 1, nextSlice + v  ));
            }
        }
    }

    // Compute normals.
    for ( const auto v : m.m_vertices )
    {
        //const VectorNormal n = v.
        m.m_normals.push_back( v );
    }

    return m;
}
