#ifndef SPHERE_HPP
#define SPHERE_HPP

#include "src/Engine/Object/Mesh.hpp"

Mesh createSphereShape(float radius=1);

#endif // SPHERE_HPP
