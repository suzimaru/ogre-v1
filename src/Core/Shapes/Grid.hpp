//
// Created by pierre on 23/08/18.
//

#ifndef OGRE_GROD_HPP
#define OGRE_GROD_HPP

#include "src/Engine/Object/Mesh.hpp"

Mesh createGridShape(float l = 10, float L = 10, float x = 10, float y = 10);


#endif //OGRE_GROD_HPP
