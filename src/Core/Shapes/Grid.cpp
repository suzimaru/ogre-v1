//
// Created by pierre on 23/08/18.
//

#include "Grid.hpp"

Mesh createGridShape(float l, float L, float x, float y){

    Mesh m;

    float stepX = l/x;
    float stepY = L/y;

    float startX = -l/2;
    float startY = -L/2;
    x++;
    y++;

    for(int i = 0; i < x; i++){
        for(int j = 0; j < y; j++){
            m.m_vertices.push_back(Vector3(startX+i*stepX,0,startY+j*stepY));
            m.m_texCoord.push_back(Vector3(0));
            m.m_normals.push_back(Vector3(0,1,0));
        }
    }

    for(int i = 0; i < x; i++){
        for(int j = 0; j < y-1; j++){
            m.setLine( Line( i*x+j,i*x+j+1) );
        }
    }

    for(int i = 0; i < x-1; i++){
        for(int j = 0; j < y; j++){
            m.setLine( Line( i*x+j,i*x+x+j ) );
        }
    }

    m.setMeshTypes(GL_LINES);

    return m;
}
