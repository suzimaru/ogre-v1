//
// Created by pierre on 31/08/18.
//

#ifndef OGRE_SIMPLEMESH_HPP
#define OGRE_SIMPLEMESH_HPP

#include "src/Engine/Object/Mesh.hpp"

Mesh createTriangleMeshShape(
        VectorPoint3 vertices,
        VectorUInt indices,
        VectorPoint3 texCoord = VectorPoint3(),
        VectorNormal normal = VectorNormal());

Mesh createSimpleRec();
Mesh createSimpleCube();

#endif //OGRE_SIMPLEMESH_HPP