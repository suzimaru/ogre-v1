//
// Created by pierre on 20/08/18.
//

#ifndef OGRE_CYLINDER_HPP
#define OGRE_CYLINDER_HPP

#include "src/Engine/Object/Mesh.hpp"

Mesh createCylinderShape(float length = 1, float radius = 1);
Mesh createCylinderShapeAnim(std::vector<std::vector<float>> &weights, float alpha, Vector3 base = Vector3(0,0,0), Vector3 axis = Vector3(1,0,0), float radius = 1.0f, float length = 10.0f, int subdiv1 = 32, int subdiv2 = 256);


#endif //OGRE_CYLINDER_HPP
