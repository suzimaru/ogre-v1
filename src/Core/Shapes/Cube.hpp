//
// Created by pierre on 26/07/18.
//

#ifndef OGRE_CUBE_HPP
#define OGRE_CUBE_HPP

#include "src/Engine/Object/Mesh.hpp"

Mesh createCubeShape(float length = 1, float width = 1, float depth = 1);

#endif //OGRE_CUBE_HPP
