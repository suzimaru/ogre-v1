//
// Created by pierre on 20/08/18.
//

#include "Cylinder.hpp"
#include <math.h>

Mesh createCylinderShape(float length, float radius){

    Mesh m;

    const Vector3 a{0,0,0};
    const Vector3 b{0,length,0};
    int nFaces=32;

    Vector3 xPlane(1,0,0);
    Vector3 yPlane(0,0,1);

    Vector3 c = Vector3(0.5) * ( a + b );

    const float thetaInc( Math_Pi*2 / nFaces );
    for ( uint i = 0; i <= nFaces; ++i )
    {
        float theta = i * thetaInc;

        m.m_vertices.push_back(
                a + radius * ( std::cos( theta ) * xPlane + std::sin( theta ) * yPlane ) );
        m.m_vertices.push_back(
                c + radius * ( std::cos( theta ) * xPlane + std::sin( theta ) * yPlane ) );
        m.m_vertices.push_back(
                b + radius * ( std::cos( theta ) * xPlane + std::sin( theta ) * yPlane ) );

        for( int j =0; j<3; ++j){

            m.m_normals.push_back( Vector3(std::cos(theta), 0, std::sin(theta)));
        }

        theta = i*thetaInc;

        for(uint v=0;v<3;++v){
            const float phi = v  * Math_Pi / 2;
            m.m_texCoord.push_back({0.5 * theta / Math_Pi, phi / Math_Pi, 0});
        }
    }

    for ( uint i = 0; i < nFaces; ++i )
    {
        // Outer face.
        uint obl = 3 * i;                      // bottom left corner of outer face
        uint obr = 3 * ( i + 1 );              // bottom right corner of outer face
        uint oml = obl + 1;                    // mid left
        uint omr = obr + 1;                    // mid right
        uint otl = oml + 1;                    // top left
        uint otr = omr + 1;                    // top right

        m.setTriangle( Triangle( oml, obr,obl  ) );
        m.setTriangle( Triangle( oml, omr, obr ) );

        m.setTriangle( Triangle( otl, omr, oml  ) );
        m.setTriangle( Triangle( otl, otr, omr ) );

    }

    return m;
}

inline float lerp(float v0, float v1, float t) {
    return (1 - t) * v0 + t * v1;
}

inline float smooth (float R, float d, float alpha) {
    return (1.0/glm::pow(R,alpha)) * glm::pow(((R*R) - (d*d)),alpha/2.0);
}


Mesh createCylinderShapeAnim(std::vector<std::vector<float>> &weights, float alpha, Vector3 base, Vector3 axis, float radius,
                             float length, int subdiv1, int subdiv2) {
//    alpha = 0.25;
    float min = 0.5 - alpha;
    float max = 0.5 + alpha;
    float weight1, weight0;
    Mesh m;
    Vector3 x = Vector3(0,1,0); //orthogonal(axis);
    Vector3 y = cross(axis, x);
    weights.resize(2);
    for(int i=0; i<subdiv2; i++)
    {
        float offset = float(i)/float(subdiv2-1);
        float offset2 = (offset-0.5)*length;
        for(int j=0; j<subdiv1; j++)
        {
            float angle = 2.*glm::pi<float>()*float(j)/float(subdiv1);
            Point3 nv;
            nv = base+offset2*axis+radius*cos(angle)*x+radius*sin(angle)*y;
            m.m_vertices.push_back(nv);
            m.m_normals.push_back(normalize( cos(angle)*x+sin(angle)*y) );

            if (offset < min) {
                weight1 = 0.0;
                weight0 = 1.0;
            }
            else if (offset > max) {
                weight1 = 1.0;
                weight0 = 0.0;
            }
            else {
                float distance = glm::distance(Vector3(offset,0.0,0.0),Vector3(min,0.0,0.0));
                float t = smooth((max-min),distance,8.0);  //4 : valeur ad-hoc
                weight0 = t;
                weight1 = 1.0-t;
            }
            weights[1].push_back(weight1);
            weights[0].push_back(weight0);
        }
    }
    for (int i=0; i<m.m_vertices.size(); i++) {
        float sum=0.0;
        sum = weights[0][i] + weights[1][i];
        weights[0][i] /= sum;
        weights[1][i] /= sum;
        m.m_texCoord.push_back(Vector3(weights[1][i],weights[0][i],0.0));
    }



    for(unsigned int i=0; i<subdiv2-1; i++)
    {
        for(unsigned int j=0; j<subdiv1; j++)
        {
            m.setTriangle( Triangle( i*subdiv1+j,i*subdiv1+(j+1)%subdiv1,i*subdiv1+j+subdiv1  ));
            m.setTriangle( Triangle( i*subdiv1+(j+1)%subdiv1,i*subdiv1+j+subdiv1, i*subdiv1+(j+1)%subdiv1+subdiv1 ));
        }

    }
    return m;
}

