//
// Created by pierre on 31/08/18.
//

#include "TriangleMesh.hpp"

#define PRE "[createTriangleMeshShape] "

Mesh createTriangleMeshShape(
        VectorPoint3 vertices,
        VectorUInt indices,
        VectorPoint3 texCoord,
        VectorNormal normal){
    Mesh m;

    bool texDefaultB = false;
    bool normalDefaultB = false;

    Point3 texDefault(0,0,0);
    Normal normalDefault(0,1,0);


    unsigned int p = vertices.size();
    if(texCoord.size() != 0){
        if(texCoord.size() != p){
            Log(logError) << PRE << "texCoord.size is not corresponding";
            texDefaultB = true;
        }
    }else texDefaultB = true;

    if(normal.size() != 0){
        if(normal.size() != p){
            Log(logError) << PRE << "texCoord.size is not corresponding";
            normalDefaultB = true;
        }
    } else {
        normalDefaultB = true;
    }

    if(indices.size()%3 != 0){
        Log(logError) << PRE << "indices%3 != 0 ";
    }

    unsigned int cpt = 0;
    for(cpt = 0; cpt != p; cpt++){
        m.m_vertices.push_back(vertices[cpt]);

        if(!normalDefaultB){
            m.m_normals.push_back(glm::normalize(normal[cpt]));
        }

        if(texDefaultB){
            m.m_texCoord.push_back(texDefault);
        }else{
            m.m_texCoord.push_back(texCoord[cpt]);
        }
    }


    for(auto i:indices){
        m.m_indices.push_back(i);
    }

    if(normalDefaultB) m.autoNormals();

    return m;
}

Mesh createSimpleRec(){
    VectorPoint3 vertices;
    vertices.push_back(Point3(-1,0,1));
    vertices.push_back(Point3(-1,0,-1));
    vertices.push_back(Point3(1,0,-1));
    vertices.push_back(Point3(1,0,1));

    VectorUInt indices;
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);

    VectorPoint3 tex;
    tex.push_back(Point3(0,1,0));
    tex.push_back(Point3(0,0,0));
    tex.push_back(Point3(1,0,0));
    tex.push_back(Point3(1,1,0));

    return createTriangleMeshShape(vertices, indices, tex);
}

Mesh createSimpleCube(){

    VectorPoint3 vertices;
    vertices.push_back(Point3(-1,1,-1));
    vertices.push_back(Point3(-1,-1,-1));
    vertices.push_back(Point3(1,-1,-1));
    vertices.push_back(Point3(1,1,-1));

    vertices.push_back(Point3(-1,-1,1));
    vertices.push_back(Point3(-1,-1,-1));
    vertices.push_back(Point3(-1,1,-1));
    vertices.push_back(Point3(-1,1,1));

    vertices.push_back(Point3(1,-1,-1));
    vertices.push_back(Point3(1,-1,1));
    vertices.push_back(Point3(1,1,1));
    vertices.push_back(Point3(1,1,-1));

    vertices.push_back(Point3(-1,-1,1));
    vertices.push_back(Point3(-1,1,1));
    vertices.push_back(Point3(1,1,1));
    vertices.push_back(Point3(1,-1,1));

    vertices.push_back(Point3(-1,1,-1));
    vertices.push_back(Point3(1,1,-1));
    vertices.push_back(Point3(1,1,1));
    vertices.push_back(Point3(-1,1,1));

    vertices.push_back(Point3(-1,-1,-1));
    vertices.push_back(Point3(1,-1,-1));
    vertices.push_back(Point3(1,-1,1));
    vertices.push_back(Point3(-1,-1,1));

    VectorUInt indices;
    for(unsigned int i = 0; i<6; i++) {
        indices.push_back(0+i*4);
        indices.push_back(1+i*4);
        indices.push_back(2+i*4);
        indices.push_back(0+i*4);
        indices.push_back(2+i*4);
        indices.push_back(3+i*4);
    }

    VectorPoint3 tex;
    for(unsigned int i = 0; i<6; i++){
        tex.push_back(Point3(0,1,0));
        tex.push_back(Point3(0,0,0));
        tex.push_back(Point3(1,0,0));
        tex.push_back(Point3(1,1,0));
    }

    return createTriangleMeshShape(vertices, indices, tex);
}