//
// Created by pierre on 02/08/18.
//

#ifndef OGRE_GLMTOEIGEN_HPP
#define OGRE_GLMTOEIGEN_HPP

#include <Core/Macros.hpp>
#include <Eigen/Eigen>


Eigen_Vector3f GTE_VECTOR3(Vector3 vec);


#endif //OGRE_GLMTOEIGEN_HPP
