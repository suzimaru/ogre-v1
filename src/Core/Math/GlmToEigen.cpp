//
// Created by pierre on 02/08/18.
//

#include "GlmToEigen.hpp"

Eigen_Vector3f GTE_VECTOR3(Vector3 vec){
    return Eigen::Vector3f{vec.x,vec.y,vec.z};
}