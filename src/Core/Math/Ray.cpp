//
// Created by pierre on 29/08/18.
//

#include "Ray.hpp"
#include <Engine/Object/Object.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>

Ray::Ray(Vector3 orig, Vector3 dir): m_orig{orig}, m_dir{dir} {
}


bool Ray::updateT(float &t, float &r){
    if(r>0){
        if(t<0){
            t=r;
            return true;
        }
        else if(r<t){
            t=r;
            return true;
        }
    }
    return false;
}

bool Ray::intersectionTriangle(Vector3 &v1, Vector3 &v2, Vector3 &v3, float &t){
    Vector2 bary;
    float d;
    float r;
    bool inter = false;
    if(glm::intersectRayTriangle(m_orig,m_dir,v1,v2,v3,bary,d)){
        r= d;
        if(updateT(t,r)) inter = true;
    }
    return inter;
}

bool Ray::intersectionPlane(Vector3 pt, Normal normal, Vector3 &res) {
    float dist;
    if(glm::intersectRayPlane(m_orig,m_dir,pt,normal,dist)){
        res = m_orig + dist * m_dir;
        return true;
    }
    return false;
}

bool Ray::intersectionMesh(Mesh *m, Matrix4 model, float &t){
    bool inter = false;
    if(m->mesh_types==GL_TRIANGLES){
        for(unsigned int i=0 ; i<m->m_indices.size(); i+=3){
            Vector3 v1 = m->m_vertices[m->m_indices[i]];
            Vector3 v2 = m->m_vertices[m->m_indices[i+1]];
            Vector3 v3 = m->m_vertices[m->m_indices[i+2]];
            Vector4 w1 = model * Vector4(v1,1);
            Vector4 w2 = model * Vector4(v2,1);
            Vector4 w3 = model * Vector4(v3,1);
            v1 = w1;
            v2 = w2;
            v3 = w3;
            if(intersectionTriangle(v1,v2,v3,t)) inter=true;
        }
    }
    return inter;
}

bool Ray::intersectionElement(Element *e, Matrix4 model, float &t){
    return intersectionMesh(&e->m_mesh, model, t);
}

bool Ray::intersectionObject(Object *o, float &t){
    bool inter = false;
    for(auto i:o->getElements()){
        if(intersectionElement(i,o->getModel(), t)) inter=true;
    }
    return inter;
}

Vector3 Ray::getOrig(){
    return m_orig;
}

Vector3 Ray::getDir() {
    return m_dir;
}


void Ray::displayInfo(){
    Log(logInfo) << "Ray, origin : " << m_orig.x << " " << m_orig.y << " " << m_orig.z <<
                    ". direction : " << m_dir.x << " " << m_dir.y << " " << m_dir.z;
}