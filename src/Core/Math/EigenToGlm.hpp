//
// Created by pierre on 03/08/18.
//

#ifndef OGRE_EIGENTOGLM_HPP
#define OGRE_EIGENTOGLM_HPP

#include <Core/Macros.hpp>

Vector3 ETG_VECTOR3(Eigen_Vector3f vec);


#endif //OGRE_EIGENTOGLM_HPP
