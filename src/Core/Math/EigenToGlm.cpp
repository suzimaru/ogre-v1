//
// Created by pierre on 03/08/18.
//

#include "EigenToGlm.hpp"

Vector3 ETG_VECTOR3(Eigen_Vector3f vec){
    return Vector3(vec[0], vec[1], vec[2]);
}