//
// Created by pierre on 20/08/18.
//

#ifndef OGRE_CHRONOMETER_HPP
#define OGRE_CHRONOMETER_HPP

#include <chrono>

class Chronometer {
public:

    Chronometer();

    void start();
    void pause();
    float getDeltaMark();

    std::chrono::high_resolution_clock::time_point t1,t2;

    bool active = false;
private:
};


#endif //OGRE_CHRONOMETER_HPP
