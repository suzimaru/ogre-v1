#ifndef MACROS_HPP
#define MACROS_HPP


#include <vector>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Eigen/Eigen>
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>
#include <string>
#include <build/submodule/include/Eigen/src/Core/Matrix.h>
#include <Eigen/Geometry>
#include <glm/gtc/quaternion.hpp>


typedef glm::vec3 Vector3;
typedef glm::vec2 Vector2;
typedef glm::vec4 Vector4;
typedef glm::vec3 Point3;
typedef glm::vec2 Point2;
typedef glm::vec3 Normal;
typedef glm::uvec3 Triangle;
typedef glm::uvec2 Line;
typedef glm::vec3 Color;

typedef Eigen::Vector3f Eigen_Vector3f;

typedef std::vector<Point3> VectorPoint3;
typedef std::vector<Point2> VectorPoint2;
typedef std::vector<Normal> VectorNormal;
typedef std::vector<GLfloat> VectorFloat;
typedef std::vector<GLuint> VectorUInt;
typedef std::vector<Triangle> VectorTriangles;

typedef glm::mat4 Matrix4;
typedef glm::mat3 Matrix3;
typedef glm::mat2 Matrix2;

typedef Matrix4 Transform;
typedef std::vector<Transform> Pose;

typedef glm::fquat Quaternion;

Transform Identity();

#include <Eigen/Geometry>
#include <src/Core/Log/Log.hpp>

typedef Eigen::AlignedBox<float, 3> AABB;

#define Math_Pi 3.141592653

float degToRad(float t);

template <typename T>
Vector3 setVec3(T x, T y, T z){
    return {x,y,z};
}

template <typename T>
Vector2 setVec2(T x, T y){
    return{x,y};
}

template <typename T>
inline constexpr T signNZ( const T& val )
{
    return T(std::copysign( T(1), val) );
}

void displayMat4(Matrix4 mat);
void displayVec3(Vector3 vec);
Vector3 getPositionMat4(Matrix4 mat);
void setPositionMat4(Matrix4 &mat, Vector3 vec);
inline Vector3 translation(Transform T) {return Vector3(T[0][3],T[1][3],T[2][3]);}
inline Matrix2 rotation(Transform T) {return Matrix2(T[0][0],T[0][1],T[1][0],T[1][1]);}


void GL_CHECK_ERROR();

#endif // MACROS_HPP
