#ifndef LOG_HPP
#define LOG_HPP

#include <iostream>

enum LogType {logError, logDebug, logWarning, logInfo};

class Log
{
public:
    Log() {}
    Log(LogType type){
        switch(type){
        case logError:
            std::cout << "\033[31m" << " [Error] ";
            break;
        case logDebug:
            std::cout << "\033[34m" << " [Debug] ";
            break;
        case logWarning:
            std::cout << "\033[35m" << " [Warning] ";
            break;
        case logInfo:
            std::cout << "\033[37m" << " [Info] ";
            break;
        }
    }

    ~Log(){
        std::cout << "\033[0m" << std::endl;
    }

    template<class T>
    Log &operator<<(const T &msg){
        std::cout << msg;
        return *this;
    }

};

#endif // LOG_HPP
