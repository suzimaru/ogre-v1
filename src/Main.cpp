#include "Gui/MainWindow.hpp"

#include <QApplication>
#include <Core/Log/Log.hpp>

int main(int argc, char *argv[]) {

    Log(logInfo) << " <<< Ogre main launch >>>";

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
