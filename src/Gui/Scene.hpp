#ifndef SCENE_H
#define SCENE_H


#include <vector>
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>

#include <Engine/Camera/Camera.hpp>
#include <Engine/Camera/EulerCamera.hpp>
#include <Engine/Renderer/Renderer.hpp>
#include <src/Engine/Renderer/ComputeEachDraw.hpp>
#include "ShortcutInterface.hpp"

/** Simple classe for managing an OpenGL scene
 */
class Scene {

public:

    explicit Scene(FileLoaderManager *flm);
    virtual ~Scene();

    virtual void cleanScene();

    virtual void resize(int width, int height);
    void draw();

    virtual void mouseclick(ButtonMouse button, float xpos, float ypos);
    virtual void mousemove(float xpos, float ypos);
    virtual void mouserelease(ButtonMouse button);
    virtual void keyboardmove(int key, double time);

    std::vector<ShortcutInterface*> shortCuts;
    ShortcutInterface * shortcut;
    int num_shortcut = -1;
    void changeShortcut();

    virtual bool keyboard(unsigned char k);
    virtual void wheelEvent(float offset);


    void toggledrawmode();
    void changeCamera();
    void changeDefaultShader(std::string s);
    void fitScene();

    void changeRadius(double d);
    void changeBiais(double d);

    void activateHdr(bool b);
    void changeGamma(double d);
    void changeExposure(double d);

    void activateFxaa(bool b);
    void changeThresholdMin(double d);
    void changeThresholdMax(double d);

    Renderer renderer;
    std::vector<ComputeEachDraw*> computeEachDraw;
    FileLoaderManager *fileLoaderManager;

protected:
    // Width and heigth of the viewport
    int _width;
    int _height;


   // bool firstMouse = true;
    //bool middlePush = false;
    float lastX[3],lastY[3];
    bool firstMouse[3];

private:
    int shaderIndex = 0;
    bool hdrActivated;
    bool fxaaActivated;

    Object *obj;
};


#endif // SCENE_H
