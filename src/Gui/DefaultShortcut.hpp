//
// Created by pierre on 28/08/18.
//

#ifndef OGRE_DEFAULTSHORTCUT_HPP
#define OGRE_DEFAULTSHORTCUT_HPP


#include <src/Engine/Renderer/Renderer.hpp>
#include "ShortcutInterface.hpp"

class DefaultShortcut : public ShortcutInterface {
public:
    DefaultShortcut();

    bool keyboard(unsigned char k) override;

};


#endif //OGRE_DEFAULTSHORTCUT_HPP
