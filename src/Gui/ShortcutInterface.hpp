//
// Created by pierre on 28/08/18.
//

#ifndef OGRE_SHORTCUTINTERFACE_HPP
#define OGRE_SHORTCUTINTERFACE_HPP


#include <string>
enum ButtonMouse{LEFT_BUTTON = 0, RIGHT_BUTTON = 1, MIDDLE_BUTTON = 2, NO_BUTTON = 3};

class ShortcutInterface {
public:
    std::string id_shortcut = "NO ID GIVEN FOR SHORTCUT";

    virtual bool keyboard(unsigned char k) = 0;
    virtual void mouseclick(ButtonMouse button, float xpos, float ypos);
    virtual void mousemove(ButtonMouse button, float xpos, float ypos);

};


#endif //OGRE_SHORTCUTINTERFACE_HPP
