#include "MainWindow.hpp"
#include "ui/ui_mainwindow.h"

#include <QMessageBox>

#include <iostream>
#include <sstream>
#include <iomanip>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
    QSurfaceFormat format;
    format.setVersion(4, 1);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setDepthBufferSize(24);
    QSurfaceFormat::setDefaultFormat(format);

    ui->setupUi(this);

    //ui->dockWidget->setVisible(false);

    ui->openglWidget->setFocus();

    tabWidget = ui->tabWidget;
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_action_Version_OpenGL_triggered() {
    std::stringstream message;
    message << "Renderer       : " << glGetString(GL_RENDERER) << std::endl;
    message << "Vendor         : " << glGetString(GL_VENDOR) << std::endl;
    message << "Version        : " << glGetString(GL_VERSION) << std::endl;
    message << "GLSL Version   : " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    QMessageBox::information(this, "OpenGL Information", message.str().c_str());
}

void MainWindow::on_actionCommande_triggered()
{
    std::stringstream message;
    message << "d : toggle draw mode\n" <<
            "u : toggle update every time \n" <<
            "c : change camera \n" <<
            "s : change default shader \n" <<
            "f : fit scene \n" <<
            "r : clean scene\n" <<
            "l : load file\n" <<
            "y : change shortcut set\n" <<
            "1-3 : See info in terminal\n" << std::endl;
    //messageBox =QMessageBox(QMessageBox::Information, "Global shortcut", message.str().c_str());
    messageBox.setIcon(QMessageBox::Information);
    messageBox.setWindowTitle("Global shortcut");
    messageBox.setText(message.str().c_str());
    messageBox.setModal(false);
    messageBox.show();

    //QMessageBox::information(this, "Global shortcut", message.str().c_str());
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    Log(logInfo) << "PASS";
    ui->openglWidget->keyPressEvent(event);

}


void MainWindow::on_actionOptions_triggered()
{
    ui->dockWidget->setVisible(true);
}

void MainWindow::on_cameraButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->changeCamera());
    ui->cameraButton->setText(t);
}

void MainWindow::on_updateButton_clicked()
{
    if(ui->openglWidget->updateToggle()){
        ui->updateButton->setText("Update");
    }else{
        ui->updateButton->setText("No Update");
    }
}

void MainWindow::on_fitButton_clicked()
{
    ui->openglWidget->fitScene();
}

void MainWindow::on_clearButton_clicked()
{
    ui->openglWidget->clearScene();
}

void MainWindow::on_shortcutButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->changeShortcut());
    ui->shortcutButton->setText(t);
}

void MainWindow::on_loadButton_clicked()
{
    ui->openglWidget->loadFile();
}

void MainWindow::on_drawmodeButton_clicked()
{
    ui->openglWidget->changeDrawMode();
}

void MainWindow::on_openglInfoButton_clicked()
{
    ui->openglWidget->displayInfo();
}

void MainWindow::on_drawdataInfoButton_clicked()
{
    ui->openglWidget->getScene()->renderer.drawData.displayInfo();
}

void MainWindow::on_rendererInfoButton_clicked()
{
    ui->openglWidget->getScene()->renderer.displayInfo();
}

void MainWindow::on_postProcessButton_clicked()
{
    QString t = QString::fromStdString(ui->openglWidget->changePostProcess());
    ui->postProcessButton->setText(t);
}

void MainWindow::on_skyboxButton_clicked()
{

    QString t = QString::fromStdString(ui->openglWidget->changeSkybox());
    ui->skyboxButton->setText(t);
}

void MainWindow::on_shaderComboBox_activated(const QString &arg1)
{
    ui->openglWidget->defaultShader(arg1.toStdString());
}



void MainWindow::on_checkBoxHdr_clicked(bool checked)
{
    ui->openglWidget->useHDR(checked);
}

void MainWindow::on_doubleSpinBoxGamma_valueChanged(double arg1)
{
    ui->openglWidget->changeGamma(arg1);
}

void MainWindow::on_doubleSpinBoxExposure_valueChanged(double arg1)
{
    ui->openglWidget->changeExposure(arg1);
}


void MainWindow::on_checkBoxFxaa_clicked(bool checked)
{
    ui->openglWidget->useFxaa(checked);
}

void MainWindow::on_doubleSpinBoxThresholdMax_valueChanged(double arg1)
{
    ui->openglWidget->changeThresholdMax(arg1);
}

void MainWindow::on_doubleSpinBoxThresholdMin_valueChanged(double arg1)
{
    ui->openglWidget->changeThresholdMin(arg1);
}

void MainWindow::on_doubleSpinBoxRadius_valueChanged(double arg1)
{
    ui->openglWidget->changeRadius(arg1);
}

void MainWindow::on_doubleSpinBoxBiais_valueChanged(double arg1)
{
    ui->openglWidget->changeBiais(arg1);
}
