#include "Scene.hpp"
#include <iostream>

#include "src/Engine/Object/Mesh.hpp"
#include "Engine/Object/Object.hpp"
#include "Core/Shapes/Sphere.hpp"
#include "DefaultShortcut.hpp"
#include <Engine/Shader/Shader.hpp>
#include <Core/Log/Log.hpp>
#include <Core/Math/Ray.hpp>

Scene::Scene(FileLoaderManager *flm) : fileLoaderManager{flm} {
    glEnable(GL_DEPTH_TEST);
    //glViewport(0, 0, width, height);
    //renderer.drawData.cameraManager.changeSize(width,height);

    for(int i = 0; i!=3; i++) firstMouse[i] = true;

    shortCuts.push_back(new DefaultShortcut());
    changeShortcut();
    renderer.changeModeRenderer(0);
    hdrActivated = false;
    renderer.useHdr(hdrActivated);

}

Scene::~Scene() {

}

void Scene::cleanScene() {
    Log(logInfo) << "Clean Scene";
    renderer.drawData.cleanScene();
    for(auto i: computeEachDraw){
        i->cleanUp();
    }
}

void Scene::resize(int width, int height) {
   _width = width;
   _height = height;
   renderer.resize(width, height);
   //glViewport(0, 0, width, height);
}

void Scene::draw() {

    for(auto i: computeEachDraw){
        i->computeEachDraw();
    }

    renderer.draw();
}

void Scene::mouseclick(ButtonMouse button, float xpos, float ypos) {
    lastX[button] = xpos;
    lastY[button] = ypos;
    firstMouse[button] = false;
    if(button==LEFT_BUTTON){
        obj=renderer.drawData.rayCast(xpos,ypos);
        if(obj!=nullptr){
            Log(logInfo) << "Ray cast: " << obj->getName();
        }else{
            Log(logInfo) << "Ray cast: NO OBJET HIT";
        }
    }
    shortcut->mouseclick(button, xpos, ypos);
}

void Scene::mouserelease(ButtonMouse button){
    firstMouse[button] = true;
}

void Scene::mousemove(float xpos, float ypos) {
    ButtonMouse button = NO_BUTTON;
    if(!firstMouse[RIGHT_BUTTON]){
        button=RIGHT_BUTTON;
    }else{
        if(!firstMouse[MIDDLE_BUTTON]){
            button=MIDDLE_BUTTON;
        }else{
            if(!firstMouse[LEFT_BUTTON]){
                button=LEFT_BUTTON;
            }
        }
    }

    float xOffset = xpos - lastX[button];
    float yOffset = lastY[button] - ypos;
    lastX[button] = xpos;
    lastY[button] = ypos;
    switch(button){
        case RIGHT_BUTTON: renderer.drawData.camera->mouseMovement(xOffset, yOffset); break;
        case LEFT_BUTTON:
            //if(obj) obj->rotate(xOffset/6,0,1,0);
            break;
        case MIDDLE_BUTTON: renderer.drawData.camera->mouseMovementMiddle(xOffset, yOffset);break;
        case NO_BUTTON: break;
    }
    shortcut->mousemove(button,xpos,ypos);
}

void Scene::wheelEvent(float offset){
    renderer.drawData.camera->mouseScroll(offset);
}

void Scene::keyboardmove(int key, double time) {
    renderer.drawData.camera->keyboard((Movement)key,time);
}

bool Scene::keyboard(unsigned char k) {
    return shortcut->keyboard(k);
}


void Scene::changeShortcut(){
    num_shortcut=(num_shortcut+1)%shortCuts.size();
    shortcut = shortCuts[num_shortcut];
    Log(logInfo) << "Shortcut activated: " << shortCuts[num_shortcut]->id_shortcut;
}

void Scene::toggledrawmode() {
    Log(logInfo) << "Toggle draw mode";
    renderer.toggleDrawMode();
}

void Scene::fitScene() {
    Log(logInfo) << "Fit Scene";
    renderer.drawData.camera->fitScene(renderer.drawData.objectManager.getAABB());
}

void Scene::changeCamera(){
    renderer.drawData.changeCamera();
}

void Scene::changeDefaultShader(std::string s) {
    shaderIndex=(shaderIndex+1)%4;
    renderer.drawData.shaderManager.remValue("default");
    if (s == "BasicLight") {
        Log(logInfo) << "Default shader : basic light";
        renderer.changeModeRenderer(0);
        renderer.drawData.shaderManager.addValue("default", new Shader("../Shaders/vertexShader.vs.glsl", "../Shaders/BasicLight.fs.glsl"));
    }
    else if (s == "Normal") {
        Log(logInfo) << "Default shader : normal";
        renderer.changeModeRenderer(0);
        renderer.drawData.shaderManager.addValue("default", new Shader("../Shaders/default.vs.glsl",
                                                                       "../Shaders/fragmentShader.fs.glsl"));
    }
    else if (s == "SSAO" ) {
        Log(logInfo) << "Default sader : SSAO";
        renderer.changeModeRenderer(1);
        renderer.drawData.shaderManager.addValue("default", new Shader("../Shaders/vertexShader.vs.glsl",
                                                                       "../Shaders/BasicLight.fs.glsl"));
    }
    else {
        Log(logError) << "Shader inexistant";
    }
}

void Scene::changeRadius(double d) {
    renderer.changeRadius(float(d));
}

void Scene::changeBiais(double d) {
    renderer.changeBiais(float(d));
}

void Scene::activateHdr(bool b) {
    hdrActivated = b;
    renderer.useHdr(hdrActivated);
}

void Scene::changeGamma(double d) {
    renderer.changeGamma(float(d));
}

void Scene::changeExposure(double d) {
    renderer.changeExposure(float(d));
}

void Scene::activateFxaa(bool b) {
    fxaaActivated = b;
    renderer.useFxaa(fxaaActivated);
}

void Scene::changeThresholdMax(double d) {
    renderer.changeThresholdMax(float(d));
}

void Scene::changeThresholdMin(double d) {
    renderer.changeThresholdMin(float(d));
}
