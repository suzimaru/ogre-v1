#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QMessageBox>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void keyPressEvent(QKeyEvent *event) override;

    QTabWidget *tabWidget;
private slots:
    void on_action_Version_OpenGL_triggered();

    void on_actionCommande_triggered();

    void on_actionOptions_triggered();

    void on_cameraButton_clicked();

    void on_updateButton_clicked();

    void on_fitButton_clicked();

    void on_clearButton_clicked();

    void on_shortcutButton_clicked();

    void on_loadButton_clicked();

    void on_drawmodeButton_clicked();

    void on_openglInfoButton_clicked();

    void on_drawdataInfoButton_clicked();

    void on_rendererInfoButton_clicked();

    void on_postProcessButton_clicked();

    void on_skyboxButton_clicked();

    void on_shaderComboBox_activated(const QString &arg1);

    void on_checkBoxHdr_clicked(bool checked);

    void on_doubleSpinBoxGamma_valueChanged(double arg1);

    void on_doubleSpinBoxExposure_valueChanged(double arg1);

    void on_checkBoxFxaa_clicked(bool checked);

    void on_doubleSpinBoxThresholdMax_valueChanged(double arg1);

    void on_doubleSpinBoxThresholdMin_valueChanged(double arg1);

    void on_doubleSpinBoxRadius_valueChanged(double arg1);

    void on_doubleSpinBoxBiais_valueChanged(double arg1);

private:
    Ui::MainWindow *ui;
    QMessageBox messageBox;

};

#endif // MAINWINDOW_H
