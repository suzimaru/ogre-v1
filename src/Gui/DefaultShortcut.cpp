//
// Created by pierre on 28/08/18.
//

#include <src/Core/Log/Log.hpp>
#include "DefaultShortcut.hpp"

DefaultShortcut::DefaultShortcut() {
    id_shortcut = "Default shortcut";
}

bool DefaultShortcut::keyboard(unsigned char k) {
    switch(k) {
        default:
            return false;
    }
}