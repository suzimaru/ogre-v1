#include "OpenGLWidget.hpp"
#include "MainWindow.hpp"

#include <Core/Log/Log.hpp>

#include <QMessageBox>
#include <QApplication>
#include <QDateTime>

#include <iostream>
#include <stdexcept>
#include <QtWidgets/QFileDialog>
#include <src/Plugin/LoaderPlugin.hpp>

namespace Ui {
    class MainWindow;
}

OpenGLWidget::OpenGLWidget(QWidget *parent) :QOpenGLWidget(parent), QOpenGLFunctions_4_1_Core(), _scene(nullptr), _lastime(0) {
    // add all demo constructors here

}

OpenGLWidget::~OpenGLWidget() {
}

void OpenGLWidget::initializeGL() {
    if (!initializeOpenGLFunctions()) {
        QMessageBox::critical(this, "OpenGL initialization error", "OpenGLWidget::initializeGL() : Unable to initialize OpenGL functions");
        exit(1);
    }

    // Initialize OpenGL and all OpenGL dependent stuff below
    _scene.reset(new Scene(fileLoaderManager));

    fileLoaderManager = new FileLoaderManager();

    LoaderPlugin loaderPlugin({fileLoaderManager,this,_scene.get(),((MainWindow*)parentWidget())->tabWidget});

    _scene->renderer.drawData.fileLoaderManager=fileLoaderManager;
    //fileLoaderManager->loadFile(&_scene->renderer.drawData, "../Assets/scenes.ogre");

    displayInfo();
    _scene->renderer.drawData.displayInfo();
    _scene->fitScene();
}

void OpenGLWidget::paintGL() {
    std::int64_t starttime = QDateTime::currentMSecsSinceEpoch();
    _scene->draw();
    glFinish();
    std::int64_t endtime = QDateTime::currentMSecsSinceEpoch();
    _lastime = endtime-starttime;
    if(updateEverytime)update();
}

void OpenGLWidget::resizeGL(int width, int height) {
    _scene->resize(width, height);
}

void OpenGLWidget::mousePressEvent(QMouseEvent *event) {
    // buttons are 0(left), 1(right) to 2(middle)
    int b;
    Qt::MouseButton button=event->button();
    if (button & Qt::LeftButton) {
        if ((event->modifiers() & Qt::ControlModifier))
            b = 2;
        else
            b = 0;
    } else if (button & Qt::RightButton)
        b = 1;
    else if (button & Qt::MiddleButton)
        b = 2;
    else
        b=3;
    _scene->mouseclick((ButtonMouse)b, event->x(), event->y());
    _lastime = QDateTime::currentMSecsSinceEpoch();
    this->setFocus();
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent *event){
    int b;
    Qt::MouseButton button=event->button();
    if (button & Qt::LeftButton) {
        if ((event->modifiers() & Qt::ControlModifier))
            b = 2;
        else
            b = 0;
    } else if (button & Qt::RightButton)
        b = 1;
    else if (button & Qt::MiddleButton)
        b = 2;
    else
        b=3;
    _scene->mouserelease((ButtonMouse)b);
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent *event) {
    _scene->mousemove(event->x(), event->y());
    update();
}

void OpenGLWidget::wheelEvent(QWheelEvent *event){
    float delta = event->delta();

    if(delta > 0) delta = 1;
    else delta = -1;
    _scene->wheelEvent(delta);
    update();
}

void OpenGLWidget::keyPressEvent(QKeyEvent *event) {
    bool used = true;
    switch(event->key()) {
        // Demo keys
        case Qt::Key_Y:
            changeShortcut();
            break;
        // Move keys
        case Qt::Key_Left:
        case Qt::Key_Up:
        case Qt::Key_Right:
        case Qt::Key_Down:
            _scene->keyboardmove(event->key()-Qt::Key_Left, 1./100/*double(_lastime)/10.*/);
            update();
        break;
        // Wireframe key
        case Qt::Key_D:
        break;
        case Qt::Key_U:
            updateToggle();
        break;
        case Qt::Key_C:
            changeCamera();
        break;
        case Qt::Key_S:
            break;
        case Qt::Key_F:
            fitScene();
            break;
        case Qt::Key_R:
            clearScene();
            break;
        case Qt::Key_L:
            loadFile();
            break;
        case Qt::Key_1:
            displayInfo();
            break;
        case Qt::Key_2:
            _scene->renderer.drawData.displayInfo();
            break;
        case Qt::Key_3:
            _scene->renderer.displayInfo();
            break;
        default :
            used = false;
        break;
    }
    if(_scene->keyboard(event->text().toStdString()[0]) && used)
        Log(logWarning) << "Watchout, shortcut '" << event->text().toStdString()[0] << "' is use in openGlWidget ...";
    update();
}

std::string OpenGLWidget::changeCamera(){
    _scene->changeCamera();
    update();
    _scene->fitScene();
    return _scene->renderer.drawData.camera->name_camera;
}

bool OpenGLWidget::updateToggle(){
    updateEverytime = ! updateEverytime;
    if(updateEverytime){
        Log(logInfo) << "Update everytime";
    }else{
        Log(logInfo) << "No Update everytime";
    }
    update();
    return updateEverytime;
}

void OpenGLWidget::fitScene(){
    _scene->fitScene();
    update();
}

void OpenGLWidget::clearScene(){
    _scene->cleanScene();
    update();
}

std::string OpenGLWidget::changeShortcut(){
    _scene->changeShortcut();
    return _scene->shortcut->id_shortcut;
}

void OpenGLWidget::loadFile(){
    QString filename;
    bool save;
    save = updateEverytime;
    updateEverytime=false;
    filename = QFileDialog::getOpenFileName(parentWidget(), QObject::tr("File loader"), "../Assets", QObject::tr("File loader (*)"));
    if(filename != "") {
        fileLoaderManager->loadFile(&_scene->renderer.drawData, filename.toStdString());
        _scene->fitScene();
    }
    updateEverytime = save;
    update();
}

void OpenGLWidget::loadFile(std::string filename) {
    fileLoaderManager->loadFile(&_scene->renderer.drawData, filename);
    _scene->fitScene();
    update();
}

void OpenGLWidget::defaultShader(std::string s){
    _scene->changeDefaultShader(s);
    update();
}

void OpenGLWidget::changeDrawMode(){
    _scene->toggledrawmode();
    update();
}

Scene* OpenGLWidget::getScene(){
    return _scene.get();
}

std::string OpenGLWidget::changePostProcess() {
    std::string t =_scene->renderer.changeShaderPost();
    update();
    return t;
}

std::string OpenGLWidget::changeSkybox() {
    std::string t =_scene->renderer.changeSkybox();
    update();
    return t;
}

void OpenGLWidget::displayInfo(){
    Log(logInfo) << "";
    Log(logInfo) << "|---------- Info OpenGL Widget ----------";
    Log(logInfo) << "| - Actual shortcut: " << _scene->shortcut->id_shortcut;
    Log(logInfo) << "| - Shortcut set number: "<< _scene->shortCuts.size();
    Log(logInfo) << "| - Update every time: " << (updateEverytime?"true":"false");
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";

}

void OpenGLWidget::changeRadius(double d) {
    _scene->changeRadius(d);
    update();
}

void OpenGLWidget::changeBiais(double d) {
    _scene->changeBiais(d);
    update();
}

void OpenGLWidget::useHDR(bool use){
    _scene->activateHdr(use);
    update();
}

void OpenGLWidget::changeGamma(double d) {
    _scene->changeGamma(d);
    update();
}

void OpenGLWidget::changeExposure(double d) {
    _scene->changeExposure(d);
    update();
}

void OpenGLWidget::useFxaa(bool use) {
    _scene->activateFxaa(use);
    update();
}

void OpenGLWidget::changeThresholdMin(double d) {
    _scene->changeThresholdMin(d);
    update();
}

void OpenGLWidget::changeThresholdMax(double d) {
    _scene->changeThresholdMax(d);
    update();
}