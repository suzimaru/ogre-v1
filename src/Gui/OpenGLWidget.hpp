#ifndef OPENGLWIDGET_HPP
#define OPENGLWIDGET_HPP

#include <QOpenGLWidget>
#include <QOpenGLFunctions_4_1_Core>
#include <QKeyEvent>

#include <memory>
#include <functional>
#include <QtWidgets/QMenu>
#include <src/Engine/Renderer/ComputeEachDraw.hpp>
#include "Scene.hpp"


class OpenGLWidget : public QOpenGLWidget, public QOpenGLFunctions_4_1_Core {

public:
    explicit OpenGLWidget(QWidget *parent = 0);

    ~OpenGLWidget();

    // OpenGL management
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;

    // Event maagement
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;

    std::string changeCamera();
    bool updateToggle();
    void fitScene();
    void clearScene();
    std::string changeShortcut();
    void loadFile();
    void loadFile(std::string filename);
    void defaultShader(std::string s);
    void changeDrawMode();
    std::string changePostProcess();
    std::string changeSkybox();
    void useHDR(bool use);

    void changeRadius(double d);
    void changeBiais(double d);

    void changeGamma(double d);
    void changeExposure(double d);

    void useFxaa(bool use);
    void changeThresholdMin(double d);
    void changeThresholdMax(double d);

    Scene* getScene();

    void displayInfo();

private :
    std::unique_ptr<Scene> _scene;
    bool updateEverytime = false;

    FileLoaderManager *fileLoaderManager;

    // for event management
    std::int64_t _lastime;
};

#endif // OpenGLWidget_H
