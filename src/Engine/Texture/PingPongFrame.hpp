#ifndef PINGPONGFRAME_HPP
#define PINGPONGFRAME_HPP


class PingPongFrame {
public:
    PingPongFrame();
    ~PingPongFrame();

    void init(int w, int h);

    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);

    unsigned int pingPongColorBuffers;


private:

    int m_width;
    int m_height;

    unsigned int m_id;

};


#endif //PINGPONGFRAME_HPP
