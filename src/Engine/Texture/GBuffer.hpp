#ifndef OGRE_GBUFFER_HPP
#define OGRE_GBUFFER_HPP


class GBuffer {
public:
    GBuffer();
    ~GBuffer();

    void init(int w, int h);
    void initPosition();
    void initNormal();


    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);
    unsigned int gPosition;
    unsigned int gNormal;


private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;

};


#endif //OGRE_GBUFFER_HPP
