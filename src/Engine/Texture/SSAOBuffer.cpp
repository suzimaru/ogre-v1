//
// Created by pierre on 31/08/18.
//

#include "SSAOBuffer.hpp"
#include <Core/Macros.hpp>

SSAOBuffer::SSAOBuffer(){
    glGenFramebuffers(1, &m_id);
    glGenTextures(1, &Color);
}

void SSAOBuffer::init(int w, int h) {
    m_width = w;
    m_height = h;
    bind();
    glBindTexture(GL_TEXTURE_2D, Color);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Color, 0);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "SSAO Framebuffer not complete!" << std::endl;
    unbind();
}

SSAOBuffer::~SSAOBuffer() {
    glDeleteFramebuffers(1, &m_id);
}

void SSAOBuffer::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
}

void SSAOBuffer::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SSAOBuffer::unbindOther(unsigned int id){
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void SSAOBuffer::resize(int w, int h) {
    init(w,h);
}