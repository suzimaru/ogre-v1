//
// Created by pierre on 31/08/18.
//

#ifndef OGRE_FRAMEBUFFER_HPP
#define OGRE_FRAMEBUFFER_HPP


class FrameBuffer {
public:
    FrameBuffer();
    ~FrameBuffer();

    void init(int w, int h);

    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);
    unsigned int textureColor;

private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;

};


#endif //OGRE_FRAMEBUFFER_HPP
