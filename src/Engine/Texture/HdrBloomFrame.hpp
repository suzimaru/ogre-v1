#ifndef HDRBLOOMFRAME_HPP
#define HDRBLOOMFRAME_HPP


class HdrBloomFrame {
public:
    HdrBloomFrame();
    ~HdrBloomFrame();

    void init(int w, int h);

    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);

    unsigned int colorBuffers[2];


private:

    int m_width;
    int m_height;

    unsigned int m_id;
    unsigned int rbo;

};


#endif //HDRBLOOMFRAME_HPP
