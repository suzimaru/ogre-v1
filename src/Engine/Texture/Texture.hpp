#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <string>

#include <Core/Log/Log.hpp>
#include <Core/Macros.hpp>

/* For the moment texture is only an image file
 * but later, texture will may be generated proceduraly */

class Texture
{
public:
    Texture(std::string filename);
    Texture(std::string directory, std::vector<std::string> faces);

    void bind(GLenum unit);
    void bindCubemap(GLenum unit);

    bool isLoadingOk();

    std::string m_filename;

private:

    unsigned int texture;
    bool loadingOk = true;

};

#endif // TEXTURE_HPP
