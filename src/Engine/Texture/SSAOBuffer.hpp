
#ifndef OGRE_SSAOBUFFER_HPP
#define OGRE_SSAOBUFFER_HPP


class SSAOBuffer {
public:
    SSAOBuffer();
    ~SSAOBuffer();

    void init(int w, int h);

    void bind();
    void unbind();
    void unbindOther(unsigned int id);

    void resize(int w, int h);
    unsigned int Color;

private:

    int m_width;
    int m_height;

    unsigned int m_id;

};


#endif //OGRE_SSAOBUFFER_HPP
