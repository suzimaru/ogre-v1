
#include "PingPongFrame.hpp"
#include <Core/Macros.hpp>
#include <stdio.h>

PingPongFrame::PingPongFrame(){
    glGenFramebuffers(1, &m_id);
    glGenTextures(1, &pingPongColorBuffers);
}


void PingPongFrame::init(int w, int h){
    m_width = w;
    m_height = h;

    bind();
    glBindTexture(GL_TEXTURE_2D, pingPongColorBuffers);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, m_width, m_height, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // we clamp to the edge as the blur filter would otherwise sample repeated texture values!
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, pingPongColorBuffers, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;
    unbind();
}

PingPongFrame::~PingPongFrame() {
    glDeleteFramebuffers(1, &m_id);
}

void PingPongFrame::bind() {
    glBindFramebuffer(GL_FRAMEBUFFER, m_id);
}

void PingPongFrame::unbind() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void PingPongFrame::unbindOther(unsigned int id){
    glBindFramebuffer(GL_FRAMEBUFFER, id);
}

void PingPongFrame::resize(int w, int h) {
    init(w,h);
}