#include "Texture.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <submodule/stb/stb_image.h>
#include <Core/Util/FileUtil.hpp>

Texture::Texture(std::string filename): m_filename{filename}
{
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // load image, create texture and generate mipmaps
    int width, height, nrChannels;
    // The FileSystem::getPath(...) is part of the GitHub repository so we can find files on any IDE/platform; replace it with your own image path.
    unsigned char *data = stbi_load(FileUtil::AbsolutePath(filename).c_str(), &width, &height, &nrChannels, 0);
    GLenum format;
    if (nrChannels == 1){
        format = GL_RED;
        Log(logWarning) << "Image with only 1 channel is not managed correctly (" << filename << ")";
    }else if (nrChannels == 3)
        format = GL_RGB;
    else if (nrChannels == 4)
        format = GL_RGBA;
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    if(!data){
        Log(logError) << "Failed to load texture:" << filename;
        loadingOk = false;
    }
    stbi_image_free(data);
}

Texture::Texture(std::string directory, std::vector<std::string> faces){
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
    int width, height, nrChannels;

    for(unsigned int i=0; i<faces.size(); i++){

        unsigned char *data = stbi_load(FileUtil::AbsolutePath(directory+'/'+faces[i]).c_str(), &width, &height, &nrChannels, 0);

        if(data){
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            stbi_image_free(data);
        }else{
            Log(logError) << "Cubemap failed to load texture:" << directory << "/" << faces[i];
            stbi_image_free(data);
        }
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

}

void Texture::bind(GLenum unit){
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_2D, texture);
}

void Texture::bindCubemap(GLenum unit) {
    glActiveTexture(unit);
    glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
}

bool Texture::isLoadingOk() {
    return loadingOk;
}