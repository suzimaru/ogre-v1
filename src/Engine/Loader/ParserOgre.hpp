//
// Created by pierre on 08/08/18.
//

#ifndef OGRE_PARSER_HPP
#define OGRE_PARSER_HPP


#include <string>
#include <vector>
#include <map>
#include <Core/Macros.hpp>
#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Gui/Scene.hpp>

using TOKENS = std::vector<std::string>;

float getFloat(TOKENS list, unsigned int n, int d = 0);
Vector3 getVector3(TOKENS list, unsigned int n, Vector3 d = Vector3(0));
std::string getString(TOKENS list, unsigned int n, std::string d = "default");

class ParserOgre
{
public:
    ParserOgre(DrawData *d);

    void processShapeSphere(TOKENS list);
    void processShapeCube(TOKENS list);
    void processShapeGrid(TOKENS list);

    void processLightDir(TOKENS list);
    void processLightSpot(TOKENS list);
    void processLightPoint(TOKENS list);
    void processMaterial(TOKENS list);
    void processTranslate(TOKENS list);
    void processRotate(TOKENS list);
    void processScale(TOKENS list);
    void processFileLoader(TOKENS list, std::string path);
    void processDefaultMaterial(TOKENS list);


    DrawData *m_drawData;

    Object *current_object=nullptr;
    std::string defaultMaterial = "default";

    void cleanUp();
};


#endif //OGRE_PARSER_HPP
