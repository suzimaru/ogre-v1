//
// Created by pierre on 03/08/18.
//

#ifndef OGRE_FILELOADER_HPP
#define OGRE_FILELOADER_HPP

#include <vector>
#include <string>
#include <src/Engine/Renderer/DrawData.hpp>

class Scene;

class FileLoader {
public:

    FileLoader();

    virtual std::vector<std::string> getFormatManaged() = 0;
    virtual void loadModel(DrawData *d, std::string path) = 0;

};


#endif //OGRE_FILELOADER_HPP
