//
// Created by pierre on 08/08/18.
//

#include <QtCore/QString>
#include <src/Core/Shapes/Sphere.hpp>
#include <src/Core/Shapes/Cube.hpp>
#include <src/Core/Shapes/MacrosShapes.hpp>
#include "ParserOgre.hpp"
#include "FileLoaderManager.hpp"
#include <Engine/Light/DirLight.hpp>
#include <Engine/Light/SpotLight.hpp>
#include <Engine/Light/PointLight.hpp>
#include <Engine/Material/MaterialGenerator.hpp>
#include <src/Gui/Scene.hpp>

class PSpring;



float getFloat(TOKENS list, unsigned int n, int d){
    if(n<list.size()){
        QString s = QString::fromStdString(list[n]);
        return s.toFloat();
    }
    return d;
}

Vector3 getVector3(TOKENS list, unsigned int n, Vector3 d){
    float x = getFloat(list,n,d.x);
    float y = getFloat(list,n+1,d.y);
    float z = getFloat(list,n+2,d.z);
    return Vector3(x,y,z);
}

std::string getString(TOKENS list, unsigned int n, std::string d){
    if(n<list.size()){
        return list[n];
    }
    return d;
}

ParserOgre::ParserOgre(DrawData *d): m_drawData{d} {
}

void ParserOgre::cleanUp(){
    current_object=nullptr;
}

void ParserOgre::processShapeSphere(TOKENS list){
    float radius = getFloat(list,2,1);
    current_object = new Object(Mesh(createSphereShape(radius)));
    m_drawData->objectManager.addValue(current_object,"sphere");
    current_object->setMaterial(defaultMaterial);

}

void ParserOgre::processShapeCube(TOKENS list){

    Vector3 c = getVector3(list,2,Vector3(1));

    current_object = new Object(Mesh(createCubeShape(c.x, c.y, c.z)));
    m_drawData->objectManager.addValue(current_object,"cube");
    current_object->setMaterial(defaultMaterial);
}

void ParserOgre::processShapeGrid(TOKENS list){
    float l = getFloat(list,2,10);
    float L = getFloat(list,3,10);
    float x = getFloat(list,4,10);
    float y = getFloat(list,5,10);

    Object *o = new Object(Mesh(createGridShape(l,L,x,y)));
    o->setMaterial("blank");
    m_drawData->objectManager.addValue(o,"grid");
    current_object = o;
}

void ParserOgre::processLightDir(TOKENS list){
    Vector3 dir = getVector3(list,2,Vector3(-1));
    Vector3 color = getVector3(list,5,Vector3(1));
    m_drawData->lightManager.addValue(new DirLight(dir,color));
}

void ParserOgre::processLightSpot(TOKENS list){
    Vector3 point = getVector3(list,2,Vector3(1));
    Vector3 dir = getVector3(list,5,Vector3(1));
    float inner = getFloat(list,8,20);
    float outer = getFloat(list,9,25);
    Vector3 color = getVector3(list,10,Vector3(1));
    m_drawData->lightManager.addValue(new SpotLight(point,dir,inner,outer,color));

}

void ParserOgre::processLightPoint(TOKENS list){
    Vector3 point = getVector3(list,2,Vector3(1));
    Vector3 color = getVector3(list,5,Vector3(1));
    Point3 att = getVector3(list,8,Point3(-1));
    if(att.x == -1) att=Point3(1,0.09,0.032);
    Log(logDebug) << att.x << att.y << att.z;
    Attenuation a{att.x,att.y,att.z};
    m_drawData->lightManager.addValue(new PointLight(point,color,a));
}

void ParserOgre::processMaterial(TOKENS list){
    std::string m = getString(list,1);
    if(current_object!= nullptr){
        current_object->setMaterial(m);
    }
}

void ParserOgre::processTranslate(TOKENS list) {
    Vector3 t = getVector3(list,1,Vector3(0));
    if(current_object!= nullptr){
        current_object->translate(t);
    }
}

void ParserOgre::processRotate(TOKENS list){
    float a = getFloat(list,1,0);
    Vector3 t = getVector3(list,2,Vector3(0));
    if(current_object!= nullptr){
        current_object->rotate(a,t);
    }
}

void ParserOgre::processScale(TOKENS list){
    Vector3 t = getVector3(list,1,Vector3(0));
    if(current_object!= nullptr){
        current_object->scale(t);
    }
}

void ParserOgre::processFileLoader(TOKENS list, std::string path){
    std::string f = getString(list,1);
    Log(logInfo) << "[OgreLoader] Load file: " << path + "/" + f;
    m_drawData->fileLoaderManager->loadFile(m_drawData , path + "/" + f);
    if(m_drawData->objectManager.getValues().size() > 0) current_object = m_drawData->objectManager.getLastValue();
}

void ParserOgre::processDefaultMaterial(TOKENS list) {
    defaultMaterial = getString(list,1,"default");
}
