//
// Created by pierre on 03/08/18.
//

#include "FileLoaderManager.hpp"
#include "AssimpLoader.hpp"
#include "OgreLoader.hpp"
#include <regex>

FileLoaderManager::FileLoaderManager() : ManagerList<FileLoader*>("FileLoaderManager") {

    addValue(new AssimpLoader());
    addValue(new OgreLoader());
}


void FileLoaderManager::loadFile(DrawData *d, std::string file){

    Log(logInfo) << IDM << "Try to load :" << file;
    for(auto i:getValues()){
        auto formats = i->getFormatManaged();
        for(auto format:formats){
            format.erase(0,1);
            format.insert(1,".*\\.");
            if(std::regex_match(file,std::regex(format))){
                i->loadModel(d,file);
                return;
            }
        }
    }
    Log(logError) << "We can't load file (format not managed) : " << file;
}