//
// Created by pierre on 03/08/18.
//

#ifndef OGRE_OGRELOADER_HPP
#define OGRE_OGRELOADER_HPP


#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Gui/Scene.hpp>
#include "FileLoader.hpp"
#include "ParserOgre.hpp"


//TODO make OgreLoader much cleaner ... (way to define child class ...)

class OgreLoader : public FileLoader {
public:
    OgreLoader();


    virtual std::vector<std::string> getFormatManaged() override;
    virtual void loadModel(DrawData *d, std::string path) override;

    // Have to be redefined for child class ...
    virtual void loadCustomData();
    virtual bool customToken(std::vector<std::string> line, int n);
    ParserOgre *m_parserOgre;
    void processFile(std::string path);
    void Error(std::string mes, int n);

    DrawData *m_drawData;

private:
    bool processLine(std::vector<std::string> line,int n);
    void processShape(std::vector<std::string> line, int n);
    void processLight(std::vector<std::string> line, int n);

    void cleanUp();

    std::string directory;
    std::string label = "";

};


#endif //OGRE_OGRELOADER_HPP
