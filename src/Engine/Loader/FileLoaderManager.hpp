//
// Created by pierre on 03/08/18.
//

#ifndef OGRE_FILELOADERMANAGER_HPP
#define OGRE_FILELOADERMANAGER_HPP


#include "FileLoader.hpp"
#include <Core/Managers/ManagerList.hpp>

class FileLoaderManager : public ManagerList<FileLoader*> {
public:
    FileLoaderManager();

    void loadFile(DrawData *d, std::string file);

};


#endif //OGRE_FILELOADERMANAGER_HPP
