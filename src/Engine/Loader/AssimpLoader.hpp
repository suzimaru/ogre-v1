//
// Created by pierre on 29/07/18.
//

#ifndef OGRE_ASSIMPLOADER_HPP
#define OGRE_ASSIMPLOADER_HPP

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <string>
#include <src/Engine/Object/Element.hpp>
#include <src/Engine/Object/Object.hpp>
#include <src/Engine/Object/ObjectManager.hpp>
#include <Engine/Renderer/DrawData.hpp>
#include "FileLoader.hpp"
#include <map>

class AssimpLoader : public FileLoader {
public:
    AssimpLoader();

    std::vector<std::string> getFormatManaged() override;

    void loadModel(DrawData *d, std::string path) override;
    void processNode(aiNode *node, const aiScene *scene);

    Element* processElement(aiMesh *mesh, const aiScene *scene, aiMatrix4x4 transform);

    Object * getObject();


    std::vector<Element *> m_elements;
    std::string directory;
private:
    Mesh processMesh(aiMesh *mesh, aiMatrix4x4 transform);
    std::string processMaterial(aiMesh *mesh, const aiScene *scene);
    std::vector<Texture*> loadMaterialTextures(aiMaterial *mat, aiTextureType type);

    DrawData *m_DrawData;
    std::vector<Texture*> textures_loaded;
    void displayNote();

    std::string fileType;
};

// Taken from Radium ...
struct Triplet {
    Triplet( const Vector3& v = Vector3() );

    Vector3 m_v;

    bool operator==( const Triplet& t ) const;
    bool operator<( const Triplet& t ) const;
};

#endif //OGRE_ASSIMPLOADER_HPP
