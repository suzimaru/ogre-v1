//
// Created by pierre on 03/08/18.
//

#include <src/Core/Util/FileReader.hpp>
#include <src/Core/Log/Log.hpp>
#include <src/Core/Util/StringUtils.hpp>
#include "OgreLoader.hpp"
#include "ParserOgre.hpp"

#define IDL "[OgreLoader] "

OgreLoader::OgreLoader() : FileLoader(){
}


std::vector<std::string> OgreLoader::getFormatManaged(){
    std::vector<std::string> formats;
    formats.push_back("*.ogre");
    return formats;
}

void OgreLoader::Error(std::string mes, int n){
    Log(logError) << IDL << "(l." << n << ") " << mes;
}

void OgreLoader::loadModel(DrawData *d, std::string path) {
    m_drawData=d;
    m_parserOgre=new ParserOgre(d);
    loadCustomData();
    processFile(path);
}

void OgreLoader::processFile(std::string path) {
    Log(logInfo) << IDL << "Try to parse :" << path;
    directory = path.substr(0, path.find_last_of('/'));
    std::string content = FileReader::getFileWithString(path);
    auto lines = StringUtils::splitString(content,'\n');
    int number_line = 1;
    bool again = true;
    bool first = true;
    std::string token;
    for(auto line : lines){
        auto dec = StringUtils::splitStringForce(line,';');               // Use ';' as '/n', separate each "line"
        for(auto d:dec) {
            auto dec2 = StringUtils::splitStringForce(d,',');             // Use "," as a separator but use same token as previous line
            first = true;
            for(auto d2:dec2){
                auto words = StringUtils::splitStringForce(d2, ' ');      // Separate token and parameters
                if(first){
                    if(d2.size()>0) token = words[0];
                    first=false;
                }else{
                    words.insert(words.begin(), token);
                }
                if(d2.size()>0) again = processLine(words,number_line);
            }
        }
        if(!again) break;
        number_line++;
    }
    if(label!="") Log(logError) << IDL << "label \"" << label << "\" was not found";
    Log(logInfo) << IDL << "Parsing Done!";
    cleanUp();
}

void OgreLoader::cleanUp() {
    m_parserOgre->cleanUp();
    label="";
    directory="";
}


void OgreLoader::processShape(std::vector<std::string> line, int n){
    std::string shape;
    if(line.size()<=1) shape = "No shape given ...";
    else shape = line[1];

    if(shape == "sphere"){
        m_parserOgre->processShapeSphere(line);
    }else if(shape == "cube") {
        m_parserOgre->processShapeCube(line);
    }else if(shape == "grid"){
        m_parserOgre->processShapeGrid(line);
    }else{
        Error("Shape unknown :" + shape, n);
    }
}

void OgreLoader::processLight(std::vector<std::string> line, int n){
    std::string light;
    if(line.size()<=1) light = "No light given ...";
    else light = line[1];
    if(light == "dir"){
        m_parserOgre->processLightDir(line);
    }else if(light == "point"){
        m_parserOgre->processLightPoint(line);
    }else if(light == "spot") {
        m_parserOgre->processLightSpot(line);
    }else{
        Error("Light unknown :" + light, n);
    }
}

bool OgreLoader::processLine(std::vector<std::string> line, int n){

    std::string token = line[0];
    if(label!=""){
        if(label + ":" == token) label = "";
        return true;
    }
    bool token_unknown = false;
    switch(token.c_str()[0]){
        case 'd':
            if(token == "defaultMaterial"){
                m_parserOgre->processDefaultMaterial(line);
            }else token_unknown = true;
            break;
        case 'E':
            if(token == "END"){
                return false;
            }else token_unknown=true;
            break;
        case 'G':
            if(token == "GOTO"){
                if(line.size()<2){
                    Error("GOTO without label",n);
                    label = "";
                } else label = line[1];
            }else token_unknown=true;
            break;
        case 'l':
            if(token == "loadfile"){
                m_parserOgre->processFileLoader(line,directory);
            }else if(token == "light"){
                processLight(line,n);
            }else token_unknown=true;
            break;
        case 'm':
            if(token == "material"){
                m_parserOgre->processMaterial(line);
            }else token_unknown=true;
            break;
        case 'r':
            if(token == "rotate"){
                m_parserOgre->processRotate(line);
            }else token_unknown=true;
            break;
        case 's':
            if(token == "shape"){
                processShape(line,n);
            }else if(token == "scale"){
                m_parserOgre->processScale(line);
            }else token_unknown=true;
            break;
        case 't':
            if(token == "translate"){
                m_parserOgre->processTranslate(line);
            }else token_unknown=true;
            break;
        case '/': return true;
        default: token_unknown=true; break;
    }
    if(token_unknown) token_unknown = customToken(line,n);
    if(token_unknown){
        Error("Token unknown :" + token , n);
    }
    return true;
}

bool OgreLoader::customToken(std::vector<std::string>, int) {
    // Redefine this on child class
    return true;
}

void OgreLoader::loadCustomData(){
    // Redefine this on child class
}

