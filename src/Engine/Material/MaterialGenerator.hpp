//
// Created by pierre on 25/08/18.
//

#ifndef OGRE_MATERIALGENERATOR_HPP
#define OGRE_MATERIALGENERATOR_HPP

#include "MaterialManager.hpp"

std::string kdMaterialGeneratorN(int number, MaterialManager *mm);
std::string kdMaterialGenerator(MaterialManager *mm);

#endif //OGRE_MATERIALGENERATOR_HPP
