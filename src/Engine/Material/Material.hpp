#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include "../Texture/Texture.hpp"
#include "../Shader/Shader.hpp"

struct DataForDrawing;

class Material
{
public:
    Material(Vector3 kd, Vector3 ks = Vector3(1), float ns = 5);
    Material(std::string kd);
    Material(std::string kd, std::string ks);

    void setKd(Vector3 kd);
    void setKs(Vector3 ks);
    void setNs(float ns);

    void setKdTexture(Texture *tex);
    void setKsTexture(Texture *tex);
    void setNsTexture(Texture *tex);
    void setKdTexture(std::string tex);
    void setKsTexture(std::string tex);
    void setNsTexture(std::string tex);

    virtual void bind(Shader *shader, DataForDrawing data);

    void setShader(std::string str);
    std::string getNameShader();

private:


    Vector3 m_kd;
    Vector3 m_ks;
    float m_ns;

    bool m_hasKdTexture = false;
    bool m_hasKsTexture = false;
    bool m_hasNsTexture = false;

    Texture *m_KdTexture;
    Texture *m_KsTexture;
    Texture *m_NsTexture;

    float m_ambient;

    std::string m_nameShader = "default";

};

#endif // MATERIAL_HPP
