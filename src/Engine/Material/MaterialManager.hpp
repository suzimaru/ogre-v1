//
// Created by pierre on 26/07/18.
//

#ifndef OGRE_MATERIALMANAGER_HPP
#define OGRE_MATERIALMANAGER_HPP

#include <Core/Managers/ManagerMap.hpp>
#include <Engine/Material/Material.hpp>

class MaterialManager : public ManagerMap<Material*> {
public:
    MaterialManager();

    void displayInfo();

};


#endif //OGRE_MATERIALMANAGER_HPP
