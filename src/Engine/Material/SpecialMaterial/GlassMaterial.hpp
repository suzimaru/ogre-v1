//
// Created by pierre on 01/09/18.
//

#ifndef OGRE_GLASSMATERIAL_HPP
#define OGRE_GLASSMATERIAL_HPP

#include <src/Engine/Material/Material.hpp>
#include <src/Engine/Object/Element.hpp>

class GlassMaterial : public Material {
public:

    GlassMaterial();

    void bind(Shader * shader, DataForDrawing data);

    Texture *skybox;

};


#endif //OGRE_GLASSMATERIAL_HPP
