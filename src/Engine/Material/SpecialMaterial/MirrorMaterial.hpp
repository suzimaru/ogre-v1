//
// Created by pierre on 01/09/18.
//

#ifndef OGRE_MIRRORMATERIAL_HPP
#define OGRE_MIRRORMATERIAL_HPP


#include <src/Engine/Material/Material.hpp>
#include <src/Engine/Object/Element.hpp>

class MirrorMaterial : public Material {
public:

    MirrorMaterial();

    void bind(Shader * shader, DataForDrawing data);

    Texture *skybox;

};


#endif //OGRE_MIRRORMATERIAL_HPP
