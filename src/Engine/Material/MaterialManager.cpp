//
// Created by pierre on 26/07/18.
//

#include "MaterialManager.hpp"

MaterialManager::MaterialManager() : ManagerMap<Material*>("MaterialManager") {

}

void MaterialManager::displayInfo() {
    Log(logInfo) << "| - Material number: " << getValues().size();
}