#ifndef SHADERMANAGER_HPP
#define SHADERMANAGER_HPP

#include <Engine/Shader/Shader.hpp>
#include <Core/Managers/ManagerMap.hpp>
#include <vector>
#include <map>
#include <string>

class ShaderManager: public ManagerMap<Shader*>
{
public:
    ShaderManager();

    void displayInfo();

    /*void addShader(std::string name, Shader *shader);
    Shader * getShader(std::string name);

    std::map<std::string, Shader*> mapShader;*/

};

#endif // SHADERMANAGER_HPP
