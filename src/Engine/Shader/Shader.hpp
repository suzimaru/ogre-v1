#ifndef SHADER_HPP
#define SHADER_HPP

#include <Core/Macros.hpp>

class Shader{
public:

    Shader(std::string vertexFile, std::string fragmentFile);

    void use();

    void loadDefaultShader(int num);

    void loadShader(const char* vertexshader_source,const char* fragmentshader_source);

    void setBool(const std::string &name, const bool value) const;
    void setInt(const std::string &name, const int value) const;
    void setFloat(const std::string &name, const float value) const;
    void setVec2(const std::string &name, const Vector2 &vec) const;
    void setVec3(const std::string &name, const Vector3 &vec) const;
    void setVec4(const std::string &name, const Vector4 &vec) const;
    void setMat2(const std::string &name, const Matrix2 &mat) const;
    void setMat3(const std::string &name, const Matrix3 &mat) const;
    void setMat4(const std::string &name, const Matrix4 &mat) const;

    std::string preprocessIncludes(std::string& shader);
private:
    GLuint m_program;


};


#endif // SHADER_HPP
