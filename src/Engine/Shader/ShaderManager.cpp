#include "ShaderManager.hpp"

ShaderManager::ShaderManager() : ManagerMap<Shader*>("ShaderManager")
{

}

void ShaderManager::displayInfo() {
    Log(logInfo) << "| - Shader number: " << getValues().size();
}

/*
void ShaderManager::addShader(std::string name, Shader *shader){
    mapShader[name]=shader;
}

Shader * ShaderManager::getShader(std::string name){
    return mapShader[name];
}
*/