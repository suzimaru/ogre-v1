//
// Created by pierre on 24/07/18.
//

#ifndef OGRE_DIRLIGHT_HPP
#define OGRE_DIRLIGHT_HPP


#include "Light.hpp"

class DirLight : public Light {
public:

    DirLight(Vector3 direction = Vector3(1,1,1), Color color = Color(1,1,1));
    //DirLight(Point3 from = Point3(1,1,1), Point3 to = Point3(0,0,0), Color color = Color(1,1,1));

    void bind(Shader *shader, int number) override;

    void displayInfo() override;

private:
    Vector3 m_direction;

};


#endif //OGRE_DIRLIGHT_HPP
