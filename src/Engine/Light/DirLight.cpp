//
// Created by pierre on 24/07/18.
//

#include <src/Core/Log/Log.hpp>
#include "DirLight.hpp"



DirLight::DirLight(Vector3 direction, Color color):
        Light(DIR_LIGHT, color), m_direction{direction} {

}

/*DirLight::DirLight(Point3 from, Point3 to, Color color):
        Light(DIR_LIGHT, color), m_direction{glm::normalize(to-from)}{

}*/

void DirLight::bind(Shader *shader, int number) {
    Light::bind(shader,number);
    std::string num = Construct_number(number);
    shader->setVec3(num + "dirLight.direction",m_direction);
}

void DirLight::displayInfo() {
    Log(logInfo) << "| ---------------- Light ----------------";
    Log(logInfo) << "| - Type : DirLight";
    Log(logInfo) << "| - Color: " << m_color.x << " " << m_color.y << " " << m_color.z;
    Log(logInfo) << "| - Direction: " << m_direction.x << " " << m_direction.y << " " << m_direction.z;
}