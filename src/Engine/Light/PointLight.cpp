//
// Created by pierre on 24/07/18.
//

#include "PointLight.hpp"

PointLight::PointLight(Point3 position, Color color, Attenuation attenuation) : Light(POINT_LIGHT, color), m_attenuation{attenuation}, m_position{position}
{

}

void PointLight::bind(Shader *shader, int number){
    Light::bind(shader,number);
    std::string num = Construct_number(number);
    shader->setVec3(num + "pointLight.position",m_position);
    shader->setFloat(num + "pointLight.attenuation.constant",m_attenuation.constant);
    shader->setFloat(num + "pointLight.attenuation.linear",m_attenuation.linear);
    shader->setFloat(num + "pointLight.attenuation.quadratic",m_attenuation.quadratic);
}

void PointLight::displayInfo() {
    Log(logInfo) << "| ---------------- Light ----------------";
    Log(logInfo) << "| - Type : PointLight";
    Log(logInfo) << "| - Color: " << m_color.x << " " << m_color.y << " " << m_color.z;
    Log(logInfo) << "| - Position: " << m_position.x << " " << m_position.y << " " << m_position.z;
}