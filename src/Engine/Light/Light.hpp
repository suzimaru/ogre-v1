//
// Created by pierre on 22/07/18.
//

#ifndef OGRE_LIGHT_HPP
#define OGRE_LIGHT_HPP

#include <Core/Macros.hpp>
#include <src/Engine/Shader/Shader.hpp>

struct Attenuation{
    float constant;
    float linear;
    float quadratic;
};

class Light {
public:

    enum light_type{POINT_LIGHT = 0, SPOT_LIGHT = 1, DIR_LIGHT = 2};

    Light(light_type type, Color color = Color(1,1,1));

    virtual void bind(Shader *shader, int number) = 0;

    /* Temporary function, until the rendering is done in an other way
     * Not just give to shader all the light in one time */
    std::string Construct_number(int n);

    light_type m_type;
    Color m_color;

    virtual void displayInfo() = 0;
};


#endif //OGRE_LIGHT_HPP
