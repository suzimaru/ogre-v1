//
// Created by pierre on 22/07/18.
//

#ifndef OGRE_LIGHTMANAGER_HPP
#define OGRE_LIGHTMANAGER_HPP


#include <src/Engine/Shader/Shader.hpp>
#include <Core/Managers/ManagerList.hpp>
#include "Light.hpp"

class LightManager : public ManagerList<Light*> {
public:
    LightManager();

    void bind(Shader *shader);

    void displayInfo();
    void displayInfo2();

    Light *defaultLight;

    /*std::vector<Light*> getLights();

    std::vector<Light*> lightList;

    void addLight(Light *light);*/
};


#endif //OGRE_LIGHTMANAGER_HPP
