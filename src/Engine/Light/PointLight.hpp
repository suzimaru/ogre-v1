//
// Created by pierre on 24/07/18.
//

#ifndef OGRE_POINTLIGHT_HPP
#define OGRE_POINTLIGHT_HPP


#include "Light.hpp"

class PointLight : public Light {
public:
    PointLight(Point3 position = Point3(0,0,0), Color color = Color(1,1,1), Attenuation attenuation = Attenuation{1,0.09,0.032});


    void bind(Shader *shader, int number) override;

    void displayInfo();

private:
    Attenuation m_attenuation;
    Point3 m_position;

};


#endif //OGRE_POINTLIGHT_HPP
