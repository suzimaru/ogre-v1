//
// Created by pierre on 22/07/18.
//

#include <src/Core/Log/Log.hpp>
#include <QtCore/QString>
#include "Light.hpp"

Light::Light(light_type type, Color color) : m_type{type}, m_color{color}
{}

std::string Light::Construct_number(int n) {
    return "lights[" + std::to_string(n) + "].";
}

void Light::bind(Shader *shader, int number){
    std::string num = Construct_number(number);
    shader->setInt(num + "type",m_type);
    shader->setVec3(num + "color", m_color);
}
