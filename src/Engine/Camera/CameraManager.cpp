#include "CameraManager.hpp"

#include <Engine/Camera/EulerCamera.hpp>
#include <Engine/Camera/Camera2D.hpp>
#include <Engine/Camera/TrackBall.hpp>
#include <Core/Log/Log.hpp>

CameraManager::CameraManager()
{
    actualCamera = new EulerCamera();
    listCamera.push_back(actualCamera);
    listCamera.push_back(new Camera2D());
    listCamera.push_back(new TrackBall());
    num_camera=0;

}


Camera * CameraManager::getCamera(){
    return actualCamera;
}

Camera * CameraManager::nextCamera(){
    num_camera = (num_camera + 1) % listCamera.size();
    actualCamera = listCamera[num_camera];
    Log(logInfo) << "Actual Camera: " << actualCamera->name_camera;
    return actualCamera;
}

void CameraManager::changeSize(int width, int height){
    for(auto i: listCamera){
        i->resizeCamera(width,height);
    }
}

void CameraManager::displayInfo(){
    Log(logInfo) << "| - Camera number: " << listCamera.size();
}