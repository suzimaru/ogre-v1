#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <Core/Macros.hpp>
#include <src/Core/Math/Ray.hpp>

#define ZOOM 45
#define SPEED 2.5
#define SENSITIVITY 0.1

enum Movement{FORWARD = 1, BACKWARD = 3, LEFT = 0, RIGHT = 2};

class Camera
{
public:
    Camera(Vector3 position = Vector3(1,0,0), Vector3 front = Vector3(-1,0,0), Vector3 worldUp = Vector3(0,1,0));

    virtual void mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint = true) = 0;
    virtual void mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) = 0;
    virtual void mouseScroll(GLfloat offset) = 0;
    virtual void keyboard(Movement movement, GLfloat deltatime) = 0;
    virtual void updateCameraVectors() = 0;

    virtual void fitScene(AABB aabb) = 0;
    Ray getRayFromScreen(GLfloat x, GLfloat y);

    void updateCameraVectorsInit();

    Matrix4 GetViewMatrix();
    Matrix4 GetProjMatrix();
    Matrix4 GetProjViewMatrix();

    Vector3 m_position;
    Vector3 m_front;
    Vector3 m_up;
    Vector3 m_right;
    Vector3 m_worldUp;
    float m_movementSpeed;
    float m_mouseSensitivity;
    float m_factorDistance;
    float m_zoom;

    std::string name_camera;

    void resizeCamera(int width, int height);
    int m_width, m_height;
    float getAspect();
    float getFOV();

private:
    float m_zNear;
    float m_zFar;
};

#endif // CAMERA_HPP
