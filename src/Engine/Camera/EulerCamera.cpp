#include "EulerCamera.hpp"

#include <Core/Log/Log.hpp>
#include <Core/Math/EigenToGlm.hpp>

EulerCamera::EulerCamera(Vector3 position, Vector3 front, Vector3 worldUp, float yaw, float pitch):
    Camera(position, front, worldUp),
    m_yaw{yaw}, m_pitch{pitch}
{
    updateCameraVectors();
    name_camera="EulerCamera";
}

void EulerCamera::mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint){
    xOffset *= m_mouseSensitivity;
    yOffset *= m_mouseSensitivity;

    m_yaw += xOffset;
    m_pitch += yOffset;

    if(constraint){
        if(m_pitch>89) m_pitch = 89;
        if(m_pitch<-89) m_pitch = -89;
    }

    updateCameraVectors();
}

void EulerCamera::mouseMovementMiddle(GLfloat, GLfloat yOffset) {
    yOffset *= m_mouseSensitivity;
    m_position += yOffset*m_front;
}

void EulerCamera::mouseScroll(GLfloat offset){
    m_zoom -= offset;
    if(m_zoom <= 1) m_zoom = 1;
    if(m_zoom >= 90) m_zoom = 90;
}

void EulerCamera::keyboard(Movement movement, GLfloat deltatime){
    float velocity = m_movementSpeed * deltatime * m_factorDistance;
    switch(movement){
    case FORWARD: m_position += m_front * velocity; break;
    case BACKWARD: m_position -= m_front * velocity; break;
    case LEFT: m_position -= m_right * velocity; break;
    case RIGHT: m_position += m_right * velocity; break;
    }
}

void EulerCamera::updateCameraVectors(){
    Vector3 front;
    float yaw = glm::radians(m_yaw);
    float pitch = glm::radians(m_pitch);
    front.x = std::cos(yaw) * std::cos(pitch);
    front.y = std::sin(pitch);
    front.z = std::sin(yaw) * std::cos(pitch);
    m_front = glm::normalize(front);
    m_right = glm::normalize(glm::cross(m_front,m_worldUp));
    m_up = glm::normalize(glm::cross(m_right,m_front));
}

void EulerCamera::fitScene(AABB aabb){

    m_yaw = YAW;
    m_pitch = PITCH;

    m_worldUp = Vector3(0,1,0);

    float fovH = degToRad( getAspect()* getFOV() );
    float fov = degToRad( getFOV() );

    float r = (aabb.max() - aabb.min()).norm()/2.0;
    float x = r / std::tan(fov/2.0);
    float y = r / std::tan(fovH/2.0);

    float d = std::max(std::max(x,y), (float)0.001);
    m_factorDistance = d;

    updateCameraVectors();

    m_position = ETG_VECTOR3(aabb.center()) -d * m_front;
}
