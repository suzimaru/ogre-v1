#ifndef CAMERAMANAGER_HPP
#define CAMERAMANAGER_HPP

#include <vector>

#include <Engine/Camera/Camera.hpp>

class CameraManager
{
public:
    CameraManager();

    Camera * getCamera();
    Camera * nextCamera();
    void changeSize(int width, int height);

    void displayInfo();

private:
    std::vector<Camera*> listCamera;

    Camera *actualCamera;
    int num_camera;

};

#endif // CAMERAMANAGER_HPP
