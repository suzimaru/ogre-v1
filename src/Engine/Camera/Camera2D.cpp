#include <src/Core/Log/Log.hpp>
#include "Camera2D.hpp"
#include <Core/Math/EigenToGlm.hpp>

#define SENSITIVITY_CAMERA2D 0.006

Camera2D::Camera2D(Vector3 position, Vector3 front, Vector3 worldUp):
    Camera(position, front, worldUp)
{
    m_mouseSensitivity = SENSITIVITY_CAMERA2D;
    name_camera="Camera2D";
}

void Camera2D::mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean){
    xOffset *= m_mouseSensitivity * (m_factorDistance/10);
    yOffset *= m_mouseSensitivity * (m_factorDistance/10);

    m_position += xOffset*m_right + yOffset*m_up;

}

void Camera2D::mouseMovementMiddle(GLfloat, GLfloat yOffset) {
    yOffset *= m_mouseSensitivity * m_factorDistance;
    m_position += yOffset*m_front;
}

void Camera2D::mouseScroll(GLfloat offset){
    m_zoom -= offset;
    if(m_zoom <= 1) m_zoom = 1;
    if(m_zoom >= 45) m_zoom = 45;
}

void Camera2D::keyboard(Movement movement, GLfloat deltatime){
    float velocity = m_movementSpeed * deltatime;
    switch(movement){
    case FORWARD: m_position += m_up * velocity; break;
    case BACKWARD: m_position -= m_up * velocity; break;
    case LEFT: m_position -= m_right * velocity; break;
    case RIGHT: m_position += m_right * velocity; break;
    }
}

void Camera2D::updateCameraVectors(){
}

void Camera2D::fitScene(AABB aabb){
    m_worldUp = Vector3(0,1,0);

    float fovH = degToRad( getAspect()* getFOV() );
    float fov = degToRad( getFOV() );

    float r = (aabb.max() - aabb.min()).norm()/2.0;
    float x = r / std::tan(fov/2.0);
    float y = r / std::tan(fovH/2.0);

    float d = std::max(std::max(x,y), (float)0.001);
    m_factorDistance = d;

    m_position = ETG_VECTOR3(aabb.center()) -d * m_front;
}