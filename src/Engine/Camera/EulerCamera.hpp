#ifndef EULERCAMERA_HPP
#define EULERCAMERA_HPP

#include "Camera.hpp"

#define YAW -90
#define PITCH 0

class EulerCamera : public Camera
{
public:
    EulerCamera(Vector3 position = Vector3(0,0,5),
                Vector3 front = Vector3(0,0,-1),
                Vector3 worldUp = Vector3(0,1,0),
                float yaw = YAW, float pitch = PITCH);

    void mouseMovement(GLfloat xOffset, GLfloat yOffset, GLboolean constraint = true) override;
    void mouseMovementMiddle(GLfloat xOffset, GLfloat yOffset) override;
    void mouseScroll(GLfloat offset) override;
    void keyboard(Movement movement, GLfloat deltatime) override;
    void updateCameraVectors() override;

    void fitScene(AABB aabb) override;

    float m_yaw, m_pitch;
};

#endif // EULERCAMERA_HPP
