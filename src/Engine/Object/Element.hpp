#ifndef ELEMENT_HPP
#define ELEMENT_HPP

#include <src/Engine/Camera/Camera.hpp>
#include <src/Engine/Light/LightManager.hpp>
#include <Engine/Material/MaterialManager.hpp>
#include <src/Engine/Shader/ShaderManager.hpp>
#include "Mesh.hpp"

struct DataForDrawing{
    LightManager *lightManager;
    ShaderManager *shaderManager;
    MaterialManager *materialManager;
    Camera *camera;
    unsigned int textureSSAO;
    bool ssaoActivated;
    Texture *skybox;
};

class Element
{
public:
    Element(Mesh mesh = Mesh());
    void draw(DataForDrawing data, Matrix4 model);
    void updateData();

    void setMaterial(std::string nameMaterial);

    Mesh m_mesh;
    void setMeshTypes(GLenum t);

    std::string m_materialName = "default";

    void displayInfo();
};

#endif // ELEMENT_HPP