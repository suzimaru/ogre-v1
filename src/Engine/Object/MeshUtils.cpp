#include <src/Core/Log/Log.hpp>
#include "MeshUtils.hpp"
#include "Decimater.hpp"
#include <stdio.h>
#include <iostream>

MeshUtils::Mesh_OM MeshUtils::basicMesh2MeshOM(Mesh meshIn) {
    Mesh_OM meshOut;
    std::cout << "basic to om" << std::endl;
    //generate vertices
    std::vector<Mesh_OM::VertexHandle> vhandle;
    for (auto it = meshIn.m_vertices.begin(); it!=meshIn.m_vertices.end(); ++it) {
        vhandle.push_back(meshOut.add_vertex(Mesh_OM::Point(it->x,it->y,it->z)));
    }

    //generate triangle surfaces
    std::vector<Mesh_OM::VertexHandle> face_vhandles;

    for (unsigned int i=0; i<meshIn.m_indices.size(); i+=3) {
        face_vhandles.clear();
        face_vhandles.push_back(vhandle[meshIn.m_indices[i]]);
        face_vhandles.push_back(vhandle[meshIn.m_indices[i+1]]);
        face_vhandles.push_back(vhandle[meshIn.m_indices[i+2]]);
        meshOut.add_face(face_vhandles);
    }
    return meshOut;
}

Mesh MeshUtils::meshOM2BasicMesh(Mesh_OM meshIn) {
    Mesh meshOut;
    std::cout << "om to basic" << std::endl;
    for (auto it = meshIn.vertices_begin(); it != meshIn.vertices_end(); ++it) {
        MeshUtils::Mesh_OM::Point pt = meshIn.point(it);
        float * d = pt.data();
        meshOut.m_vertices.push_back(Point3(d[0],d[1],d[2]));
        meshOut.m_texCoord.push_back(Vector3(0,0,0));

    }

    for (auto it1 = meshIn.faces_begin(); it1!=meshIn.faces_end() ;++it1) {
        for (auto it2 = meshIn.fv_iter(it1); it2.is_valid(); ++it2) {
            meshOut.m_indices.push_back(it2->idx());
        }
    }

    meshOut.m_normals.clear();
    meshOut.autoNormals();

    return meshOut;
}


///////////////////////////////////////////////// SUBDIVISION ///////////////////////////////////////////////////


void MeshUtils::loopSubdivision(Mesh_OM &meshIn) {

    OpenMesh::VPropHandleT<Mesh_OM::Point> newPosV;
    OpenMesh::EPropHandleT<Mesh_OM::Point> newPosE;

    meshIn.add_property(newPosV);
    meshIn.add_property(newPosE);

    // Compute updated positions for all old vertices in the original mesh
    // using the vertex subdivision rule, and store them in newPosV

    for (auto it = meshIn.vertices_begin(); it!=meshIn.vertices_end(); ++it) {
        Mesh_OM::Point pos(0.0,0.0,0.0);

        if (meshIn.is_boundary(it) ) // if boundary: Point 1-6-1
        {
            typename Mesh_OM::HalfedgeHandle heh, prev_heh;
            heh      = meshIn.halfedge_handle( it );

            if ( heh.is_valid() )
            {
                prev_heh = meshIn.prev_halfedge_handle( heh );

                typename Mesh_OM::VertexHandle
                        to_vh   = meshIn.to_vertex_handle( heh ),
                        from_vh = meshIn.from_vertex_handle( prev_heh );

                // ( v_l + 6 v + v_r ) / 8
                pos  = meshIn.point( it );
                pos *= (6.0);
                pos += Mesh_OM::Normal( meshIn.point( to_vh ) );
                pos += Mesh_OM::Normal( meshIn.point( from_vh ) );
                pos *= (1.0/8.0);
            }
            else
                return;
        }
        else { // inner vertex: (1-a) * p + a/n * Sum q, q in one-ring of p
            int valence=0;
            typedef typename Mesh_OM::Normal   Vec;
            for (auto itNeighbors = meshIn.vv_iter(it); itNeighbors.is_valid(); ++itNeighbors)
            {
                ++valence;
                pos+=OpenMesh::vector_cast< Vec >(meshIn.point(*itNeighbors));
            }
            if (valence != 0) {
                double   inv_v  = 1.0/double(valence);
                double   t      = (3.0 + 2.0 * cos( 2.0 * M_PI * inv_v) );
                double   alpha  = (40.0 - t * t)/64.0;
                pos *= (inv_v*alpha);
                pos += (1.0-alpha) * meshIn.point(it);
            } else
                pos = meshIn.point(it);
        }
        meshIn.property(newPosV,it) = pos;
    }




    // Compute new positions associated with the new vertices that will be inserted at
    // edge midpoints, and store them in newPosE.
    std::vector<Mesh_OM::VertexHandle> newPositionEdges;
    //copie des indices/faces
    for (auto it = meshIn.edges_begin(); it!=meshIn.edges_end() ;++it) {
        Mesh_OM::HalfedgeHandle halfEdge, oppHaflEdge;
        halfEdge = meshIn.halfedge_handle(it,0);
        oppHaflEdge = meshIn.halfedge_handle(it,1);

        Mesh_OM::Point newPt = meshIn.point(meshIn.to_vertex_handle(halfEdge));
        newPt += meshIn.point(meshIn.to_vertex_handle(oppHaflEdge));

        //bord ??
        if (meshIn.is_boundary(it)) {
            //newPt = meshIn.calc_edge_midpoint(it);
            newPt *= 0.5;
        }
        else {
            newPt *= (3.0);
            newPt += Mesh_OM::Normal(meshIn.point(meshIn.to_vertex_handle(meshIn.next_halfedge_handle(halfEdge))));
            newPt += Mesh_OM::Normal(meshIn.point(meshIn.to_vertex_handle(meshIn.next_halfedge_handle(oppHaflEdge))));
            newPt *= (1.0/8.0);
        }
        meshIn.property(newPosE,it) = newPt;
    }


    //split edge
    auto edgesEnd = meshIn.edges_end();
    for (auto it = meshIn.edges_begin(); it != edgesEnd; ++it) {

        Mesh_OM::HalfedgeHandle halfEdge, oppHaflEdge;
        halfEdge = meshIn.halfedge_handle(it,0);
        oppHaflEdge = meshIn.halfedge_handle(it,1);

        Mesh_OM::HalfedgeHandle newHaflEdge, newOppHaflEdge, tmp;
        Mesh_OM::VertexHandle vhandle;
        Mesh_OM::VertexHandle vhandle1(meshIn.to_vertex_handle(halfEdge));
        Mesh_OM::Point midPoint(meshIn.point(meshIn.to_vertex_handle(halfEdge)));
        midPoint += meshIn.point(meshIn.to_vertex_handle(oppHaflEdge));
        midPoint *= (0.5);

        vhandle = meshIn.new_vertex(midPoint);
        //memorize position, will be set later
        meshIn.property(newPosV,vhandle) = meshIn.property(newPosE,it);

        if (meshIn.is_boundary(it)) {
            for (tmp = halfEdge; meshIn.next_halfedge_handle(tmp) != oppHaflEdge; tmp = meshIn.opposite_halfedge_handle(meshIn.next_halfedge_handle(tmp))) {

            }
        }
        else {
            for (tmp = meshIn.next_halfedge_handle(oppHaflEdge); meshIn.next_halfedge_handle(tmp) != oppHaflEdge; tmp = meshIn.next_halfedge_handle(tmp)) {

            }
        }
        newHaflEdge = meshIn.new_edge(vhandle,vhandle1);
        newOppHaflEdge = meshIn.opposite_halfedge_handle(newHaflEdge);
        meshIn.set_vertex_handle(halfEdge,vhandle);

        meshIn.set_next_halfedge_handle(tmp,newOppHaflEdge);
        meshIn.set_next_halfedge_handle(newHaflEdge,meshIn.next_halfedge_handle(halfEdge));
        meshIn.set_next_halfedge_handle(halfEdge,newHaflEdge);
        meshIn.set_next_halfedge_handle(newOppHaflEdge,oppHaflEdge);

        if (meshIn.face_handle(oppHaflEdge).is_valid()) {
            meshIn.set_face_handle(newOppHaflEdge,meshIn.face_handle(oppHaflEdge));
            meshIn.set_halfedge_handle(meshIn.face_handle(newOppHaflEdge),newOppHaflEdge);
        }

        meshIn.set_face_handle(newHaflEdge,meshIn.face_handle(halfEdge));
        meshIn.set_halfedge_handle(vhandle,newHaflEdge);
        meshIn.set_halfedge_handle(meshIn.face_handle(halfEdge),halfEdge);
        meshIn.set_halfedge_handle(vhandle1,newOppHaflEdge);

        meshIn.adjust_outgoing_halfedge(vhandle);
        meshIn.adjust_outgoing_halfedge(vhandle1);

    }


    // Split face
    auto facesEnd = meshIn.faces_end();
    for (auto it = meshIn.faces_begin(); it != facesEnd; ++it) {
        Mesh_OM::HalfedgeHandle halfEdge1(meshIn.halfedge_handle(it));
        Mesh_OM::HalfedgeHandle halfEdge2(meshIn.next_halfedge_handle(meshIn.next_halfedge_handle(halfEdge1)));
        Mesh_OM::HalfedgeHandle halfEdge3(meshIn.next_halfedge_handle(meshIn.next_halfedge_handle(halfEdge2)));

        //cutting off every corner of the 6gon.
        corner_cutting(meshIn, halfEdge1);
        corner_cutting(meshIn, halfEdge2);
        corner_cutting(meshIn, halfEdge3);
    }

    for (auto it = meshIn.vertices_begin(); it != meshIn.vertices_end(); ++it) {
        meshIn.set_point(it,meshIn.property(newPosV,it));
    }



}


void MeshUtils::corner_cutting(Mesh_OM& meshIn, const Mesh_OM::HalfedgeHandle& halfEdge)
{
    // Define Halfedge Handles
    Mesh_OM::HalfedgeHandle halfEdge1(halfEdge), halfEdge5(halfEdge1), halfEdge6(meshIn.next_halfedge_handle(halfEdge1));

    // Cycle around the polygon to find correct Halfedge
    for (; meshIn.next_halfedge_handle(meshIn.next_halfedge_handle(halfEdge5)) != halfEdge1; halfEdge5 = meshIn.next_halfedge_handle(halfEdge5))
    {}

    Mesh_OM::VertexHandle
            vhandle1 = meshIn.to_vertex_handle(halfEdge1),
            vhandle2 = meshIn.to_vertex_handle(halfEdge5);


    Mesh_OM::HalfedgeHandle halfEdge2(meshIn.next_halfedge_handle(halfEdge5));
    Mesh_OM::HalfedgeHandle halfEdge3(meshIn.new_edge( vhandle1, vhandle2));
    Mesh_OM::HalfedgeHandle halfEdge4(meshIn.opposite_halfedge_handle(halfEdge3));

    /* Intermediate result
     *
     *            *
     *         5 /|\
     *          /_  \
     *    vh2> *     *
     *        /|\3   |\
     *       /_  \|4   \
     *      *----\*----\*
     *          1 ^   6
     *            vh1 (adjust_outgoing halfedge!)
     */

    // Old and new Face
    Mesh_OM::FaceHandle faceHandler_old(meshIn.face_handle(halfEdge6));
    Mesh_OM::FaceHandle faceHandler_new(meshIn.new_face());


    // Re-Set Handles around old Face
    meshIn.set_next_halfedge_handle(halfEdge4, halfEdge6);
    meshIn.set_next_halfedge_handle(halfEdge5, halfEdge4);

    meshIn.set_face_handle(halfEdge4, faceHandler_old);
    meshIn.set_face_handle(halfEdge5, faceHandler_old);
    meshIn.set_face_handle(halfEdge6, faceHandler_old);
    meshIn.set_halfedge_handle(faceHandler_old, halfEdge4);

    // Re-Set Handles around new Face
    meshIn.set_next_halfedge_handle(halfEdge1, halfEdge3);
    meshIn.set_next_halfedge_handle(halfEdge3, halfEdge2);

    meshIn.set_face_handle(halfEdge1, faceHandler_new);
    meshIn.set_face_handle(halfEdge2, faceHandler_new);
    meshIn.set_face_handle(halfEdge3, faceHandler_new);

    meshIn.set_halfedge_handle(faceHandler_new, halfEdge1);
}

/////////////////////////////////////////////////// SIMPLIFICATION ///////////////////////////////////////////////:


void MeshUtils::simplify(Mesh_OM &meshIn,unsigned int nbPointsTarget) {
    Decimater decimater(meshIn);
    decimater.setNbPointsTarget(nbPointsTarget);
    decimater.decimate_to();
    meshIn.garbage_collection();
    //1-Compute the quadric matrixq Q of each vertex
    //2-Compute the list of all valid pairs of vertices
    //3-Compute the optimal contraction target v for each valid pair, the cost of that pair is defined as deltaV
    //4-Sort the list of pairs with increasing cost (the lowest cost is first)
    //5-Iteratively removed the pair of lowest cost from the list, contract this pair,
    // update the costs of all valid pair involving the contracted vertices
}




