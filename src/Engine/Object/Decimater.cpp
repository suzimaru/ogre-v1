#include "Decimater.hpp"
#include <OpenMesh/Core/Mesh/PolyConnectivity.hh>
Decimater::Decimater(Mesh_OM& _mesh) : mesh_om(_mesh) {
    mesh_om.request_vertex_status();
    mesh_om.request_edge_status();
    mesh_om.request_face_status();
    mesh_om.add_property(collapse_target_);
    mesh_om.add_property(priority_);
    mesh_om.add_property(heap_position_);
    max_err = DBL_MAX;
    initialize();
}

Decimater::~Decimater() {
    mesh_om.remove_property(quadrics);
    mesh_om.remove_property(collapse_target_);
    mesh_om.remove_property(priority_);
    mesh_om.remove_property(heap_position_);
    mesh_om.release_vertex_status();
    mesh_om.release_edge_status();
    mesh_om.release_face_status();
}

float Decimater::collapse_priority(const CollapseInfo& ci) {
    typedef OpenMesh::Geometry::QuadricT<double> Q;
    Q q = mesh_om.property(quadrics, ci.v0);
    q += mesh_om.property(quadrics, ci.v1);

    double err = q(ci.p1);
    return float( (err < max_err) ? err : float( -1 ) );    }

void Decimater::initialize() {
    if (!quadrics.is_valid())
        mesh_om.add_property(quadrics);

    Mesh_OM::VertexIter v_it = mesh_om.vertices_begin(),
            v_end = mesh_om.vertices_end();
    //clear
    for (; v_it != v_end; ++v_it) {
        mesh_om.property(quadrics, *v_it).clear();
    }

    Mesh_OM::FaceIter f_it = mesh_om.faces_begin();
    Mesh_OM::FaceIter f_end = mesh_om.faces_end();

    Mesh_OM::FaceVertexIter fv_it;
    Mesh_OM::VertexHandle vh0, vh1, vh2;

    for (; f_it != f_end; ++f_it) {
        fv_it = mesh_om.fv_iter(*f_it);
        vh0 = *fv_it; ++fv_it;
        vh1 = *fv_it; ++fv_it;
        vh2 = *fv_it;

        Point3 v0, v1, v2;
        v0 = {mesh_om.point(vh0)[0],mesh_om.point(vh0)[1],mesh_om.point(vh0)[2]};
        v1 = {mesh_om.point(vh1)[0],mesh_om.point(vh1)[1],mesh_om.point(vh1)[2]};
        v2 = {mesh_om.point(vh2)[0],mesh_om.point(vh2)[1],mesh_om.point(vh2)[2]};

        Vector3 n = glm::cross((v1-v0),(v2-v0));

        float normN = glm::length(n);
        if (normN > FLT_MIN)
        {
            n /= normN;
            normN *= 0.5;
        }

        const double a = n[0];
        const double b = n[1];
        const double c = n[2];
        const double d = -(glm::dot(v0,n));

        OpenMesh::Geometry::Quadricd q(a,b,c,d);
//        q *= normN;

        mesh_om.property(quadrics,vh0) += q;
        mesh_om.property(quadrics,vh1) += q;
        mesh_om.property(quadrics,vh2) += q;
    }
}

size_t Decimater::decimate_to() {
    return (nbPointsTarget < mesh_om.n_vertices()) ? decimate(mesh_om.n_vertices() - nbPointsTarget) : 0;
}

void Decimater::postprocess_collapse(const CollapseInfo& ci) {
    mesh_om.property(quadrics, ci.v1) += mesh_om.property(quadrics, ci.v0);
}

void Decimater::heap_vertex(OpenMesh::VertexHandle vh) {
    float prio, best_prio(FLT_MAX);
    OpenMesh::HalfedgeHandle heh, collapse_target;

    //find the best target ine one ring
    Mesh_OM::VertexOHalfedgeIter voh_it(mesh_om,vh);
    for (; voh_it.is_valid(); ++voh_it) {
        heh =*voh_it;
        CollapseInfo ci(mesh_om,heh);

        if (is_collapse_legal(ci)) {
            prio = collapse_priority(ci);
            if (prio >= 0.0 && prio < best_prio) {
                best_prio = prio;
                collapse_target = heh;
            }
        }
    }

    if (collapse_target.is_valid()) {
        mesh_om.property(collapse_target_,vh) = collapse_target;
        mesh_om.property(priority_,vh) = best_prio;

        if(heap->is_stored(vh)) {
            heap->update(vh);
        }
        else {
            heap->insert(vh);
        }
    }
    // not valid -> remove from heap
    else {
        if (heap->is_stored(vh)) {
            heap->remove(vh);
        }

        mesh_om.property(collapse_target_,vh) = collapse_target;
        mesh_om.property(priority_,vh) = -1;
    }
}


bool Decimater::is_collapse_legal(const CollapseInfo& _ci) {
    //   std::clog << "McDecimaterT<>::is_collapse_legal()\n";

    // locked ?
    if (mesh_om.status(_ci.v0).locked())
        return false;

    // this test checks:
    // is v0v1 deleted?
    // is v0 deleted?
    // is v1 deleted?
    // are both vlv0 and v1vl boundary edges?
    // are both v0vr and vrv1 boundary edges?
    // are vl and vr equal or both invalid?
    // one ring intersection test
    // edge between two boundary vertices should be a boundary edge
    if (!mesh_om.is_collapse_ok(_ci.v0v1))
        return false;

    if (_ci.vl.is_valid() && _ci.vr.is_valid()
        && mesh_om.find_halfedge(_ci.vl, _ci.vr).is_valid()
        && mesh_om.valence(_ci.vl) == 3 && mesh_om.valence(_ci.vr) == 3) {
        return false;
    }
    //--- feature test ---

    if (mesh_om.status(_ci.v0).feature()
        && !mesh_om.status(mesh_om.edge_handle(_ci.v0v1)).feature())
        return false;

    //--- test boundary cases ---
    if (mesh_om.is_boundary(_ci.v0)) {

        // don't collapse a boundary vertex to an inner one
        if (!mesh_om.is_boundary(_ci.v1))
            return false;

        // only one one ring intersection
        if (_ci.vl.is_valid() && _ci.vr.is_valid())
            return false;
    }

    // there have to be at least 2 incident faces at v0
    if (mesh_om.cw_rotated_halfedge_handle(
            mesh_om.cw_rotated_halfedge_handle(_ci.v0v1)) == _ci.v0v1)
        return false;

    // collapse passed all tests -> ok
    return true;
}


size_t Decimater::decimate(size_t nb_collapses) {
    Mesh_OM::VertexIter v_it, v_end(mesh_om.vertices_end());
    Mesh_OM::VertexHandle vp;
    Mesh_OM::HalfedgeHandle v0v1;
    Mesh_OM::VertexVertexIter vv_it;
    Mesh_OM::VertexFaceIter vf_it;
    unsigned int n_collapses(0);

    typedef std::vector<Mesh_OM::VertexHandle> Support;
    typedef typename Support::iterator SupportIterator;

    Support support(15);
    SupportIterator s_it, s_end;

    // check _n_collapses
    if (!nb_collapses)
        nb_collapses = mesh_om.n_vertices();

    // initialize heap
    HeapInterface HI(mesh_om, priority_, heap_position_);

    heap = std::unique_ptr<DeciHeap>(new DeciHeap(HI));

    heap->reserve(mesh_om.n_vertices());

    for (v_it = mesh_om.vertices_begin(); v_it != v_end; ++v_it) {
        heap->reset_heap_position(*v_it);
        if (!mesh_om.status(*v_it).deleted())
            heap_vertex(*v_it);
    }

    const bool update_normals = mesh_om.has_face_normals();

    // process heap
    while ((!heap->empty()) && (n_collapses < nb_collapses)) {
        // get 1st heap entry
        vp = heap->front();
        v0v1 = mesh_om.property(collapse_target_, vp);
        heap->pop_front();

        // setup collapse info
        CollapseInfo ci(mesh_om, v0v1);

        // check topological correctness AGAIN !
        if (!this->is_collapse_legal(ci))
            continue;

        // store support (= one ring of *vp)
        vv_it = mesh_om.vv_iter(ci.v0);
        support.clear();
        for (; vv_it.is_valid(); ++vv_it)
            support.push_back(*vv_it);

        // perform collapse
        mesh_om.collapse(v0v1);
        ++n_collapses;

        if (update_normals)
        {
            // update triangle normals
            vf_it = mesh_om.vf_iter(ci.v1);
            for (; vf_it.is_valid(); ++vf_it)
                if (!mesh_om.status(*vf_it).deleted())
                    mesh_om.set_normal(*vf_it, mesh_om.calc_face_normal(*vf_it));
        }
        // post-process collapse : update Q matrices
        this->postprocess_collapse(ci);
        // update heap (former one ring of decimated vertex)
        for (s_it = support.begin(), s_end = support.end(); s_it != s_end; ++s_it) {
            assert(!mesh_om.status(*s_it).deleted());
            heap_vertex(*s_it);
        }
    }

    // delete heap
    heap.reset();


    std::cout << "n collapse = " << n_collapses << std::endl;

    // DON'T do garbage collection here! It's up to the application.
    return n_collapses;
}
