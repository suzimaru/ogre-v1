#include "ObjectManager.hpp"
#include <Core/Log/Log.hpp>
#include <Core/Math/GlmToEigen.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>

ObjectManager::ObjectManager(): ManagerList<Object*>("ObjectManager")
{
}

ObjectManager::~ObjectManager()
{
/*    for(auto i:listObject){
        delete i;
    }*/
}
/*
void ObjectManager::addObject(Object *object){
    listObject.push_back(object);
}

std::vector<Object*> ObjectManager::getListObject(){
    return listObject;
}*/

void ObjectManager::draw(DataForDrawing data){
    for(auto i:getValues()){
        i->draw(data);
    }
}

AABB ObjectManager::getAABB() {
    AABB aabb;
    //TODO improve ...
    Log(logWarning) << "Improve compute aabb";
    for(auto i: getValues()){
        Matrix4 model = i->getModel();
        for(auto j: i->getElements()){
            for(auto l: j->m_mesh.m_vertices){
                Vector4 vec = model *  Vector4(l.x,l.y,l.z,1);
                aabb.extend(GTE_VECTOR3(Vector3(vec.x, vec.y, vec.z)));
            }
        }
        aabb.extend(GTE_VECTOR3(getPositionMat4(i->getModel())));
    }
    return aabb;
}

void ObjectManager::setMeshTypes(GLenum t){
    for(auto i:getValues()){
        i->setMeshTypes(t);
    }
}

int ObjectManager::addValue(Object *o, std::string name) {
    o->setName(name + std::to_string(getValues().size()+1));
    return ManagerList::addValue(o);
}

void ObjectManager::setNameLast(std::string name) {
    if(getValues().size()>0){
        getLastValue()->setName(name + std::to_string(getValues().size()));
    }
}

Object* ObjectManager::rayIntersection(Ray &ray){
    float t = -1;
    Object *o = nullptr;
    for(auto i:getValues()){
        if(ray.intersectionObject(i,t)){
            o=i;
        }
    }
    return o;

}

void ObjectManager::displayInfo() {
    Log(logInfo) << "| - Object number: " << getValues().size();
}

void ObjectManager::displayInfo2() {
    if(getValues().size()==0){
        Log(logInfo) << "|";
        Log(logInfo) << "| There are no objects ...";
        Log(logInfo) << "|";
    }
    for(auto i:getValues()){
        i->displayInfo();
    }
}