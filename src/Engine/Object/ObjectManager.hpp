#ifndef OBJECTMANAGER_HPP
#define OBJECTMANAGER_HPP


#include <Engine/Object/Object.hpp>
#include <vector>
#include <src/Core/Managers/ManagerList.hpp>
#include <Core/Macros.hpp>

class ObjectManager : public ManagerList<Object*>
{
public:
    ObjectManager();
    ~ObjectManager();

    void draw(DataForDrawing data);

    AABB getAABB();
    void setMeshTypes(GLenum t);
    int addValue(Object *o, std::string name = "DEFAULT_NAME");
    void setNameLast(std::string name);

    Object* rayIntersection(Ray &r);

    void displayInfo();
    void displayInfo2();
};

#endif // OBJECTMANAGER_HPP
