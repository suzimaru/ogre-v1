#include "Element.hpp"

Element::Element(Mesh mesh ) : m_mesh{mesh}
{

}

void Element::updateData(){
    m_mesh.updateData();
}

void Element::draw(DataForDrawing data, Matrix4 model) {

    Material *material = data.materialManager->getValue(m_materialName);

    Shader *shader = data.shaderManager->getValue(material->getNameShader());

    shader->use();
    shader->setBool("ssaoActivated",data.ssaoActivated);
    shader->setInt("textureSSAO",3);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, data.textureSSAO);

    shader->setMat4("model",model);
    shader->setMat4("proj",data.camera->GetProjMatrix());
    shader->setMat4("view",data.camera->GetViewMatrix());
    shader->setVec3("viewPos",data.camera->m_position);


    data.lightManager->bind(shader);
    material->bind(shader, data);

    m_mesh.draw();
}

void Element::setMeshTypes(GLenum t) {
    m_mesh.setMeshTypes(t);
}

void Element::setMaterial(std::string nameMaterial){
    m_materialName=nameMaterial;
}


void Element::displayInfo(){
    Log(logInfo) << "| - Material :" << m_materialName;
}