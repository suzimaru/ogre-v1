#ifndef MESHUTILS_HPP
#define MESHUTILS_HPP

#include "src/Core/Macros.hpp"
#include <Engine/Object/Mesh.hpp>
#include "Decimater.hpp"
//#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>

class MeshUtils
{
public:

    typedef OpenMesh::TriMesh_ArrayKernelT<>  Mesh_OM;


    static Mesh_OM basicMesh2MeshOM(Mesh meshIn);
    static Mesh meshOM2BasicMesh(Mesh_OM meshIn);

    static void loopSubdivision(Mesh_OM &meshIn);

    static void simplify(Mesh_OM &meshIn,unsigned int nbPointsTarget);

private:
    static void corner_cutting(Mesh_OM& meshIn, const Mesh_OM::HalfedgeHandle& halfEdge);

};

#endif // MESHUTILS_HPP
