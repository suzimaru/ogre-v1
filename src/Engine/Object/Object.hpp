#ifndef OBJECT_HPP
#define OBJECT_HPP

/*
#define GL_GLEXT_PROTOTYPES 1
#include <GL/gl.h>
#include <GL/glext.h>*/

#include <Core/Macros.hpp>
#include "Mesh.hpp"
#include "Element.hpp"
#include <Engine/Material/Material.hpp>



class Object
{
public:
    Object(Mesh mesh = Mesh());
    Object(std::vector<Element*> elements);

    void updateData();
    virtual void draw(DataForDrawing data);
    //Mesh m_mesh;

    void translate(Vector3 vec);
    void translate(float x, float y, float z);
    void rotate(float angle, Vector3 vec);
    void rotate(float angle, float x, float y, float z);
    void scale(float s);
    void scale(Vector3 vec);
    void scale(float x, float y, float z);

    void setMaterial(std::string nameMaterial);
    Matrix4 getModel();
    void setIdentity();
    void setPositionModel(Vector3 position);
    Point3 getPositionModel();
    std::vector<Element*> getElements();
    void setMeshTypes(GLenum t);
    Mesh getMesh();
    void setMesh(Mesh mesh);

    void setName(std::string name);
    std::string getName();

    void displayInfo();
private:

    std::string m_name = "NO NAME GIVEN";
    Matrix4 m_model;
    std::vector<Element*> m_elements;

};

#endif // OBJECT_HPP
