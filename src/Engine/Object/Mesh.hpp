#ifndef MESH_HPP
#define MESH_HPP

#include "src/Core/Macros.hpp"

class Mesh
{
public:
    Mesh(VectorPoint3 vertices = VectorPoint3(),
         VectorNormal normals = VectorNormal(),
                 VectorPoint3 texCoord = VectorPoint3());

    void draw();
    void updateData();
    void sendData(GLuint location, GLint n, GLuint *buffer, VectorPoint3* data);

    VectorPoint3 m_vertices;
    VectorNormal m_normals;

    void setTriangle(Triangle t);
    void setLine(Line l);
    void addLine(Point3 p1, Point3 p2);
    void autoNormals();
    VectorUInt m_indices;

    VectorPoint3 m_texCoord;

    GLuint m_vao;
    GLuint m_vbo; // Vertices
    GLuint m_nbo; // Normals
    GLuint m_ebo; // Edges
    GLuint m_tbo; // Texture coords

    void setMeshTypes(GLenum t);
    GLenum mesh_types = GL_TRIANGLES;




private:

    bool toUpdate;
};

#endif // MESH_HPP
