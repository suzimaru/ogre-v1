#include <src/Core/Log/Log.hpp>
#include "Mesh.hpp"

Mesh::Mesh(VectorPoint3 vertices, VectorNormal normals, VectorPoint3 texCoord):
    m_vertices{vertices}, m_normals{normals}, m_texCoord{texCoord}
{
    toUpdate=true;
    m_vao=0;
    m_vbo = 0;
    m_nbo = 0;
    m_ebo = 0;
    m_tbo = 0;
}

void Mesh::setTriangle(Triangle t) {
    m_indices.push_back(t.x);
    m_indices.push_back(t.y);
    m_indices.push_back(t.z);
}

void Mesh::setLine(Line l) {
    m_indices.push_back(l.x);
    m_indices.push_back(l.y);
}


void Mesh::draw(){
    updateData();
    glBindVertexArray(m_vao);
    //Log(logInfo) << "Draw:" << m_vao;
    glDrawElements(mesh_types, m_indices.size()*sizeof(uint), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void Mesh::sendData(GLuint location, GLint n, GLuint *buffer, VectorPoint3 *data) {
    if(*buffer == 0){
        glGenBuffers(1,buffer);
        glBindBuffer(GL_ARRAY_BUFFER, *buffer);
        glVertexAttribPointer(location, n, GL_FLOAT, GL_FALSE, sizeof(Point3), (GLvoid*)0);
        glEnableVertexAttribArray(location);

    }

    glBindBuffer(GL_ARRAY_BUFFER, *buffer);
    glBufferData(GL_ARRAY_BUFFER, data->size()*sizeof(Point3), data->data(), GL_DYNAMIC_DRAW);


}

/*void Mesh::sendData(GLuint location, GLint n, GLuint *buffer, VectorPoint3 *data) {
    if(*buffer == 0){
        glGenBuffers(1,buffer);
        glBindBuffer(GL_ARRAY_BUFFER, *buffer);
        glVertexAttribPointer(location, n, GL_FLOAT, GL_FALSE, sizeof(Point3), (GLvoid*)0);
        glEnableVertexAttribArray(location);

    }

    glBindBuffer(GL_ARRAY_BUFFER, *buffer);
    glBufferData(GL_ARRAY_BUFFER, data->size()*sizeof(Point3), data->data(), GL_DYNAMIC_DRAW);


}*/

void Mesh::updateData(){
    if(toUpdate){

        // Initialize the geometry
        // 1. Generate geometry buffers
        if(m_vao == 0) glGenVertexArrays(1, &m_vao);

        glBindVertexArray(m_vao);
        sendData(0,3,&m_vbo,&m_vertices);
        sendData(1,3,&m_nbo,&m_normals);
        sendData(2,2,&m_tbo,&m_texCoord);



        if(m_ebo == 0){
            glGenBuffers(1, &m_ebo) ;
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
        }

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size()*sizeof(uint), m_indices.data(), GL_DYNAMIC_DRAW);


        glBindVertexArray(0);

        toUpdate=false;

    }
}

void Mesh::setMeshTypes(GLenum t) {
    mesh_types=t;
}

void Mesh::addLine(Point3 p1, Point3 p2){
    if(mesh_types != GL_LINES){
        Log(logWarning) << "We won't add a line to a mesh from other type than GL_POINT ! ";
        return;
    }
    m_vertices.push_back(p1);
    m_vertices.push_back(p2);
    m_normals.push_back(Normal(0,1,0));
    m_normals.push_back(Normal(0,1,0));
    m_texCoord.push_back(Point3(0,0,0));
    m_texCoord.push_back(Point3(0,0,0));
    m_indices.push_back(m_vertices.size()-2);
    m_indices.push_back(m_vertices.size()-1);

    toUpdate=true;
}

void Mesh::autoNormals() {
    m_normals.resize(m_vertices.size(), Point3());


    for(auto i=0;i<m_indices.size();i+=3){
        int s1 = m_indices[i];
        int s2 = m_indices[i+1];
        int s3 = m_indices[i+2];
        Vector3 v1 = m_vertices[s1]-m_vertices[s2];
        Vector3 v2 = m_vertices[s1]-m_vertices[s3];
        Vector3 n = glm::cross(v1,v2); // pour le model lapin
//        Vector3 n = glm::cross(v2,v1);

        m_normals[s1] += n;
        m_normals[s2] += n;
        m_normals[s3] += n;

    }
    for(auto i=0;i<m_normals.size(); i++){
        m_normals[i] = glm::normalize(m_normals[i]);
    }
}

