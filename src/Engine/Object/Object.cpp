#include "Object.hpp"

#include <Engine/Shader/Shader.hpp>

Object::Object(Mesh mesh)
{
    m_elements.push_back(new Element(mesh));
}

Object::Object(std::vector<Element *> elements) : m_elements{elements} {
    //bind();
}

void Object::updateData(){
    for(auto i : m_elements) i->updateData();
}


void Object::draw(DataForDrawing data){
    for(auto i : m_elements) i->draw(data,m_model);
}

void Object::translate(Vector3 vec){
    m_model = glm::translate(m_model,vec);
}

void Object::translate(float x, float y, float z){
    m_model = glm::translate(m_model,Vector3(x,y,z));
}

void Object::rotate(float angle, Vector3 vec){
    m_model = glm::rotate(m_model, glm::radians(angle), vec);
}

void Object::rotate(float angle, float x, float y, float z){
    m_model = glm::rotate(m_model, glm::radians(angle), Vector3(x,y,z));
}

void Object::scale(float s){
    m_model = glm::scale(m_model, Vector3(s));
}

void Object::scale(Vector3 vec){
    m_model = glm::scale(m_model, vec);
}

void Object::scale(float x, float y, float z){
    m_model = glm::scale(m_model, Vector3(x,y,z));
}

void Object::setMaterial(std::string m) {
    for(auto i : m_elements) i->setMaterial(m);
}

Matrix4 Object::getModel() {
    return m_model;
}

std::vector<Element*> Object::getElements(){
    return m_elements;
}

void Object::setPositionModel(Vector3 position){
    setPositionMat4(m_model,position);
}

Point3 Object::getPositionModel() {
    return getPositionMat4(m_model);
}

void Object::setIdentity() {
    m_model=Matrix4();
}

void Object::setMeshTypes(GLenum t) {
    for(auto i:getElements()){
        i->setMeshTypes(t);
    }
}

void Object::setName(std::string name) {
    m_name = name;
}

std::string Object::getName(){
    return m_name;
}

Mesh Object::getMesh() {
    return m_elements[0]->m_mesh;
}

void Object::setMesh(Mesh mesh){
    m_elements[0]->m_mesh=mesh;
}

void Object::displayInfo(){
    Log(logInfo) << "|---------------- Object ----------------";
    auto pos=getPositionMat4(m_model);
    Log(logInfo) << "| - Name : " << m_name;
    Log(logInfo) << "| - Position : " << pos.x << " " << pos.y << " " << pos.z;
    for(auto i:getElements()){
        i->displayInfo();
    }
}