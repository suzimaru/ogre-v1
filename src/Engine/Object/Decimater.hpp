#ifndef DECIMATER_HPP
#define DECIMATER_HPP

#include "src/Core/Macros.hpp"
#include <Engine/Object/Mesh.hpp>
//#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Geometry/QuadricT.hh>
#include <OpenMesh/Tools/Decimater/ModBaseT.hh>
#include <OpenMesh/Tools/Utils/HeapT.hh>
#include <memory>


class Decimater {

    typedef OpenMesh::TriMesh_ArrayKernelT<>  Mesh_OM;

public:
    Decimater(Mesh_OM& _mesh);

    void setNbPointsTarget(unsigned int nbpoints) {
        nbPointsTarget = nbpoints;
    }

    ~Decimater();

    size_t decimate_to();

    size_t decimate (size_t nb_collapses = 0);

private:
    Mesh_OM& mesh_om;
    unsigned int nbPointsTarget;
    double max_err;
    OpenMesh::VPropHandleT<OpenMesh::Geometry::QuadricT<double>>  quadrics;

    void initialize();

    typedef OpenMesh::Decimater::CollapseInfoT<Mesh_OM> CollapseInfo;

    float collapse_priority(const CollapseInfo& ci);

    void postprocess_collapse(const CollapseInfo& ci);


    class HeapInterface {

    private:
        Mesh_OM& mesh_heap;
        OpenMesh::VPropHandleT<float> prio;
        OpenMesh::VPropHandleT<int> pos;


    public:
        HeapInterface(Mesh_OM& _mesh, OpenMesh::VPropHandleT<float> _prio, OpenMesh::VPropHandleT<int> _pos) :
        mesh_heap(_mesh), prio(_prio), pos(_pos)
        { }

        inline bool less (OpenMesh::VertexHandle vh0, OpenMesh::VertexHandle vh1) {
            return mesh_heap.property(prio,vh0) < mesh_heap.property(prio,vh1);
        }

        inline bool greater (OpenMesh::VertexHandle vh0, OpenMesh::VertexHandle vh1) {
            return mesh_heap.property(prio,vh0) > mesh_heap.property(prio,vh1);
        }

        inline int get_heap_position(OpenMesh::VertexHandle vh) {
            return mesh_heap.property(pos,vh);
        }

        inline void set_heap_position(OpenMesh::VertexHandle vh, int _pos) {
            mesh_heap.property(pos,vh) = _pos;
        }
    };

    void heap_vertex(OpenMesh::VertexHandle vh);
    bool is_collapse_legal(const CollapseInfo& _ci);

        typedef OpenMesh::Utils::HeapT<OpenMesh::VertexHandle, HeapInterface> DeciHeap;
    std::unique_ptr<DeciHeap> heap;

    OpenMesh::VPropHandleT<OpenMesh::HalfedgeHandle>  collapse_target_;
    OpenMesh::VPropHandleT<float>           priority_;
    OpenMesh::VPropHandleT<int>             heap_position_;
};


#endif // DECIMATER_HPP
