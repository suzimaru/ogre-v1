#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <Engine/Object/Object.hpp>
#include <Engine/Renderer/DrawData.hpp>
#include <src/Engine/Loader/FileLoaderManager.hpp>
#include <src/Engine/Texture/FrameBuffer.hpp>
#include <Engine/Texture/GBuffer.hpp>
#include <Engine/Texture/SSAOBuffer.hpp>
#include <Engine/Texture/PingPongFrame.hpp>
#include <Engine/Texture/HdrBloomFrame.hpp>
#include <random>

class Renderer
{
public:
    Renderer();

    virtual void draw();

    int addObject(Object *object);

    DrawData drawData;

    void displayInfo();

    void toggleDrawMode();

    void resize(int w, int h);

    std::string changeShaderPost();
    std::string changeSkybox();

    void changeModeRenderer(int mode);

    void changeRadius(float f);
    void changeBiais(float f);

    void useHdr(bool b);
    void changeGamma(float f);
    void changeExposure(float f);

    void useFxaa(bool b);
    void changeThresholdMax(float f);
    void changeThresholdMin(float f);

private:
    bool m_drawfill = true;
    Shader *screenShader;
    Object *screenObj;
    Object *skyboxObj;
    Texture *cubemapTexture;
    Shader *skyboxShader;
    FrameBuffer frameBuffer;
    FrameBuffer frameBright;
    FrameBuffer frameBlur;
    FrameBuffer frameFxaa;

    int rendererMode;
    float radius;
    float biais;

    bool hdrActivated;
    float exposure;
    float gamma;

    bool fxaaActivated;
    float thresholdMin;
    float thresholdMax;
    /*0: default, 1: SSAO ...*/

    void init();
    void initSSAO();
    void initHdr();
    void initFxaa();

    unsigned int textureFinal;

    Shader* shaderSSAOGeometry;
    Shader* shaderSSAO;
    Shader* shaderSSAOBlur;

    Shader* shaderBright;
    Shader* shaderBlur;
    Shader* shaderFinalBloom;

    Shader* shaderFxaa;

    unsigned int noiseTexture;
    std::uniform_real_distribution<GLfloat> randomFloats;
    std::default_random_engine generator;
    std::vector<glm::vec3> ssaoKernel;

    GBuffer gBuffer;
    SSAOBuffer ssaoFBO;
    SSAOBuffer ssaoBlurFBO;

    PingPongFrame pingpongFrameBuffer[2];

    std::vector<std::pair<std::string,Shader *>> shadersPostProcess;
    int numShaderPP = 0;

    std::vector<std::pair<std::string,Texture *>> skyboxTextures;
    int numSkyBoxPP = 0;

};

#endif // RENDERER_HPP
