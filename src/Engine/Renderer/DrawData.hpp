#ifndef DRAWDATA_HPP
#define DRAWDATA_HPP

#include <Engine/Camera/Camera.hpp>
#include <Engine/Shader/ShaderManager.hpp>
#include <Engine/Camera/CameraManager.hpp>
#include <src/Engine/Light/LightManager.hpp>
#include <src/Engine/Object/ObjectManager.hpp>
#include <Engine/Material/MaterialManager.hpp>
class FileLoaderManager;

class DrawData
{
public:
    DrawData();

    CameraManager cameraManager;
    Camera *camera;

    ShaderManager shaderManager;
    LightManager lightManager;
    ObjectManager objectManager;
    MaterialManager materialManager;

    FileLoaderManager *fileLoaderManager;

    DataForDrawing getDataForDrawing();

    void cleanScene();

    void changeCamera();
    Object *rayCast(GLfloat x, GLfloat y);

    void displayInfo();
private:

    void initShader();
    void initMaterial();
    void initData();
    
};

#endif // DRAWDATA_HPP
