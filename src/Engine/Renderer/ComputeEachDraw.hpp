//
// Created by pierre on 28/08/18.
//

#ifndef OGRE_COMPUTEEACHDRAW_HPP
#define OGRE_COMPUTEEACHDRAW_HPP


class ComputeEachDraw {
public:
    std::string id_computeEachDraw = "NO ID GIVER COMPUTEEACHDRAW";

    virtual void computeEachDraw() = 0;
    virtual void cleanUp() = 0;

};


#endif //OGRE_COMPUTEEACHDRAW_HPP
