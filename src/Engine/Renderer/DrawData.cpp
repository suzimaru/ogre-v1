#include "DrawData.hpp"

#include <Engine/Camera/EulerCamera.hpp>
#include <Engine/Camera/Camera2D.hpp>
#include <Engine/Material/Material.hpp>
#include <src/Engine/Material/SpecialMaterial/MirrorMaterial.hpp>
#include <src/Engine/Material/SpecialMaterial/GlassMaterial.hpp>

DrawData::DrawData()
{
    camera = cameraManager.getCamera();

    initData();
}

void DrawData::changeCamera(){
    camera= cameraManager.nextCamera();
}

DataForDrawing DrawData::getDataForDrawing(){
    DataForDrawing data;
    data.shaderManager=&shaderManager;
    data.camera=camera;
    data.lightManager=&lightManager;
    data.materialManager=&materialManager;
    return data;
}

Object * DrawData::rayCast(GLfloat x, GLfloat y) {
    Ray r=camera->getRayFromScreen(x,y);
    return objectManager.rayIntersection(r);
}



void DrawData::initMaterial(){
    Material *m = new Material(Vector3(0.5));
    materialManager.addValue("default",m);
    m = new Material("../Assets/Textures/container.png");
    m->setKsTexture("../Assets/Textures/container_specular.png");
    materialManager.addValue("container",m);

    materialManager.addValue("emerald",new Material(Vector3(0.07568,0.61424,0.07568),Vector3(0.633,0.727811,0.633),0.6));
    materialManager.addValue("gold",new Material(Vector3(0.75164,0.60648,0.22648),Vector3(0.628281,0.555802,0.3666065),0.4));
    materialManager.addValue("greenPlastic",new Material(Vector3(0.1,0.35,0.1),Vector3(0.45,0.55,0.45),0.25));
    materialManager.addValue("chrome",new Material(Vector3(0.4,0.4,0.4),Vector3(0.774597,0.774597,0.774597),0.6));


    m = new Material("../Assets/Textures/earth.jpg","../Assets/Textures/earth_spec.jpg");
    m->setNs(32);
    materialManager.addValue("earth",m);

    m = new Material("../Assets/Textures/rock/Granite_kd.jpg");
    m->setKsTexture("../Assets/Textures/rock/Granite_ks.jpg");
    m->setNs(5);
    materialManager.addValue("granite",m);

    m = new Material("../Assets/Textures/rock/Blue_Marble_kd.jpg");
    m->setNsTexture("../Assets/Textures/rock/Blue_Marble_ks.jpg");
    materialManager.addValue("blue_marble",m);

    m = new Material(Vector3(1));
    m->setShader("normal");
    materialManager.addValue("normal",m);

    m = new Material(Vector3(0.5));
    m->setShader("onlyKd");
    materialManager.addValue("blank",m);

    m = new Material(Vector3(0.5,0,0));
    m->setShader("onlyKd");
    materialManager.addValue("red",m);

    m = new Material(Vector3(0.1,0.1,0.5));
    m->setShader("onlyKd");
    materialManager.addValue("blue",m);

    materialManager.addValue("mirror", new MirrorMaterial());
    materialManager.addValue("glass", new GlassMaterial());

}

void DrawData::initShader() {
    shaderManager.addValue("default",new Shader("../Shaders/vertexShader.vs.glsl","../Shaders/BasicLight.fs.glsl"));
    shaderManager.addValue("normal",new Shader("../Shaders/default.vs.glsl","../Shaders/fragmentShader.fs.glsl"));
    shaderManager.addValue("onlyKd",new Shader("../Shaders/default.vs.glsl","../Shaders/onlyKd.fs.glsl"));
    shaderManager.addValue("mirror",new Shader("../Shaders/mirror.vs.glsl","../Shaders/mirror.fs.glsl"));
    shaderManager.addValue("glass", new Shader("../Shaders/glass.vs.glsl","../Shaders/glass.fs.glsl"));/*
    shaderManager.addValue("ssao_geometry", new Shader("../Shaders/SSAO/ssao_geometry.vs.glsl","../Shaders/SSAO/ssao_geometry.fs.glsl"));
    shaderManager.addValue("ssao_lighting", new Shader("../Shaders/SSAO/ssao.vs.glsl","../Shaders/SSAO/ssao_lighting.fs.glsl"));
    shaderManager.addValue("ssao", new Shader("../Shaders/SSAO/ssao.vs.glsl","../Shaders/SSAO/ssao.fs.glsl"));
    shaderManager.addValue("ssao_blur", new Shader("../Shaders/SSAO/ssao.vs.glsl","../Shaders/SSAO/ssao_blur.fs.glsl"));*/
    //shaderManager.addValue("defaultTexture",new Shader("../Shaders/vertexShader.vs.glsl","../Shaders/BasicLight.fs.glsl"));

}

void DrawData::initData(){
    initMaterial();
    initShader();
}

void DrawData::cleanScene() {
    objectManager.remAll();
    lightManager.remAll();
}

void DrawData::displayInfo(){
    Log(logInfo) << "";
    Log(logInfo) << "|------------ DrawData info -------------";
    Log(logInfo) << "| - Actual camera: " << camera->name_camera;
    cameraManager.displayInfo();
    shaderManager.displayInfo();
    lightManager.displayInfo();
    objectManager.displayInfo();
    materialManager.displayInfo();
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";
}