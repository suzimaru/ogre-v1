#include "Renderer.hpp"
#include <Engine/Camera/EulerCamera.hpp>
#include <Core/Macros.hpp>
#include <src/Core/Shapes/TriangleMesh.hpp>
#include <Engine/Texture/GBuffer.hpp>

#include <stdio.h>
#include <random>

#define IDR "[Renderer] "

Renderer::Renderer()
{
    init();
    initSSAO();
    initHdr();
    initFxaa();
    hdrActivated = false;
    fxaaActivated = false;
}

inline float lerp(float a, float b, float f)
{
    return a + f * (b - a);
}

void Renderer::draw(){
    // QT use his own default frame buffer ...

    GLint qt_buffer;
    //pour trouver le numéro du buffer de qt
    glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);

    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    DataForDrawing dfd = drawData.getDataForDrawing();
    dfd.ssaoActivated = false;
    if (rendererMode == 1) {
        /* *********************** PrePass   ********************** */
//        geometry
        gBuffer.bind();
        glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        shaderSSAOGeometry->use();

        shaderSSAOGeometry->setMat4("proj",drawData.camera->GetProjMatrix());
        shaderSSAOGeometry->setMat4("view",drawData.camera->GetViewMatrix());

        for (auto obj : drawData.objectManager.getValues()) {
            shaderSSAOGeometry->setMat4("model", obj->getModel());
            for (auto elem : obj->getElements()) {
                elem->m_mesh.draw();
            }
        }
        gBuffer.unbind();

        //generate SSAO texture
        ssaoFBO.bind();
        glClear(GL_COLOR_BUFFER_BIT);
        shaderSSAO->use();
        for (unsigned int i = 0; i < 64; ++i)
            shaderSSAO->setVec3("samples[" + std::to_string(i) + "]", ssaoKernel[i]);
        shaderSSAO->setMat4("proj", drawData.camera->GetProjMatrix());
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gBuffer.gPosition);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, gBuffer.gNormal);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, noiseTexture);

        shaderSSAO->setFloat("radius",radius);
        shaderSSAO->setFloat("biais",biais);
        Vector2 noiseScale(drawData.camera->m_width/4,drawData.camera->m_height/4);
        shaderSSAO->setVec2("noiseScale", noiseScale);
        screenObj->getMesh().draw();
        ssaoFBO.unbind();


        //blur SSAO texture to remove noise
        ssaoBlurFBO.bind();
        glClear(GL_COLOR_BUFFER_BIT);
        shaderSSAOBlur->use();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, ssaoFBO.Color);
        screenObj->getMesh().draw();
        ssaoBlurFBO.unbind();
        dfd.ssaoActivated = true;
        dfd.textureSSAO = ssaoFBO.Color;
    }
    /* *********************** Rendering ********************** */

    frameBuffer.bind();

    glClearColor(0.05f, 0.1f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    if (m_drawfill)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    if (rendererMode==1)
    {
        dfd.textureSSAO = ssaoBlurFBO.Color;
    }
    dfd.skybox = cubemapTexture;

    drawData.objectManager.draw(dfd);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    /* ************************ Skybox ************************ */

    if(cubemapTexture != nullptr){
        glDepthFunc(GL_LEQUAL);
        skyboxShader->use();
        cubemapTexture->bindCubemap(GL_TEXTURE0);
        skyboxShader->setInt("skybox",0);
        skyboxShader->setMat4("proj",drawData.camera->GetProjMatrix());
        skyboxShader->setMat4("view",Matrix4(Matrix3(drawData.camera->GetViewMatrix())));
        skyboxObj->getMesh().draw();
        glDepthFunc(GL_LESS);

    }
    /* ********************** PostProcess ********************* */

    /* ************************* FXAA ****************************/
    if (fxaaActivated) {
        frameFxaa.bind();
        glClearColor(1, 1, 1, 1.0f);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT);

        shaderFxaa->use();
        shaderFxaa->setInt("screenTexture", 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, frameBuffer.textureColor);
        shaderFxaa->setFloat("thresholdMin", thresholdMin);
        shaderFxaa->setFloat("thresholdMax", thresholdMax);
        Vector2 step(1. / drawData.camera->m_width, 1. / drawData.camera->m_height);
        shaderFxaa->setVec2("texelStep", step);

        screenObj->getMesh().draw();
        frameFxaa.unbind();
    }

    /* ********************** HDR + Bloom ***************************/

    // Blur bright fragments with two-pass Gaussian blur

    if (hdrActivated) {
        frameBright.bind();
        glClearColor(1, 1, 1, 1.0f);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT);

        shaderBright->use();
        shaderBright->setInt("screenTexture", 0);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, frameBuffer.textureColor);
        screenObj->getMesh().draw();
        frameBright.unbind();

        bool horizontal = true, first_iteration = true;
        unsigned int amount = 10;
        shaderBlur->use();
        shaderBlur->setInt("brightColor", 0);
        for (unsigned int i = 0; i < amount; i++) {
            pingpongFrameBuffer[horizontal].bind();
            shaderBlur->setBool("horizontal", horizontal);
            glActiveTexture(GL_TEXTURE0);
            if (first_iteration) {
                glBindTexture(GL_TEXTURE_2D, frameBright.textureColor);
            } else {
                glBindTexture(GL_TEXTURE_2D, pingpongFrameBuffer[!horizontal].pingPongColorBuffers);
            }
            // bind texture of other framebuffer (or scene if first iteration)
            screenObj->getMesh().draw();
            horizontal = !horizontal;
            if (first_iteration)
                first_iteration = false;
        }

        frameBlur.bind();
        glClearColor(1, 1, 1, 1.0f);
        glDisable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT);
        shaderFinalBloom->use();
        glActiveTexture(GL_TEXTURE0);
        if (fxaaActivated)
            glBindTexture(GL_TEXTURE_2D, frameFxaa.textureColor);
        else
            glBindTexture(GL_TEXTURE_2D, frameBuffer.textureColor);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, pingpongFrameBuffer[!horizontal].pingPongColorBuffers);
        shaderFinalBloom->setFloat("exposure", exposure);
        shaderFinalBloom->setFloat("gamma", gamma);
        screenObj->getMesh().draw();
        frameBlur.unbind();
    }

    glBindFramebuffer(GL_FRAMEBUFFER, qt_buffer);
    glClearColor(1,1,1, 1.0f);
    glDisable(GL_DEPTH_TEST);
    glClear(GL_COLOR_BUFFER_BIT );
    screenShader->use();
    screenShader->setInt("screenTexture",0);
    glActiveTexture(GL_TEXTURE0);
    if (hdrActivated) {
        glBindTexture(GL_TEXTURE_2D, frameBlur.textureColor);
    }
    else if (fxaaActivated) {
        glBindTexture(GL_TEXTURE_2D, frameFxaa.textureColor);
    }
    else {
        glBindTexture(GL_TEXTURE_2D, frameBuffer.textureColor/*ssaoBlurFBO.Color*/);
    }
    screenObj->getMesh().draw();

}

void Renderer::init() {
    Log(logInfo) << IDR << "Initialization shader postProcess";
    std::string dir = "../Shaders/PostProcess/";
    shadersPostProcess.push_back({"default", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"framebuffer_screen.fs.glsl")});
    shadersPostProcess.push_back({"blur", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_blur.fs.glsl")});
    shadersPostProcess.push_back({"inversion", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_inversion.fs.glsl")});
    shadersPostProcess.push_back({"grayscale", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_grayscale.fs.glsl")});
    shadersPostProcess.push_back({"edgeDetection", new Shader(dir+"framebuffer_screen.vs.glsl",dir+"fb_edgeDetection.fs.glsl")});
    screenShader = shadersPostProcess[0].second;
    screenObj = new Object(createSimpleRec());

    Log(logInfo) << IDR << "Initialization skybox";
    skyboxShader = new Shader("../Shaders/skybox.vs.glsl","../Shaders/skybox.fs.glsl");
    skyboxTextures.push_back({"no skybox", nullptr});
    skyboxTextures.push_back({"Ocean", new Texture("../Assets/Textures/Skybox/ocean", {"right.jpg","left.jpg","top.jpg","bottom.jpg","front.jpg","back.jpg"})});
    skyboxTextures.push_back({"Nebula", new Texture("../Assets/Textures/Skybox/ame_nebula", {"rt.tga","lf.tga","up.tga","dn.tga", "ft.tga", "bk.tga"})});
    skyboxTextures.push_back({"City", new Texture("../Assets/Textures/Skybox/hw_city", {"rt.jpg","lf.jpg","up.jpg","dn.jpg", "ft.jpg", "bk.jpg"})});

    cubemapTexture = skyboxTextures[0].second;
    skyboxObj = new Object(createSimpleCube());
}

void Renderer::initSSAO(){
    radius = 0.5;
    biais = 0.01;
    shaderSSAOGeometry = new Shader("../Shaders/SSAO/ssao_geometry.vs.glsl","../Shaders/SSAO/ssao_geometry.fs.glsl");
    shaderSSAO = new Shader("../Shaders/SSAO/ssao.vs.glsl","../Shaders/SSAO/ssao.fs.glsl");
    shaderSSAOBlur = new Shader("../Shaders/SSAO/ssao.vs.glsl","../Shaders/SSAO/ssao_blur.fs.glsl");

    randomFloats= std::uniform_real_distribution<GLfloat> (0.0, 1.0);
    for (unsigned int i=0;i<64;++i)
    {
        glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
        sample = glm::normalize(sample);
        sample *= randomFloats(generator);
        float scale = float(i)/64.0;

        // Scale samples s.t. they're more aligned to center of kernel
        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssaoKernel.push_back(sample);
    }
    std::vector<glm::vec3> ssaoNoise;
    for(unsigned int i = 0; i < 16; i++)
    {
        glm::vec3 noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f);
        ssaoNoise.push_back(noise);
    }
    glGenTextures(1, &noiseTexture);
    glBindTexture(GL_TEXTURE_2D, noiseTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, 4, 4, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    //lighting info
    glm::vec3 lightPos = glm::vec3(2.0, 4.0, -2.0);
    glm::vec3 lightColor = glm::vec3(0.2,0.2,0.7);


    //shader configuration

    shaderSSAO->use();
    shaderSSAO->setInt("gPosition",0);
    shaderSSAO->setInt("gNormal",1);
    shaderSSAO->setInt("texNoise",2);
    shaderSSAOBlur->use();
    shaderSSAOBlur->setInt("ssaoInput",0);
}

void Renderer::initHdr() {
    exposure = 1.0f;
    gamma = 2.2f;
    std::string dir = "../Shaders/PostProcess/";
    shaderBlur = (new Shader(dir+"hdr.vs.glsl", dir + "hdr-blur.fs.glsl"));
    shaderBright = (new Shader(dir+"hdr.vs.glsl", dir + "brightness.fs.glsl"));
    shaderFinalBloom = (new Shader(dir+"hdr.vs.glsl", dir + "hdr-bloomFinal.fs.glsl"));

    shaderFinalBloom->use();
    shaderFinalBloom->setInt("scene", 0);
    shaderFinalBloom->setInt("bloomBlur", 1);

    shaderBright->use();
    shaderBright->setInt("screenTexture", 0);
}

void Renderer::initFxaa() {
    thresholdMax = 0.1250;
    thresholdMin = 0.0312;
    std::string dir = "../Shaders/PostProcess/";
    shaderFxaa = (new Shader (dir+"fxaa.vs.glsl", dir+"fxaa.fs.glsl"));
}

int Renderer::addObject(Object *object){
    return drawData.objectManager.addValue(object);
}

void Renderer::resize(int w, int h) {
    drawData.cameraManager.changeSize(w,h);
    frameBuffer.resize(w,h);
    gBuffer.resize(w,h);
    ssaoFBO.resize(w,h);
    ssaoBlurFBO.resize(w,h);
    frameFxaa.resize(w,h);
    pingpongFrameBuffer[0].resize(w,h);
    pingpongFrameBuffer[1].resize(w,h);
    frameBlur.resize(w,h);
    frameBright.resize(w,h);
}


void Renderer::toggleDrawMode(){
    m_drawfill=!m_drawfill;
}

std::string Renderer::changeShaderPost(){
    numShaderPP = (numShaderPP+1)%shadersPostProcess.size();
    auto p = shadersPostProcess[numShaderPP];
    screenShader = p.second;
    return p.first;
}

std::string Renderer::changeSkybox(){
    numSkyBoxPP = (numSkyBoxPP+1)%skyboxTextures.size();
    auto p = skyboxTextures[numSkyBoxPP];
    cubemapTexture = p.second;
    return p.first;
}

void Renderer::displayInfo(){
    Log(logInfo) << "|------------- Renderer info ------------";
    drawData.objectManager.displayInfo2();
    drawData.lightManager.displayInfo2();
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";
}

void Renderer::changeModeRenderer(int mode){
    rendererMode = mode;
}

void Renderer::changeRadius(float f) {
    radius = f;
}

void Renderer::changeBiais(float f) {
    biais = f;
}

void Renderer::useHdr(bool b) {
    hdrActivated = b;
}

void Renderer::changeExposure(float f) {
    exposure = f;
}

void Renderer::changeGamma(float f) {
    gamma = f;
}

void Renderer::useFxaa(bool b) {
    fxaaActivated = b;
}

void Renderer::changeThresholdMin(float f) {
    thresholdMin = f;
}

void Renderer::changeThresholdMax(float f) {
    thresholdMax = f;
}