//
// Created by pierre on 26/08/18.
//

#ifndef OGRE_LOADERPLUGIN_HPP
#define OGRE_LOADERPLUGIN_HPP


#include "PluginInterface.hpp"
#include <vector>

//Temporary class to load plugins ...

class LoaderPlugin {
public:
    LoaderPlugin(ContextPlugin contextPlugin);
    void load();

    std::vector<PluginInterface*> plugins;
    ContextPlugin m_context;

};


#endif //OGRE_LOADERPLUGIN_HPP
