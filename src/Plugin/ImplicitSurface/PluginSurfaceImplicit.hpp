#ifndef PLUGINSURFACEIMPLICIT_HPP
#define PLUGINSURFACEIMPLICIT_HPP

#include <Plugin/PluginInterface.hpp>
#include <Plugin/ImplicitSurface/SurfaceImplicitWidget.hpp>

class PluginSurfaceImplicit : public PluginInterface {
public:

    PluginSurfaceImplicit();
    void initData();
    void addFileLoader();
    void addComputeEachDraw();
    void addShortcut();
    void addWidget();

    SurfaceImplicitWidget *widget;

};


#endif //PLUGINGEOMETRY_HPP
