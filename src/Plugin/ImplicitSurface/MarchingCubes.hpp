#ifndef MARCHINGCUBES_HPP
#define MARCHINGCUBES_HPP


#include "src/Core/Macros.hpp"
#include <Engine/Object/Mesh.hpp>



class MarchingCubes {
public :

#define NX 50
#define NY 50
#define NZ 50

    typedef struct {
        double x,y,z;
    } Vector3d;

    typedef struct {
        Vector3d vertices[8];
        Vector3d normals[8];
        double val[8];
    } GridCell;

    typedef struct {
        Vector3d vertices[3];
        Vector3d centroid;
        Vector3d normals[3];
    } Triangles;

    static int polygoniseCube(GridCell grid, double iso, Mesh& m_mesh);
    static Vector3d vertexInterp(double iso, Vector3d v1, Vector3d v2, double valV1, double valV2);
    static int compute(Mesh& mesh, double isolevel, std::vector<Vector3> metaballs);


};


#endif
