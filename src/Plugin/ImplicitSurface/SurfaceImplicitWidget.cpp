#include "SurfaceImplicitWidget.hpp"
#include "ui_SurfaceImplicitWidget.h"
#include <stdio.h>

SurfaceImplicitWidget::SurfaceImplicitWidget(ContextPlugin context, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SurfaceImplicitWidget)
{
    ui->setupUi(this);
    m_context = context;
    objectManager = &m_context.scene->renderer.drawData.objectManager;
    isolevel=ui->lineIsoLevel->text().toDouble();
}

SurfaceImplicitWidget::~SurfaceImplicitWidget()
{
    delete ui;
}

void SurfaceImplicitWidget::on_lineIsoLevel_editingFinished()
{
    isolevel=ui->lineIsoLevel->text().toDouble();
}

void SurfaceImplicitWidget::on_buttonVSI_clicked()
{
    m_context.scene->cleanScene();
    mesh_metaball.m_vertices.clear();
    mesh_metaball.m_indices.clear();
    mesh_metaball.m_normals.clear();
    mesh_metaball.m_texCoord.clear();
    if (metaballs.size()<2)
    {
        // Seulement pour l'init (pas besoin de repush back les 2 metaballs pour mettre à jour le maillage après seulement un changement de l'isolevel
        metaballs.push_back(Vector3(25.0,25.0,25.0));
        metaballs.push_back(Vector3(10.0,10.0,10.0));
    }
    int n = MarchingCubes::compute(mesh_metaball,isolevel,metaballs);
    mesh_metaball.autoNormals();
    objectManager->addValue(new Object(mesh_metaball),"METABALLS");
    m_context.openGLWidget->update();
    m_context.scene->fitScene();
}

void SurfaceImplicitWidget::on_buttonAvancer_clicked()
{
    m_context.scene->cleanScene();
    mesh_metaball.m_vertices.clear();
    mesh_metaball.m_indices.clear();
    mesh_metaball.m_normals.clear();
    mesh_metaball.m_texCoord.clear();
    metaballs[1] = Vector3(metaballs[1].x+1.0,metaballs[1].y+1.0,metaballs[1].z+1.0);
    int n = MarchingCubes::compute(mesh_metaball,isolevel,metaballs);
    mesh_metaball.autoNormals();
    objectManager->addValue(new Object(mesh_metaball),"METABALLS");
    m_context.openGLWidget->update();
}

void SurfaceImplicitWidget::on_buttonReculer_clicked()
{
    m_context.scene->cleanScene();
    mesh_metaball.m_vertices.clear();
    mesh_metaball.m_indices.clear();
    mesh_metaball.m_normals.clear();
    mesh_metaball.m_texCoord.clear();
    metaballs[1] = Vector3(metaballs[1].x-1.0,metaballs[1].y-1.0,metaballs[1].z-1.0);
    int n = MarchingCubes::compute(mesh_metaball,isolevel,metaballs);
    mesh_metaball.autoNormals();
    objectManager->addValue(new Object(mesh_metaball),"METABALLS");
    m_context.openGLWidget->update();
}
