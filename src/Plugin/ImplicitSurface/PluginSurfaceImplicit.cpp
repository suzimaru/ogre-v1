#include "PluginSurfaceImplicit.hpp"

PluginSurfaceImplicit::PluginSurfaceImplicit() {
    m_name = "PluginSurfaceImplicit";
}

void PluginSurfaceImplicit::initData() {
    widget = new SurfaceImplicitWidget(m_context);
}

void PluginSurfaceImplicit::addFileLoader() {

}

void PluginSurfaceImplicit::addWidget() {
    m_context.tabWidget->addTab(widget,"Surface Implicit");
}


void PluginSurfaceImplicit::addShortcut() {

}

void PluginSurfaceImplicit::addComputeEachDraw() {

}
