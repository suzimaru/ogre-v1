#ifndef SURFACEIMPLICITWIDGET_HPP
#define SURFACEIMPLICITWIDGET_HPP

#include <Plugin/PluginInterface.hpp>
#include <Engine/Object/Mesh.hpp>
#include "MarchingCubes.hpp"

#include <QWidget>

namespace Ui {
class SurfaceImplicitWidget;
}

class SurfaceImplicitWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SurfaceImplicitWidget(ContextPlugin context, QWidget *parent = 0);
    ~SurfaceImplicitWidget();

private slots:
    void on_lineIsoLevel_editingFinished();

    void on_buttonVSI_clicked();

    void on_buttonAvancer_clicked();

    void on_buttonReculer_clicked();

private:
    Ui::SurfaceImplicitWidget *ui;
    ContextPlugin m_context;
    ObjectManager *objectManager;
    static MarchingCubes marchingCubes;
    Mesh mesh_metaball;
    std::vector<Vector3> metaballs;
    double isolevel;

};

#endif // SURFACEIMPLICITWIDGET_HPP
