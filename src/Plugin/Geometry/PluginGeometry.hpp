#ifndef PLUGINGEOMETRY_HPP
#define PLUGINGEOMETRY_HPP

#include <Plugin/PluginInterface.hpp>
#include <Plugin/Geometry/GeometryWidget.hpp>

class PluginGeometry : public PluginInterface {
public:

    PluginGeometry();
    void initData();
    void addFileLoader();
    void addComputeEachDraw();
    void addShortcut();
    void addWidget();

    GeometryWidget *widget;

};


#endif //PLUGINGEOMETRY_HPP
