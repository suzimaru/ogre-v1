#include "PluginGeometry.hpp"
#include "GeometryWidget.hpp"
#include <Engine/Renderer/DrawData.hpp>

PluginGeometry::PluginGeometry() {
    m_name = "PluginGeometry";
}

void PluginGeometry::initData() {
    widget = new GeometryWidget(m_context);
}

void PluginGeometry::addFileLoader() {

}

void PluginGeometry::addWidget() {
    m_context.tabWidget->addTab(widget,"Geometry");
}


void PluginGeometry::addShortcut() {

}

void PluginGeometry::addComputeEachDraw() {

}
