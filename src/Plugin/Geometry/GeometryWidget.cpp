#include "GeometryWidget.hpp"
#include "ui_GeometryWidget.h"
#include <Core/Shapes/TriangleMesh.hpp>
#include <OpenMesh/Tools/Subdivider/Uniform/LoopT.hh>
#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>

GeometryWidget::GeometryWidget(ContextPlugin context, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GeometryWidget)
{
    ui->setupUi(this);
    m_context = context;
    objectManager = &m_context.scene->renderer.drawData.objectManager;
}

GeometryWidget::~GeometryWidget()
{
    delete ui;
}

void GeometryWidget::on_buttonSubdiv_clicked()
{
    meshBasic = m_context.scene->renderer.drawData.objectManager.getValue(0)->getMesh();
    m_context.scene->cleanScene();
    loadMeshForSubdivision();
    actualNbPoints = meshBasic.m_vertices.size();
    std::string s = std::to_string(actualNbPoints);
    ui->labelNbpointsInit->setText(QString::fromStdString(s));
}

void GeometryWidget::loadMeshForSubdivision() {
    meshOM = MeshUtils::basicMesh2MeshOM(meshBasic);

//    OpenMesh::Subdivider::Uniform::LoopT<MeshUtils::Mesh_OM> subdiv;
//    subdiv.attach(meshOM);
//    subdiv(1);
//    subdiv.detach();

    MeshUtils::loopSubdivision(meshOM);
    meshBasic = MeshUtils::meshOM2BasicMesh(meshOM);
    meshBasic.autoNormals();
    objectManager->addValue(new Object(meshBasic),"MESHBASIC");
    m_context.openGLWidget->update();

}

void GeometryWidget::on_lineEditNbPoints_editingFinished()
{
    nbPointsToSimplify = ui->lineEditNbPoints->text().toUInt();
}



void GeometryWidget::on_buttonSimplif_clicked()
{
    meshBasic = m_context.scene->renderer.drawData.objectManager.getValue(0)->getMesh();
    m_context.scene->cleanScene();
    meshOM = MeshUtils::basicMesh2MeshOM(meshBasic);

//    OpenMesh::Decimater::DecimaterT<MeshUtils::Mesh_OM>   decimater(meshOM);  // a decimater object, connected to a mesh
//    OpenMesh::Decimater::ModQuadricT<MeshUtils::Mesh_OM>::Handle hModQuadric;      // use a quadric module
//
//    decimater.add(hModQuadric); // register module at the decimater
//    decimater.module( hModQuadric ).unset_max_err();
//    decimater.initialize();       // let the decimater initialize the mesh and the modules
//
//    decimater.decimate_to(nbPointsToSimplify);         // do decimation
//    meshOM.garbage_collection();

    MeshUtils::simplify(meshOM,nbPointsToSimplify);
    meshBasic = MeshUtils::meshOM2BasicMesh(meshOM);
    meshBasic.autoNormals();
    objectManager->addValue(new Object(meshBasic),"MESHBASIC");
    m_context.openGLWidget->update();
    actualNbPoints = meshBasic.m_vertices.size();
    std::string s = std::to_string(actualNbPoints);
    ui->labelNbpointsInit->setText(QString::fromStdString(s));

}
