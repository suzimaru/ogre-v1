#ifndef GEOMETRYWIDGET_HPP
#define GEOMETRYWIDGET_HPP

#include <QWidget>
#include <Plugin/PluginInterface.hpp>
#include <Engine/Object/Mesh.hpp>
#include <Engine/Object/MeshUtils.hpp>

namespace Ui {
class GeometryWidget;
}

class GeometryWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GeometryWidget(ContextPlugin context, QWidget *parent = 0);
    ~GeometryWidget();
    Object *m_bSpline;
    ContextPlugin m_context;

    void loadMeshForSubdivision();

        private slots:
    void on_buttonSubdiv_clicked();

    void on_lineEditNbPoints_editingFinished();

    void on_buttonSimplif_clicked();

private:
    Ui::GeometryWidget *ui;
    ObjectManager *objectManager;

    Mesh meshBasic;
    MeshUtils::Mesh_OM meshOM;
    unsigned int nbPointsToSimplify;
    unsigned long int actualNbPoints;

};

#endif // GEOMETRYWIDGET_HPP
