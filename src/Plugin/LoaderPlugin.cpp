//
// Created by pierre on 26/08/18.
//

#include <src/Plugin/PEngine/PluginPEngine.hpp>
#include <src/Plugin/BSpline/PluginCore/PluginBSpline.hpp>
#include <src/Plugin/Geometry/PluginGeometry.hpp>
#include <src/Plugin/ImplicitSurface/PluginSurfaceImplicit.hpp>
#include <src/Plugin/Animation/PluginAnimation.hpp>
#include "LoaderPlugin.hpp"

LoaderPlugin::LoaderPlugin(ContextPlugin contextPlugin) {

    Log(logInfo) << "";
    Log(logInfo) << "[LOAD PLUGINS]";
    plugins.push_back(new PluginAnimation());
    plugins.push_back(new PluginGeometry());
    plugins.push_back(new PluginSurfaceImplicit());
//    plugins.push_back(new PluginPEngine());
    plugins.push_back(new PluginBSpline());


    m_context=contextPlugin;
    load();

    Log(logInfo) << "[END LOAD PLUGINS]";
    Log(logInfo) << "";
}

void LoaderPlugin::load() {
    for(auto i:plugins){
        i->loadPlugin(m_context);
    }
}
