//
// Created by pierre on 12/09/18.
//

#ifndef OGRE_BSPLINE_HPP
#define OGRE_BSPLINE_HPP


#include <src/Engine/Object/Mesh.hpp>
#include <src/Engine/Object/Object.hpp>

class BSpline {
public:
    // Math
    BSpline(int k, int pointsNumber, std::string nodal);
    BSpline(int k, int pointsNumber, VectorFloat nodal);

    // Link with ogre
    virtual Mesh getMesh(float step = 0.1) = 0;


    VectorFloat m_U; //Vecteur nodal
    float m_k;

};


#endif //OGRE_BSPLINE_HPP
