//
// Created by pierre on 21/09/18.
//

#include <src/Core/Shapes/Sphere.hpp>
#include "BSpline2D.hpp"
#include "Nodals.hpp"

BSpline2D::BSpline2D(std::vector<VectorPoint3> pts, float k, std::string nodal):
    BSpline(k, pts[0].size(), nodal), m_pts{pts}, m_nodal{nodal} {

    //m_k = k;

    if(pts.size() == 0){ Log(logError) << "BSpline2D can't init with pts empty ..."; return;}
    int s = pts[0].size();
    bsplineU = pts.size();
    bsplineV = s;

    for(auto i:pts){
        if((int)i.size() != s){
            Log(logError) << "BSpline2D can't init with different size int pts ...";
            return;
        }
    }

    for(auto i: pts){
        BSpline1D bSpline1D(i,k,m_U);
        m_bsplines.push_back(bSpline1D);
    }

}


Mesh BSpline2D::getMesh(float step){
    Mesh m;

    VectorPoint3 pts;
    Point3 pt;

    int ptV = 0,ptU = 0;


    Nodals nodal(m_k,m_bsplines.size(), m_nodal);
    for(auto u = m_U[m_k-1]; u< m_U[bsplineV];u+=step){

        pts.clear();
        for(auto spline : m_bsplines){
            pts.push_back(spline.compute(u));
        }

        BSpline1D bSpline1D(pts, m_k, nodal.getVector());
        VectorPoint3 points = bSpline1D.getPoints(step);
        for(auto i:points) {

            m.m_vertices.push_back(i);
            m.m_texCoord.push_back(Vector3(0));
        }

        ptU = points.size();
        ptV ++;

    }

    for(auto v = 0; v< ptV-1;v++){

        for(auto u = 0; u< ptU-1;u++) {
            int i1=v*ptU+u;
            int i2=i1+1;
            int i3=(v+1)*ptU+u;
            int i4=i3+1;
            m.setTriangle(Triangle(i1,i4,i2));
            m.setTriangle(Triangle(i1,i3,i4));
        }

    }

    m.autoNormals();


    return m;
}



std::vector<std::vector<Object*>> BSpline2D::getObjectPoints() {
    std::vector<std::vector<Object*>> res;
    std::vector<Object*> objs;
    for(auto i:m_pts){
        objs.clear();
        for(auto j:i){
            Object *obj = new Object(createSphereShape(0.1));
            obj->translate(j);
            objs.push_back(obj);
        }
        res.push_back(objs);
    }
    return res;
}