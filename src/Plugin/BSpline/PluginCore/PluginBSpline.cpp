//
// Created by pierre on 16/09/18.
//

#include "PluginBSpline.hpp"
#include "BSplineWidget.hpp"
#include "BSplineShortcut.hpp"

PluginBSpline::PluginBSpline() {
    m_name = "PluginBSpline";
}

void PluginBSpline::initData() {
    m2Widget = new M2Widget(m_context);
    m2Shortcut = new M2Shortcut(m2Widget);
}

void PluginBSpline::addFileLoader() {

}

void PluginBSpline::addWidget() {
    m_context.tabWidget->addTab(m2Widget,"BSpline");
}

void PluginBSpline::addShortcut() {
    m_context.scene->shortCuts.push_back(m2Shortcut);
}

void PluginBSpline::addComputeEachDraw() {

}