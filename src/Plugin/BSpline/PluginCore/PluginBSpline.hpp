//
// Created by pierre on 16/09/18.
//

#ifndef OGRE_PLUGINM2_HPP
#define OGRE_PLUGINM2_HPP

#include <Plugin/PluginInterface.hpp>
#include "BSplineShortcut.hpp"

class PluginBSpline : public PluginInterface {
public:

    PluginBSpline();
    void initData();
    void addFileLoader();
    void addComputeEachDraw();
    void addShortcut();
    void addWidget();

    M2Widget * m2Widget;
    M2Shortcut *m2Shortcut;

};


#endif //OGRE_PLUGINM2_HPP
