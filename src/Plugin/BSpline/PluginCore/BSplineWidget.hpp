#ifndef M2WIDGET_H
#define M2WIDGET_H

#include <QWidget>
#include <Plugin/PluginInterface.hpp>
#include <QtWidgets/QSpinBox>

namespace Ui {
class M2Widget;
}

class M2Widget : public QWidget
{
    Q_OBJECT

public:
    explicit M2Widget(ContextPlugin context, QWidget *parent = 0);
    ~M2Widget();

    std::vector<Object*> m_pointsObjects2D;
    std::vector<std::vector<Object*>> m_pointsObjects3D;
    std::string nodal = "uniform";
    int m_k;
    Object *m_bSpline;
    ContextPlugin m_context;

    void activatingDemo(int i);
    void loadBSpline2D(std::vector<Vector3> pts, int k);
    void loadBSpline3D(std::vector<VectorPoint3> pts, int k);
    void reloadBSpline();
    bool mode2d;

private slots:
    void on_buttonDemo1_clicked();

    void on_buttonDemo2_clicked();

    void on_spinBoxK_valueChanged(int arg1);

    void on_comboBox_activated(int index);

    void on_buttonVisible_clicked();

    void on_doubleSpinBox_valueChanged(double arg1);

private:
    Ui::M2Widget *ui;
    ObjectManager *objectManager;
    QSpinBox *spinBoxK;
    float step = 0.1;
    bool visible = true;
    void loadBSpline();

};

#endif // M2WIDGET_H
