//
// Created by pierre on 21/09/18.
//

#ifndef OGRE_BSPLINE3D_HPP
#define OGRE_BSPLINE3D_HPP


#include <src/Core/Macros.hpp>
#include <src/Engine/Object/Object.hpp>
#include "BSpline.hpp"
#include "BSpline1D.hpp"

class BSpline2D : public BSpline{
public:

    BSpline2D(std::vector<VectorPoint3> pts, float k, std::string nodal = "uniform");

    /* Obtenir le maillage 2D */
    Mesh getMesh(float step = 0.1) override;

    /* Obtenir les points de contrôles */
    std::vector<std::vector<Object*>> getObjectPoints();
private:
    std::vector<VectorPoint3> m_pts;  // Points de contrôle
    std::vector<BSpline1D> m_bsplines;

    std::string m_nodal;
    int bsplineU;
    int bsplineV;

};


#endif //OGRE_BSPLINE3D_HPP
