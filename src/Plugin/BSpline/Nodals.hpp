//
// Created by pierre on 09/10/18.
//

#ifndef OGRE_NODALS_HPP
#define OGRE_NODALS_HPP

#include <src/Core/Macros.hpp>

class Nodals {
public:

    Nodals(int k, int pointsNumber, std::string nodal);

    VectorFloat getVector();

private:
    int m_k;
    VectorFloat m_U;

};


#endif //OGRE_NODALS_HPP
