//
// Created by pierre on 26/08/18.
//

#ifndef OGRE_PLUGININTERFACE_HPP
#define OGRE_PLUGININTERFACE_HPP

#include <src/Engine/Loader/FileLoaderManager.hpp>
#include <src/Gui/OpenGLWidget.hpp>
#include <QtWidgets/QMenu>
#include <src/Gui/MainWindow.hpp>

struct ContextPlugin{
    FileLoaderManager *fileLoaderManager;
    OpenGLWidget *openGLWidget;
    Scene *scene;
    QTabWidget *tabWidget;
};

class PluginInterface {
public:

    std::string m_name = "NO NAME PLUGIN";

    void loadPlugin(ContextPlugin contextPlugin){
        Log(logInfo) << "[" << m_name << "] loading";
        m_context = contextPlugin;
        initData();
        addFileLoader();
        addComputeEachDraw();
        addShortcut();
        addWidget();
    }

    virtual void initData() = 0;
    virtual void addFileLoader() = 0;
    virtual void addComputeEachDraw() = 0;
    virtual void addShortcut() = 0;
    virtual void addWidget() = 0;

    ContextPlugin m_context;
};

#endif //OGRE_PLUGININTERFACE_HPP
