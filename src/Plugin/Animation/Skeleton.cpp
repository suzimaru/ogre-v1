#include <src/Plugin/Animation/Skeleton.hpp>

Skeleton::Skeleton() { }

Skeleton::~Skeleton() { }

int Skeleton::addBone(const int parent, const Transform& T) {

    if (parent == -1) {
        restPose.push_back(T);
        currentPose.push_back(T);
    } else {
        restPose.push_back(glm::inverse(restPose[parent]) * T);
        currentPose.push_back(glm::inverse(restPose[parent]) * T);
    }
    tree.addNode(parent);
    return (size()-1);
}

void Skeleton::applyTransform(uint indexBone, Transform& t, int recur) {
    if (!recur)
        currentPose = restPose;
    currentPose[indexBone] = t;
    for (auto& child : tree.children[indexBone]) {
        currentPose[child] = t * restPose[child];
        applyTransform(child,t,1);
    }
}


void Skeleton::clear() {
    tree.clear();
    currentPose.clear();
    restPose.clear();
}