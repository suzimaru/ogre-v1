#ifndef TREE_HPP
#define TREE_HPP

#include <src/Core/Macros.hpp>

class Tree {
public:
    typedef std::vector<int> Parents;
    typedef std::vector<int> Children;

    typedef std::vector<Children> Parentage;

    Tree();
    ~Tree();

    uint size();
    void clear();

    //pour ajouter la racine, mettre -1
    uint addNode(int parent); // return l'index the la feuille ajoutée

    Parentage children;//vecteur des fils des parents
    Parents parents; //vecteur d'index des parents
};

#endif //TREE_HPP