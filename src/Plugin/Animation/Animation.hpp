#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include "src/Core/Macros.hpp"
#include <Engine/Object/Mesh.hpp>

class Animation {

public:

    void computeWeight();

private:
    std::vector<std::vector<GLfloat>> weights;

    std::vector<Point3> bones;

    std::vector<Matrix4> restPose;
    std::vector<Matrix4> currentPose;

};

#endif // ANIMATION_HPP