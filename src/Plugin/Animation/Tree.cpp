#include <src/Plugin/Animation/Tree.hpp>

Tree::Tree() : children(), parents() { }
Tree::~Tree() { }

uint Tree::addNode(int parent) {
    uint idx = size();
    children.push_back(Children()); //la nouvelle node n'a pas d'enfant
    if (parent >= 0) { // != de racine
        children[parent].push_back(idx); // le précédent
    }
    parents.push_back(parent); //ajouter le parent à la nouvelle node
    return idx;
}

uint Tree::size() {
    return parents.size();
}

void Tree::clear() {
    children.clear();
    parents.clear();
}
