#ifndef PLUGINANIMATION_HPP
#define PLUGINANIMATION_HPP

#include <Plugin/PluginInterface.hpp>
#include <Plugin/Animation/AnimationWidget.hpp>
#include "AnimationShortcut.hpp"

class PluginAnimation : public PluginInterface {
public:

    PluginAnimation();
    void initData();
    void addFileLoader();
    void addComputeEachDraw();
    void addShortcut();
    void addWidget();

    AnimationWidget *widget;
    AnimationShortcut *shortcut;

};


#endif //PLUGINANIMATION_HPP
