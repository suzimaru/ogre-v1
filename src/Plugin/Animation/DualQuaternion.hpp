//
// Created by suzy on 09/12/18.
//

#ifndef OGRE_DUALQUATERNION_HPP
#define OGRE_DUALQUATERNION_HPP

#include <src/Core/Macros.hpp>

class DualQuaternion {
public:

    inline DualQuaternion(Quaternion q0, Quaternion qe) {m_q0 = q0; m_qe = qe;}
    inline DualQuaternion(Transform& tr) {transform2DualQuaternion(tr);}
    inline DualQuaternion() {};

//    DualQuaternion( DualQuaternion other ) = default;
    DualQuaternion& operator= ( const DualQuaternion& ) = default;

    inline Quaternion& getQ0() {return m_q0;}
    inline void setQ0(Quaternion& q0) {m_q0 = q0;}
    inline Quaternion& getQe() {return m_qe;}
    inline void setQe(Quaternion& qe) {m_qe = qe;}

    inline DualQuaternion operator+ (DualQuaternion dq) {return DualQuaternion (m_q0 + dq.m_q0 , m_qe + dq.m_qe); }
    inline DualQuaternion operator* (float scalar) {return DualQuaternion (Quaternion(scalar * m_q0), Quaternion(scalar * m_qe)); }

    inline Quaternion transform2quat(Transform& t) {
        // trace of t
        float trace = 1 + t[0][0] + t[1][1] + t[2][2];

        float s, x, y, z, w;

        if ( trace > 0.00000001f ) // pour les distorsions (trace>0 et s!=0)
        {
            s = sqrt(trace) * 2.f;
            w = 0.25f * s;
            x = (t[2][1] - t[1][2])/s;
            y = (t[0][2] - t[2][0])/s;
            z = (t[1][0] - t[0][1])/s;
        }
        else
        {
            if (t[0][0] > t[1][1] && t[0][0] > t[2][2])
            {
                // Column 0 :
                s  = sqrt( 1.0f + t[0][0] - t[1][1] - t[2][2] ) * 2.f;
                w = (t[2][1] - t[1][2])/s;
                x = 0.25f * s;
                y = (t[1][0] + t[0][1])/s;
                z = (t[0][2] + t[2][0])/s;
            }
            else if (t[1][1] > t[2][2])
            {
                // Column 1 :
                s  = sqrt(1.0f + t[1][1] - t[0][0] - t[2][2]) * 2.f;
                w = (t[0][2] - t[2][0])/s;
                x = (t[1][0] + t[0][1])/s;
                y = 0.25f * s;
                z = (t[2][1] + t[1][2])/s;
            }
            else
            {   // Column 2 :
                s  = sqrt(1.0f + t[2][2] - t[0][0] - t[1][1]) * 2.f;
                w = (t[1][0] - t[0][1])/s;
                x = (t[0][2] + t[2][0])/s;
                y = (t[2][1] + t[1][2])/s;
                z = 0.25f * s;
            }
        }

        return Quaternion(-w,x,y,z);
    }

    inline void transform2DualQuaternion(Transform t) {
        glm::quat q = transform2quat(t);
        Vector3 v{t[3][0],t[3][1],t[3][2]};

        float w = -0.5f*( v.x * q.x + v.y * q.y + v.z * q.z);
        float i =  0.5f*( v.x * q.w + v.y * q.z - v.z * q.y);
        float j =  0.5f*(-v.x * q.z + v.y * q.w + v.z * q.x);
        float k =  0.5f*( v.x * q.y - v.y * q.x + v.z * q.w);

        m_q0 = q;
        m_qe = glm::quat(w,i,j,k);
    }

    Vector3 transformDQS( Vector3 p) {
        Vector3 realxyz{m_q0.x, m_q0.y, m_q0.z};
        Vector3 dualxyz{m_qe.x, m_qe.y, m_qe.z};
        Vector3 trans = (dualxyz * m_q0.w - realxyz * m_qe.w + glm::cross(realxyz, dualxyz)) * 2.f;
        Vector3 rot = p + glm::cross((realxyz * 2.f), glm::cross(realxyz, p) + p * m_q0.w);
        return trans + rot;
    }

    inline void normalize()
    {
        float norm = glm::length(m_q0);
        m_q0 = m_q0 / norm;
        m_qe = m_qe / norm;
    }

private:
    //rotation
    Quaternion m_q0;
    //translation
    Quaternion m_qe;
};

#endif //OGRE_DUALQUATERNION_HPP
