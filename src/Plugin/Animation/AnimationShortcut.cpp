//
// Created by suzy on 22/11/18.
//

#include "AnimationShortcut.hpp"
#include <stdio.h>

AnimationShortcut::AnimationShortcut(AnimationWidget *animWidget) {
    id_shortcut = "Animation shortcut";
    m_AnimWidget = animWidget;
}

bool AnimationShortcut::keyboard(unsigned char k) {
    return false;
}

void AnimationShortcut::mouseclick(ButtonMouse button, float xpos, float ypos) {
    if(button==LEFT_BUTTON){
        objClicked=m_AnimWidget->m_context.scene->renderer.drawData.rayCast(xpos,ypos);
        if (objClicked==nullptr)
            return;
        m_AnimWidget->objectSelected = objClicked;
        m_AnimWidget->changeObjectSelected();
        if(objClicked){
            for(auto i:m_AnimWidget->m_pointsObjects3D){
                    if(i->getName()==objClicked->getName()){
                        posObjClicked = objClicked->getPositionModel();
                        return;
                }
            }
            objClicked = nullptr;
        }
    }
}

void AnimationShortcut::mousemove(ButtonMouse button, float xpos, float ypos) {

}