#include "PluginAnimation.hpp"

PluginAnimation::PluginAnimation() {
    m_name = "PluginAnimation";
}

void PluginAnimation::initData() {
    widget = new AnimationWidget(m_context);
    shortcut = new AnimationShortcut(widget);
}

void PluginAnimation::addFileLoader() {

}

void PluginAnimation::addWidget() {
    m_context.tabWidget->addTab(widget,"Animation");
}


void PluginAnimation::addShortcut() {
    m_context.scene->shortCuts.push_back(shortcut);
}

void PluginAnimation::addComputeEachDraw() {

}

// sur les colonnes : les os
// sur les lignes, le nombre de sommets
std::vector<std::vector<float>> weights;