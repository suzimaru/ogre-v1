#ifndef ANIMATIONWIDGET_H
#define ANIMATIONWIDGET_H

#include <QWidget>
#include <Plugin/PluginInterface.hpp>
#include <Core/Shapes/Cylinder.hpp>
#include <Core/Shapes/Sphere.hpp>
#include <Core/Shapes/Grid.hpp>
#include <Plugin/Animation/Skeleton.hpp>
#include <src/Plugin/Animation/DualQuaternion.hpp>

namespace Ui {
class AnimationWidget;
}

class AnimationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AnimationWidget(ContextPlugin context, QWidget *parent = 0);
    ~AnimationWidget();
    ContextPlugin m_context;
    std::vector<Object*> m_pointsObjects3D;
    Object *objectCylinder;
    Object *objectSelected;
    void applyLBS(float ang);
    void computeDQ(std::vector<DualQuaternion>& dqList);
    void applyDQS(float ang);
    void updateLines();
    void clean();
    void changeObjectSelected();
    void createCylinder();

private slots:


    void on_buttonCylinder_clicked();

    void on_checkBoxLBS_clicked(bool checked);

    void on_checkBoxDQS_clicked(bool checked);

    void on_comboBoxRotation_activated(int index);

    void on_buttonLess_clicked();

    void on_buttonPlus_clicked();


    void on_doubleSpinBoxAngle_editingFinished();

    void on_sliderDist_valueChanged(int value);

private:
    Ui::AnimationWidget *ui;
    ObjectManager *objectManager;
    Mesh cylinder, grid;
    Object* oline1, *oline2;
    Mesh axeX, axeY, axeZ;
    Object *oaxeX,*oaxeY,*oaxeZ;
    Mesh mline1, mline2;
    Point3 Bone1Begin,Bone1End, Bone2Begin,Bone2End;
    int modeDeform;
    float distanceBone;
    float angle;

    std::vector<std::vector<float>> weights;
    Skeleton skeleton;

    Vector3 axeRotation;




};

#endif // ANIMATIONWIDGET_H
