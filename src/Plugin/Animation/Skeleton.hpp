#ifndef SKELETON_HPP
#define SKELETON_HPP

#include <src/Core/Macros.hpp>
#include <src/Plugin/Animation/Tree.hpp>

class Skeleton {

public:

    Skeleton();
    ~Skeleton();

    int addBone(const int parent = -1, const Transform& T = Identity());

    inline uint size() {
        return tree.size();
    }

    void applyTransform(uint indexBone, Transform& t, int recur);

    void clear();

    Tree tree;

    Pose restPose;
    Pose currentPose;

};

#endif //SKELETON_HPP