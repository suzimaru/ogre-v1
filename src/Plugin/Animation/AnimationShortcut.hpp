//
// Created by suzy on 22/11/18.
//

#ifndef OGRE_ANIMATIONSHORTCUT_HPP
#define OGRE_ANIMATIONSHORTCUT_HPP

#include <src/Gui/ShortcutInterface.hpp>
#include <Core/Macros.hpp>
#include <Engine/Object/Object.hpp>
#include <Plugin/Animation/AnimationWidget.hpp>


class AnimationShortcut : public ShortcutInterface {

public:
    AnimationShortcut(AnimationWidget *animWidget);

    enum mode_move{FREE = 0, XY = 1, YZ =2, XZ =3};

    bool keyboard(unsigned char k) override;
    void mouseclick(ButtonMouse button, float xpos, float ypos) override;
    void mousemove(ButtonMouse button, float xpos, float ypos) override;

    AnimationWidget *m_AnimWidget;
    Object *objClicked;
    Point3 posObjClicked;
    mode_move m_mode_move = XY;

};


#endif //OGRE_ANIMATIONSHORTCUT_HPP
