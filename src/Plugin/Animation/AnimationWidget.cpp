#include "AnimationWidget.hpp"
#include "ui_AnimationWidget.h"
#include <src/Engine/Material/MaterialManager.hpp>
#include <src/Engine/Shader/ShaderManager.hpp>


AnimationWidget::AnimationWidget(ContextPlugin context, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::AnimationWidget)
{
    ui->setupUi(this);
    m_context = context;
    objectManager = &m_context.scene->renderer.drawData.objectManager;
    objectSelected=nullptr;
}

AnimationWidget::~AnimationWidget()
{
    delete ui;
}

void AnimationWidget::clean() {
    objectManager->remValue(oline1);
    objectManager->remValue(oline2);
    m_context.scene->cleanScene();
    for (int i = 0; i < weights.size(); ++i)
        weights[i].clear();
    weights.clear();
    m_pointsObjects3D.clear();
    skeleton.clear();
    m_context.openGLWidget->update();
}

void AnimationWidget::createCylinder() {
    clean();
////////////////////////////////////////////// INITIALISATION DES OUTILS ///////////////////////////////////////////////////////
    angle = ui->doubleSpinBoxAngle->value();
    (ui->checkBoxLBS->isChecked()) ? modeDeform = 0 : modeDeform = 1;
    on_comboBoxRotation_activated(ui->comboBoxRotation->currentIndex());
    grid = createGridShape();
    distanceBone = ui->sliderDist->value();
    cylinder = createCylinderShapeAnim(weights,(distanceBone/100.0));
    m_context.scene->cleanScene();

    std::string dir = "../Shaders/";
    m_context.scene->renderer.drawData.shaderManager.addValue("weights",new Shader(dir+"Weights.vs.glsl",dir+"Weights.fs.glsl"));

    Material *weightsMat = new Material(Vector3(1));
    weightsMat->setShader("weights");
    m_context.scene->renderer.drawData.materialManager.addValue("weights",weightsMat);

    Material *yellow = new Material(Vector3(0.5,0.5,0.1));
    Material *purple = new Material(Vector3(0.5,0.1,0.5));
    Material *green = new Material(Vector3(0,0.8,0));
    Material *red = new Material(Vector3(0.8,0,0));
    Material *blue = new Material(Vector3(0.0,0.0,0.8));
    m_context.scene->renderer.drawData.materialManager.addValue("yellow",yellow);
    m_context.scene->renderer.drawData.materialManager.addValue("purple",purple);
    m_context.scene->renderer.drawData.materialManager.addValue("red",red);
    m_context.scene->renderer.drawData.materialManager.addValue("green",green);
    m_context.scene->renderer.drawData.materialManager.addValue("blue",blue);

    objectCylinder = new Object(cylinder);
    objectCylinder->setMaterial("weights");
    objectManager->addValue(objectCylinder,"CYLINDER");

    axeX.setMeshTypes(GL_LINES);
    axeY.setMeshTypes(GL_LINES);
    axeZ.setMeshTypes(GL_LINES);
    axeX.addLine(Point3(0,0,0),Point3(1,0,0));
    axeY.addLine(Point3(0,0,0),Point3(0,1,0));
    axeZ.addLine(Point3(0,0,0),Point3(0,0,1));

    oaxeX = new Object(axeX);
    oaxeX->setMaterial("red");
    oaxeX->setPositionModel(Vector3(-5.0,0.0,5.0));

    oaxeY = new Object(axeY);
    oaxeY->setMaterial("green");
    oaxeY->setPositionModel(Vector3(-5.0,0.0,5.0));

    oaxeZ = new Object(axeZ);
    oaxeZ->setMaterial("blue");
    oaxeZ->setPositionModel(Vector3(-5.0,0.0,5.0));

    objectManager->addValue(oaxeX,"AXEX");
    objectManager->addValue(oaxeY,"AXEY");
    objectManager->addValue(oaxeZ,"AXEZ");


    ////////////////////////////////////////////// INITIALISATION DU SQUELETTE //////////////////////////////////////////////////

    //ajout des 2 os du cylindre
    Transform os1(1.0,0.0,0.0,-5.0,
                  0.0,1.0,0.0,0.0,
                  0.0,0.0,1.0,0.0,
                  0.0,0.0,0.0,1.0);

    Transform os2 = Transform(1.0,0.0,0.0,0.0,
                              0.0,1.0,0.0,0.0,
                              0.0,0.0,1.0,0.0,
                              0.0,0.0,0.0,1.0);

    Transform os3 = Transform(1.0,0.0,0.0,5.0,
                              0.0,1.0,0.0,0.0,
                              0.0,0.0,1.0,0.0,
                              0.0,0.0,0.0,1.0);

    int idx = skeleton.addBone(-1,os1);
    idx = skeleton.addBone(idx,os2);
    idx = skeleton.addBone(idx,os3);

    Object *pt1 = new Object(createSphereShape(0.3));
    pt1->translate(translation(os1));

    Object *pt2 = new Object(createSphereShape(0.3));
    pt2->translate(translation(os2));

    Object *pt3 = new Object(createSphereShape(0.3));
    pt3->translate(translation(os3));

    objectManager->addValue(pt1,"OS1");
    objectManager->addValue(pt2,"OS2");
    objectManager->addValue(pt3,"OS3");
    pt1->setName("OS1");
    pt2->setName("OS2");
    pt3->setName("OS3");
    m_pointsObjects3D.push_back(pt1);
    m_pointsObjects3D.push_back(pt2);
    m_pointsObjects3D.push_back(pt3);

    updateLines();

    m_context.openGLWidget->update();
    m_context.scene->fitScene();
}

void AnimationWidget::on_buttonCylinder_clicked()
{
    createCylinder();
}

void AnimationWidget::changeObjectSelected() {
    const char* name = objectSelected->getName().c_str();
    ui->labelObjectSelected->setText(QString(name));
}


void AnimationWidget::on_checkBoxLBS_clicked(bool checked)
{
    ui->checkBoxDQS->setChecked(!checked);
    modeDeform = 0;
}

void AnimationWidget::on_checkBoxDQS_clicked(bool checked)
{
    ui->checkBoxLBS->setChecked(!checked);
    modeDeform = 1;
}

void AnimationWidget::on_comboBoxRotation_activated(int index)
{
    switch(index) {
        case 0:
            axeRotation = Vector3(1.0, 0.0, 0.0);
            break;
        case 1:
            axeRotation = Vector3(0.0, 1.0, 0.0);
            break;
        case 2:
            axeRotation = Vector3(0.0, 0.0, 1.0);
            break;
    }
}

void AnimationWidget::on_buttonLess_clicked()
{
    if (objectSelected == nullptr || objectSelected->getName() == "None")
        return;

    float a = -angle;
    if (modeDeform==0)
        applyLBS(a);
    else
        applyDQS(a);
}

void AnimationWidget::on_buttonPlus_clicked()
{
    if (objectSelected == nullptr || objectSelected->getName() == "None")
        return;

    if (modeDeform==0)
        applyLBS(angle);
    else
        applyDQS(angle);
}

void AnimationWidget::computeDQ(std::vector<DualQuaternion>& dqList) {
    dqList.clear();
    dqList.resize(cylinder.m_vertices.size(),DualQuaternion(Quaternion(0,0,0,0),Quaternion(0,0,0,0)));
    std::vector<DualQuaternion> poseDq;
    poseDq.reserve(skeleton.currentPose.size());

    //convert all transforms to dualquaternion
    std::vector<Transform> blendingMatrices;
    for (int j = 0; j < skeleton.restPose.size()-1; ++j) {
        blendingMatrices.push_back(skeleton.currentPose[j] * glm::inverse(skeleton.restPose[j]));
        poseDq.push_back(DualQuaternion(blendingMatrices[j]));
    }

    //for all vertices blend the dual quats.
    for (int i = 0; i < cylinder.m_vertices.size(); ++i) {
        for (int j = 0; j < skeleton.restPose.size()-1; ++j) {
            dqList[i] = dqList[i] + (poseDq[j] * weights[j][i]);
        }
        dqList[i].normalize();
    }
}


void AnimationWidget::applyDQS(float ang) {
    if (objectSelected == nullptr)
        return;

    std::string name = objectSelected->getName();

    int index = -1;
    if (name == "OS1")
        index = 0;
    else if (name == "OS2")
        index = 1;
    else
        return;

    Transform t = glm::rotate(skeleton.restPose[index], glm::radians(ang),axeRotation);
    skeleton.applyTransform(index,t,0);
    updateLines();


    std::vector<DualQuaternion> dqList;
    std::vector<Vector3> verticesOut;
    verticesOut.resize(cylinder.m_vertices.size(),Vector3(0.0));
    computeDQ(dqList);

    for (int i = 0; i < cylinder.m_vertices.size(); ++i) {
        verticesOut[i] = dqList[i].transformDQS(cylinder.m_vertices[i]);
    }

    cylinder.m_vertices.clear();
    for (int i = 0; i < verticesOut.size(); ++i) {
        cylinder.m_vertices.push_back(verticesOut[i]);
    }
    cylinder.m_normals.clear();
    cylinder.autoNormals();

    objectCylinder->setMesh(cylinder);
    m_context.openGLWidget->update();
}


void AnimationWidget::applyLBS(float ang) {
    if (objectSelected == nullptr)
        return;

    std::string name = objectSelected->getName();

    int index = -1;
    if (name == "OS1")
        index = 0;
    else if (name == "OS2")
        index = 1;
    else
        return;

    Transform t = glm::rotate(skeleton.restPose[index], glm::radians(ang),axeRotation);
    skeleton.applyTransform(index,t,0);
    updateLines();

    std::vector<Vector3> verticesOut;
    verticesOut.resize(cylinder.m_vertices.size(),Vector3(0.0));

    std::vector<Transform> blendingMatrices;

    for (int j = 0; j < skeleton.restPose.size()-1; j++) {
        blendingMatrices.push_back(skeleton.currentPose[j] * glm::inverse(skeleton.restPose[j]));
    }

    for (int i = 0; i < cylinder.m_vertices.size(); ++i) {
        Transform tmp(0);
        for (int j = 0; j < skeleton.restPose.size()-1; ++j) {
            float w = weights[j][i];
            tmp = tmp + w * blendingMatrices[j];
        }
        Vector4 tmpV = tmp * Vector4(cylinder.m_vertices[i],1.0);
        verticesOut[i] = Vector3(tmpV.x,tmpV.y,tmpV.z);
    }

    cylinder.m_vertices.clear();
    for (int i = 0; i < verticesOut.size(); ++i) {
        cylinder.m_vertices.push_back(verticesOut[i]);
    }
    cylinder.m_normals.clear();
    cylinder.autoNormals();

    objectCylinder->setMesh(cylinder);
    m_context.openGLWidget->update();
}

void AnimationWidget::updateLines() {
    objectManager->remValue(oline1);
    objectManager->remValue(oline2);
    mline2.m_texCoord.clear();
    mline2.m_normals.clear();
    mline2.m_vertices.clear();
    mline2.m_indices.clear();

    mline1.m_texCoord.clear();
    mline1.m_normals.clear();
    mline1.m_vertices.clear();
    mline1.m_indices.clear();

    mline1.m_vertices.clear();
    mline1.m_indices.clear();
    mline1.m_normals.clear();
    mline1.m_texCoord.clear();

    mline2.m_vertices.clear();
    mline2.m_indices.clear();
    mline2.m_normals.clear();
    mline2.m_texCoord.clear();

    for (int j = 0; j < skeleton.restPose.size(); j++) {
        Vector4 pos = skeleton.currentPose[j] * Vector4(m_pointsObjects3D[j]->getPositionModel(), 1);
        m_pointsObjects3D[j]->setPositionModel(Vector3(pos.x, pos.y, pos.z));
    }
    mline1.setMeshTypes(GL_LINES);
    mline2.setMeshTypes(GL_LINES);

    mline1.addLine(m_pointsObjects3D[0]->getPositionModel(),m_pointsObjects3D[1]->getPositionModel());
    mline1.addLine(m_pointsObjects3D[1]->getPositionModel(),m_pointsObjects3D[2]->getPositionModel());

    oline1 = new Object(mline1);
    oline2 = new Object(mline2);

    oline1->setMaterial("yellow");
    oline2->setMaterial("purple");

    objectManager->addValue(oline1,"LINE1");
    objectManager->addValue(oline2,"LINE2");

    m_context.openGLWidget->update();
}

void AnimationWidget::on_doubleSpinBoxAngle_editingFinished()
{
    angle = ui->doubleSpinBoxAngle->value();
}

void AnimationWidget::on_sliderDist_valueChanged(int value)
{
    createCylinder();
}

