//
// Created by pierre on 26/08/18.
//

#include <src/Engine/Loader/OgreLoader.hpp>
#include <src/Plugin/PEngine/Loader/POgreLoader.hpp>
#include "PluginPEngine.hpp"
#include "PhysicShortcut.hpp"
#include "PWidget.hpp"

PluginPEngine::PluginPEngine() {
    m_name = "PEngine";

    pEngine = new PEngine();
}

void PluginPEngine::initData() {

}

void PluginPEngine::addFileLoader(){
    m_context.fileLoaderManager->addValue(new POgreLoader(pEngine));
}

void PluginPEngine::addComputeEachDraw() {
    m_context.scene->computeEachDraw.push_back(pEngine);
}

void PluginPEngine::addShortcut() {
    m_context.scene->shortCuts.push_back(new PhysicShortcut(pEngine));
}

void PluginPEngine::addWidget() {
    m_context.tabWidget->addTab(new PWidget(m_context.tabWidget,pEngine),"Physic");
}
