//
// Created by pierre on 20/08/18.
//

#include <QtCore/QDateTime>
#include "PEngine.hpp"

PEngine::PEngine() : ManagerList<PObject*>("PhysicEngine"){
    id_computeEachDraw= "pEngine";
}

PEngine::~PEngine(){

}

void PEngine::computeEachDraw(){
    if(!active) return;
    utilDataP.deltaT=chrono.getDeltaMark();
    utilDataP.traceActive = activeTracebool;
    for(auto i: getValues()) {
        i->computeForce(utilDataP);
    }
    computeDeformationSpring();
}

void PEngine::addSpring(PSpring * spring){
    springList.push_back(spring);
}

void PEngine::computeDeformationSpring() {
    for(auto i: springList){
        i->deformation();
    }
}

bool PEngine::activeTrace() {
    activeTracebool=!activeTracebool;
    return activeTracebool;
}


void PEngine::cleanUp() {
    ManagerList::remAll();
    springList.clear();
    active=false;
    chrono.pause();
}

bool PEngine::playPause() {
    active=!active;
    if(active) {
        chrono.start();
    }else{
        chrono.pause();
    }
    return active;
}

void PEngine::displayInfo(){
    Log(logInfo) << "";
    Log(logInfo) << "|---------- Info Physic Engine ----------";
    Log(logInfo) << "| - Active trace: " << (activeTracebool?"true":"false");
    Log(logInfo) << "| - PObject number: " << getValues().size();
    Log(logInfo) << "| - PSpring number: " << springList.size();
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";
}

void PEngine::displayInfo2() {
    Log(logInfo) << "";
    Log(logInfo) << "|---------- Info Physic Engine ----------";
    for(auto i:getValues()){
        i->displayInfo();
    }
    for(auto i:springList){
        i->displayInfo();
    }
    Log(logInfo) << "|----------------------------------------";
    Log(logInfo) << "";
}
