//
// Created by pierre on 26/08/18.
//

#ifndef OGRE_PLUGINPENGINE_HPP
#define OGRE_PLUGINPENGINE_HPP

#include <Plugin/PluginInterface.hpp>
#include <Plugin/PEngine/PEngine.hpp>

class PluginPEngine : public PluginInterface {
public:

    PluginPEngine();
    void initData();
    void addFileLoader();
    void addComputeEachDraw();
    void addShortcut();
    void addWidget();

    PEngine *pEngine;


};


#endif //OGRE_PLUGINPENGINE_HPP
