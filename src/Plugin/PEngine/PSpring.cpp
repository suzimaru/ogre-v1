//
// Created by pierre on 20/08/18.
//

#include <src/Core/Shapes/Cylinder.hpp>
#include <Core/Log/Log.hpp>
#include <src/Engine/Object/Object.hpp>
#include "PSpring.hpp"


PSpring::PSpring(PObject * o1, PObject * o2, float stif, float length ): Object(createCylinderShape(1,0.1)), obj1{o1}, obj2{o2}, m_stifness{stif} {
    deformation();

    m_restLength=glm::length(o1->getPosition()-o2->getPosition())-length;

    o1->m_springList.push_back(this);
    o2->m_springList.push_back(this);
}

void PSpring::deformation(){
    setIdentity();

    Vector3 v1 = glm::normalize(Vector3(0,1,0));
    Vector3 v2 = glm::normalize(obj2->getPosition()-obj1->getPosition());
    float dist = glm::length(obj2->getPosition()-obj1->getPosition());

    float cos = glm::degrees(glm::acos(glm::dot(v1,v2)));
    Vector3 axis = glm::cross(v1,v2);
    if(axis==Vector3(0)) axis =Vector3(1,0,0);

    setPositionModel(obj1->getPosition());
    rotate(cos,axis);
    scale(1,dist,1);

}

Vector3 PSpring::computeForce(PObject *o){
    float l = glm::length(obj2->getPosition()-obj1->getPosition());
    float res;

    res = l - m_restLength;
    res*= res;
    res*=m_stifness;

    Vector3 r;
    if(o==obj1){
        r = glm::normalize(obj2->getPosition()-obj1->getPosition());
    }else{
        r = glm::normalize(obj1->getPosition()-obj2->getPosition());
    }
    if(l<m_restLength) r=-r;

    return Vector3(res)*r;


}

void PSpring::displayInfo() {
    Log(logInfo) << "| --------------- PSpring ---------------";
    Log(logInfo) << "| - Name: " << getName();
    Log(logInfo) << "| - PObject: " << obj1->getName() << "/" << obj2->getName();
    Log(logInfo) << "| - Stiffness: " << m_stifness;
    Log(logInfo) << "| - Rest length: " << m_restLength;
    Log(logInfo) << "| - Actual length: " << glm::length(obj1->getPosition()-obj2->getPosition());
}