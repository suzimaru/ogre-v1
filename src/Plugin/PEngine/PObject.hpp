//
// Created by pierre on 20/08/18.
//

#ifndef OGRE_POBJECT_HPP
#define OGRE_POBJECT_HPP

#include <Engine/Object/Object.hpp>
#include <Core/Macros.hpp>

class PSpring;

struct UtilDataP{
    float deltaT;
    float boundFactor = 0.8;
    float airFactor=0.999;
    bool traceActive = false;
};

class PObject : public Object{
public:
    PObject(Vector3 pos = Vector3(0,1,0), Vector3 speed = Vector3(0), float weight = 1, float radius = 0.5, std::string materialTrace = "blank");


    void setPosition(Vector3 position);
    void setSpeed(Vector3 speed);
    Vector3 getPosition();
    Vector3 getSpeed();

    std::vector<PSpring*> m_springList;

    void computeSpeed(UtilDataP &u);
    void computePosition(UtilDataP &u);
    void computeForce(UtilDataP &u);

    Object *trace;
    void draw(DataForDrawing data);

    void displayInfo();

private:
    Vector3 m_position;
    Vector3 m_speed;
    float m_weight;

    float m_radius;


};


#endif //OGRE_POBJECT_HPP
