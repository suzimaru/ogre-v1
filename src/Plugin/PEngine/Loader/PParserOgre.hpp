//
// Created by pierre on 27/08/18.
//

#ifndef OGRE_PPARSEROGRE_HPP
#define OGRE_PPARSEROGRE_HPP


#include <src/Engine/Renderer/DrawData.hpp>
#include <src/Engine/Loader/ParserOgre.hpp>
#include <src/Plugin/PEngine/PObject.hpp>
#include <src/Plugin/PEngine/PEngine.hpp>


class PParserOgre {
public:
    PParserOgre(DrawData *d, PEngine *p);

    void processPObject(TOKENS list, bool inv = false);
    std::string processPSpring(TOKENS list);
    void  processRebound(TOKENS list);
    void processDefaultMaterial(TOKENS list);

    PEngine *m_pEngine;
    DrawData *m_drawData;
    std::vector<PObject*> listPObject;

    std::string defaultMaterial = "default";

};


#endif //OGRE_PPARSEROGRE_HPP
