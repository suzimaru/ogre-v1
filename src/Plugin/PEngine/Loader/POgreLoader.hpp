//
// Created by pierre on 27/08/18.
//

#ifndef OGRE_POGRELOADER_HPP
#define OGRE_POGRELOADER_HPP


#include <src/Engine/Loader/OgreLoader.hpp>
#include <src/Plugin/PEngine/PEngine.hpp>
#include "PParserOgre.hpp"

class POgreLoader : public OgreLoader{
public:
    POgreLoader(PEngine *pEngine);

    std::vector<std::string> getFormatManaged() override;
    virtual void loadCustomData() override;
    bool customToken(std::vector<std::string> line, int n) override ;

    PEngine *m_pEngine;
    PParserOgre *pParserOgre;

};


#endif //OGRE_POGRELOADER_HPP
