//
// Created by pierre on 27/08/18.
//

#include <src/Engine/Material/MaterialGenerator.hpp>
#include <Plugin/PEngine/PSpring.hpp>
#include "PParserOgre.hpp"

PParserOgre::PParserOgre(DrawData *d, PEngine *p){
    m_pEngine=p;
    m_drawData=d;
}

void PParserOgre::processPObject(TOKENS list, bool inv){
    Vector3 p = getVector3(list,1,Vector3(0));
    Vector3 v = getVector3(list,4,Vector3(0));
    float w = getFloat(list,7,1);
    PObject *o =new  PObject(p,v,w,0.5,kdMaterialGenerator(&m_drawData->materialManager));
    if(!inv) {
        m_drawData->objectManager.addValue(o,"PObject");
        m_pEngine->addValue(o);
    }
    listPObject.push_back(o);
    o->setMaterial(defaultMaterial);
}

std::string PParserOgre::processPSpring(TOKENS list){
    int o1 = getFloat(list,1,0);
    int o2 = getFloat(list,2,0);
    int k = getFloat(list,3,5);
    int l = getFloat(list,4,0);
    if(o1>=(int)listPObject.size()){
        return "PObject n°" + std::to_string(o1) + " does not exist!";
    }if(o2>=(int)listPObject.size()){
        return "PObject n°" + std::to_string(o2) + " does not exist!";
    }
    PSpring *s = new PSpring(listPObject[o1],listPObject[o2],k,l);
    m_drawData->objectManager.addValue(s,"Spring");
    m_pEngine->addSpring(s);
    s->setMaterial(defaultMaterial);
    return "";
}

void PParserOgre::processRebound(TOKENS list){
    float rebound = getFloat(list,1,0.85);
    m_pEngine->utilDataP.boundFactor=rebound;
}

void PParserOgre::processDefaultMaterial(TOKENS list){
    defaultMaterial = getString(list,1,"defaut");
}