//
// Created by pierre on 27/08/18.
//

#include <src/Engine/Material/MaterialGenerator.hpp>
#include "POgreLoader.hpp"
#include "PParserOgre.hpp"

POgreLoader::POgreLoader(PEngine *pEngine): OgreLoader(), m_pEngine{pEngine} {
}

std::vector<std::string> POgreLoader::getFormatManaged(){
    std::vector<std::string> formats;
    formats.push_back("*.pogre");
    return formats;
}

void POgreLoader::loadCustomData() {
    pParserOgre=new PParserOgre(m_drawData,m_pEngine);
}


bool POgreLoader::customToken(std::vector<std::string> line, int n){
    std::string token = line[0];
    bool token_unknown = false;
    switch(token.c_str()[0]){
        case 'p':
            if(token == "pobject") {
                pParserOgre->processPObject(line);
            }else if(token == "pobjectinv"){
                pParserOgre->processPObject(line,true);
            }else if(token == "pspring"){
                std::string err=pParserOgre->processPSpring(line);
                if(err!="") Error(err,n);
            }else if(token == "pDefaultMaterial"){
                pParserOgre->processDefaultMaterial(line);
            }else token_unknown=true;
            break;
        case 'r':
            if(token == "rebound"){
                pParserOgre->processRebound(line);
            }else token_unknown=true;
            break;
        default: token_unknown=true; break;
    }
    return token_unknown;
}
