//
// Created by pierre on 28/08/18.
//

#ifndef OGRE_PHYSICSHORTCUT_HPP
#define OGRE_PHYSICSHORTCUT_HPP


#include <src/Gui/ShortcutInterface.hpp>
#include "PEngine.hpp"

class PhysicShortcut : public ShortcutInterface{
public:
    PhysicShortcut(PEngine *pEngine);

    bool keyboard(unsigned char k) override;

    PEngine *m_pEngine;

};


#endif //OGRE_PHYSICSHORTCUT_HPP
